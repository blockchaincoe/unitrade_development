/** ********************** Import node modules ************************ */
import mongoose from "mongoose";

/** ********************** Import local modules ************************ */
import db from "../dbConnection";

// This defines customer schema which will store customer information
const BankSchema = new mongoose.Schema({
  name: { type: String, required: true },
  password: { type: String, required: true},
  email: { type: String,
    required: true,
    index: { unique: true },
    match: /.+@.+\..+/,
    lowercase: true },
  IFSC: { type: String, required: true },
  branch: { type: String, required: true },
  city: { type: String, required: true },
  state: { type: String, required: true },
  country: { type: String, required: true }
});

export default db.model("Bank", BankSchema);
