/** ********************** Import node modules ************************ */
import mongoose from "mongoose";

/** ********************** Import local modules ************************ */
import db from "../dbConnection";

// This defines customer schema which will store customer information
const KycSchema = new mongoose.Schema({
  kyc_number: { type: String, required: true },
  status: {
    type: String, required: true, 
    enum: ["pending", "active"],
    default: "pending"
  }
});

export default db.model("Kyc", KycSchema);
