/** ********************** Import node modules ************************ */
import bodyParser from "body-parser";
import config from "config";
import cors from "cors";
import express from "express";
import morgan from "morgan";
import swaggerUi from "swagger-ui-express";

/** ********************** Import local modules *********************** */
import apiRoutes from "./apiRoutes";
import logger from "./utils/logger";
import swaggerDocument from "./swagger.json";

/** ****************************************************************** */
// import enroll from "./hyperledger/files/enrollAdmin";
// import index from "./hyperledger/files/index"

/** ********************** Variable listing ************************** */
const port = process.env.PORT || config.get("TradeFinance.port");
const env = process.env.NODE_ENV || "development";

logger.info(`Running on ${env} environment!`);

const app = express();

/** Routing */
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(express.static(`${__dirname}/ui`));
app.use("/api", apiRoutes);


app.use(express.static(__dirname + '/files'));
/** Swagger implementation */
if (config.get("TradeFinance.serveSwaggerDoc")) {
  app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  logger.info(`Swagger running on http://localhost:${port}/api-docs`);
  app.use(morgan("dev"));
}

/** Server Connection to defined port */
var server = app.listen(port, error => {
  if (error) {
    logger.error("Server Connection failed!", error);
  } else {
    logger.info(`Listening on port ${port}!`);
  }
});

