/** ********************** Import node modules ************************ */
import { Router } from "express";

/** ********************** Import local modules ************************ */
import authentication from "./helpers/authentication";
import logger from "./utils/logger";
import userController from "./controllers/userController";
import bankController from "./controllers/bankController";
import storage from './helpers/storage';
import letterOfCreditController from "./controllers/letterOfCreditController";

const apiRoutes = Router();

/** **************************** User APIs ********************************* */
/** Get Zipcode data API */
apiRoutes.get("/getZipcodeData", userController.getZipcodeData);

/** Check Kyc */
apiRoutes.post("/checkKYC", userController.checkKYC);

/** SignUp API */
apiRoutes.post("/signUp", userController.signUp);

/** Verify API */
apiRoutes.get("/verifyEmail", userController.verifyEmail);

/** Login API */
apiRoutes.post("/login", userController.login);

/** Forgot Password API */
apiRoutes.post("/forgotPassword", userController.forgotPassword);

/** Reset Password API */
apiRoutes.post("/resetPassword", userController.resetPassword);


/** **************************** Bank APIs ********************************* */

/** Login API */
apiRoutes.post("/bankLogin", bankController.bankLogin);



/** ******************* Authentication Middleware ********************* */
apiRoutes.use((req, res, next) => {
  const token =
    req.body.token || req.params.token || req.headers["x-access-token"];
  if (token) {
    authentication
      .authenticateUser(token)
      .then(decode => {
        req.user = decode;
        next();
      })
      .catch(error => {
        logger.error("Error while checking token: Invalid token", error);
        res.status(403).send({ status: false, message: "Invalid token" });
      });
  } else {
    logger.error("Error while checking token: No Token Provided");
    res.status(403).send({ status: false, message: "No Token Provided" });
  }
});

/** Get User Details API */
apiRoutes.get("/getUserDetails", userController.getUserDetails);

/** Login API */
apiRoutes.get("/getBankDetails", bankController.getBankDetails);

/** Update Password API */
apiRoutes.post("/updatePassword", userController.updatePassword);

/** Add beneficiary API */
apiRoutes.post("/addBeneficiary", userController.addBeneficiary);

/** Search beneficiaries API */
apiRoutes.post("/searchBeneficiary", userController.searchBeneficiary);

/** Get all beneficiaries API */
apiRoutes.get("/getAllBeneficiary", userController.getAllBeneficiary);

/** Search banks API */
apiRoutes.post("/searchBank", userController.searchBank);

/** Get all bank API */
apiRoutes.get("/getAllBank", userController.getAllBank);

/** Upload doc API */
apiRoutes.post("/uploadDoc", storage.uploadFiles);

/** ******************* Letter of Credit ********************* */

/** get all LC API */
apiRoutes.get("/getAllLC", letterOfCreditController.getAllLC);

/** request LC API */
apiRoutes.post("/requestLC", letterOfCreditController.requestLC);

/** new LC API */
apiRoutes.post("/newLC", letterOfCreditController.newLC);

/** get LC API */
apiRoutes.get("/getLC", letterOfCreditController.getLC);

/** forward LC API */
apiRoutes.post("/forwardLC", letterOfCreditController.forwardLC);

/** edit LC API */
apiRoutes.post("/editLC", letterOfCreditController.editLC);

/** approve LC API */
apiRoutes.post("/approveLC", letterOfCreditController.approveLC);

/** upload Bill Under LC API */
apiRoutes.post("/uploadBillsUnderLC", letterOfCreditController.uploadBillsUnderLC);

/** verify Bills Under LC API */
apiRoutes.post("/verifyBillsUnderLC", letterOfCreditController.verifyBillsUnderLC);

/** release Bills API */
apiRoutes.post("/releaseBills", letterOfCreditController.releaseBills);

/** release Funds API */
apiRoutes.post("/releaseFunds", letterOfCreditController.releaseFunds);

export default apiRoutes;
