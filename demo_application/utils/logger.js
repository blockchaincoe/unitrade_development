import fs from "fs";
import winston from "winston";

const logDirectory = "./logs";

if (!fs.existsSync(logDirectory)) {
  // Create the directory if it does not exist
  fs.mkdirSync(logDirectory);
}

module.exports = winston.createLogger({
  levels: winston.config.syslog.levels,
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.simple()
  ),
  transports: [
    new winston.transports.Console({
      level: "debug",
      handleExceptions: true,
      json: false,
      colorize: true
    }),
    new winston.transports.File({
      level: "info",
      timestamp() {
        return Date();
      },
      filename: `${logDirectory}/tradefinance.log`,
      handleExceptions: true,
      json: true
    })
  ],
  exitOnError: false
});
