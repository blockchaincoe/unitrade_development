module.exports = {
  verification_subject: "Please confirm your Email account",
  verification_body:
    "Please confirm your account by clicking the following link: ",
  verification_link: "http://40.76.70.180:5000/api/verifyEmail?link=",
  expiry_time: 30000000,
  reset_password_link: "http://40.76.70.180:5000/resetPassword/email=",
  reset_password_subject: "Reset Password request",
  reset_password_body:
    "We got a request to reset your password. Please click on the link: ",
  hyperledger: "http://localhost:3000/api",
  hl_id_lc: "resource:trade.finance.demo.LC#",
  hl_id_client: "resource:trade.finance.demo.Client#",
  hl_id_bank: "resource:trade.finance.demo.Bank#",


  enum: {
    lc_pending_at: ["Issuing Bank", "Advising Bank","Beneficiary"],
    status: ["Requested", "Issuing Bank Validated", "Advising Bank Validated", "Beneficiary Approved", "Bills Uploaded by Beneficiary", "Bills Verified by Advising Bank", "Funds Released", "Bills Released" ]
  }


};
