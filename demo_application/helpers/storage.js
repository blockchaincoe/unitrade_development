
/** ********************** Import node modules ************************ */
import multer from 'multer';


/** ********************** Import local modules ************************ */
import logger from '../utils/logger';

const storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './files');
    },
    filename: function (req, file, callback) {
        logger.info(req);
        callback(null, file.originalname);
    }
});

const upload = multer({ storage: storage }).array('files', 4);

function uploadFiles(req, res, next) {
    upload(req, res, function (err) {
        if (err) {
            logger.error("Error while uploading file using multer", err);
            res.status(500).json({ status: false, error: err.message });
        } else {
            logger.info("File uploaded successfully using multer");
            res.status(200).json({
                success: true,
                "File uploaded": true
            });
        }
    });
};

export default {
    uploadFiles,
}