/** ********************** Import node modules ************************ */
import config from "config";
import jsonwebtoken from "jsonwebtoken";
import Promise from "bluebird";

/** ********************** Import local modules ************************ */
import logger from "./../utils/logger";

/** ********************** Variable listing ************************** */
const secretKey = config.get("TradeFinance.tokenSecretKey");

/** @function createToken
 * @desc This function is used to create token for the user
 * @param {JSON object} user includes user details
 * @return {String} token
 */
const createToken = user => {
  const token = jsonwebtoken.sign(
    {
      userEmail: user.email,
      userId: user.id
    },
    secretKey,
    {
      expiresIn: "30 days"
    }
  );
  return token;
};

/** @function authenticateUser
 * @desc This function is used for authenticating the user.
 * @param {String} token user's token
 */
const authenticateUser = token =>
  new Promise((resolve, reject) => {
    jsonwebtoken.verify(token, secretKey, (err, decode) => {
      if (err) {
        logger.error(err);
        reject(err);
      } else {
        resolve(decode);
      }
    });
  });

export default {
  createToken,
  authenticateUser
};
