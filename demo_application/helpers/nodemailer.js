/** ********************** Import node modules ************************ */
import config from "config";
import nodemailer from "nodemailer";
import Promise from "bluebird";

/** ********************** Import local modules ************************ */
import logger from "../utils/logger";

/** ********************** Variable Listing ************************ */
const appConfig = config.get("TradeFinance");

/** @function sendMail
 * @desc This function is used to send mail
 * @param {JSON object} details includes to_email, subject, body
 * @return {String} token
 */
const sendMail = details =>
  new Promise((resolve, reject) => {
    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: appConfig.email,
        pass: appConfig.password
      }
    });
    const mailOptions = {
      from: appConfig.email,
      to: details.to_email,
      subject: details.subject,
      html: details.body
    };

    transporter
      .sendMail(mailOptions)
      .then(result => {
        resolve(result);
      })
      .catch(error => {
        logger.error("verification mail failed: ", error);
        reject(Error(error));
      });
  });

export default {
  sendMail
};
