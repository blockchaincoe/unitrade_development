/** ********************** Import node modules ************************ */
import _ from "lodash";
import bcrypt from "bcrypt";
import config from "config";
import Promise from "bluebird";

/** ********************** Import local modules ************************ */
import Bank from "../models/Bank";
import appConst from "../utils/appConst";
import REQUEST from "../helpers/request";
import authentication from "../helpers/authentication";
import logger from "../utils/logger";

/** @function addBank
 * @desc This is a service used for adding banks to the db
 */
const addBank = bankData =>
  new Promise((resolve, reject) => {
    // new Bank(bankData)
    //   .save()
    Bank.insertMany(bankData)
      .then(bank => {
        logger.info("DB add bank response: ", bank);
        resolve(bank);
      })
      .catch(error => {
console.log("##################################")
console.log("error------------",error)
        logger.error("DB add bank  error: ", error);
        reject(new Error(error));
      });
  });

/** @function addBankHL
* @desc This is a service used for adding banks to the db
*/
const addBankHL = bankData =>
  new Promise((resolve, reject) => {

    let reqOps = {
      url: _.get(appConst, ['hyperledger'], '') + '/Bank',
      method: 'POST',
      json: {
        "bankId": bankData._id,
        "bankName": bankData.name,
        "IFSCcode": bankData.IFSC,
        "branch": bankData.branch,
      }
    };
    REQUEST.makeRequest(reqOps)
      .then(bank => {
        logger.info("DB add bank hl response: ", bank);
        resolve(bank);
      })
      .catch(error => {
        logger.error("DB add bank hl error: ", error);
        reject(new Error(error));
      });
  });


/* eslint-disable max-statements */
/** @function bankLogin
 * @desc This is a service used for login by providing user email and password
 * @param email includes user's email
 * @param password include user's password
 */
const bankLogin = (email, password) =>
  new Promise((resolve, reject) => {
    const projections = {
      email: true,
      name: true,
      password: true,
      id: true
    };
    Bank.find({ email }, projections)
      .then(async bankResult => {
        if (bankResult[0]) {
          const isCorrectPassword = await bcrypt.compare(
            password,
            bankResult[0].password
          );

          if (isCorrectPassword) {
            const token = authentication.createToken(bankResult[0]);
            const loginResult = {
              name: bankResult[0].name,
              bank_id: bankResult[0].id,
              email: bankResult[0].email,
              token
            };
            logger.info("DB login response: ", loginResult);
            resolve(loginResult);
          } else {
            reject(new Error("Invalid password"));
          }
        } else {
          reject(new Error("Email does not exist!"));
        }
      })
      .catch(error => {
        logger.error("DB login error: ", error);
        reject(new Error(error));
      });
  });

/** @function getBankDetails
* @desc This is a service used for fetching bank details by email
* @param email includes user's email
*/
const getBankDetails = email =>
  new Promise((resolve, reject) => {
    Bank.find({ email })
      .then(async userResult => {

        let reqOps = {
          url: _.get(appConst, ['hyperledger'], '') + '/Bank/' + userResult[0]._id,
          method: 'GET'
        };
        let hl_detail = await REQUEST.makeRequest(reqOps);
        let LC = {
          "pendingLC": hl_detail.result.pendingLC,
          "activeLC": hl_detail.result.activeLC,
          "closedLC": hl_detail.result.closedLC
        };
        _.set(userResult, [0, "LC"], LC);
        logger.info("DB get bank details response: ", userResult[0]);
        resolve(userResult[0]);
      })
      .catch(error => {
        logger.error("DB get bank details error: ", error);
        reject(new Error(error));
      });
  });

export default {
  addBank,
  addBankHL,
  bankLogin,
  getBankDetails
};
