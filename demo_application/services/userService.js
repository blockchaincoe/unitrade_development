/** ********************** Import node modules ************************ */
import _ from "lodash";
import bcrypt from "bcrypt";
import config from "config";
import Promise from "bluebird";

/** ********************** Import local modules ************************ */
import appConst from "../utils/appConst";
import REQUEST from "../helpers/request";
import authentication from "../helpers/authentication";
import logger from "../utils/logger";
import timer from "../utils/timer";
import User from "../models/User";
import Bank from "../models/Bank";
import Kyc from "../models/Kyc";
import Zipcode from "../models/Zipcode";

/** ********************** Variable Listing ************************ */
const appConfig = config.get("TradeFinance");

/** @function getZipcodeData
 * @desc This is a service used for fetching Zipcode data
 */
const getZipcodeData = pin =>
  new Promise((resolve, reject) => {
    Zipcode.find({ pincode: pin })
      .then(result => {
        // logger.info("DB packages response: ", result);
        const res = {
          city: result.districtName,
          state: result.stateName
        };
        resolve(res);
      })
      .catch(error => {
        logger.error("DB zipcode error: ", error);
        reject(Error(error));
      });
  });

  const checkKYC = kyc =>
  new Promise((resolve, reject) => {
    Kyc.find({kyc_number: kyc})
    .then(result => {
      if(result.length){
        if(result[0].status == "active")
        resolve("active");
        else
        resolve("pending");
    }else{
      resolve("invalid");
    }
      
    })
    .catch(error => {
      logger.error("DB zipcode error: ", error);
      reject(Error(error));
    })
  });

  

/** @function signUp
 * @desc This is a service used for sign up by providing user details
 * @param {JSON Object} userDetails includes the user credentials and details (email, password, name, contact, organisation)
 */
const signUp = userDetails =>
  new Promise((resolve, reject) => {
    bcrypt
      .hash(userDetails.password, appConfig.saltRounds)
      .then(async hash => {
        const emailHash = Math.floor(Math.random() * 1000000000);
        logger.info(`password hash ${hash}`);
        _.set(userDetails, "password", hash);
        _.set(userDetails, "email_verification_code", emailHash);
        _.set(
          userDetails,
          "email_verification_expiry",
          timer.getTimeAfterAnHour()
        );
        const user = await new User(userDetails).save();
        if (user) {
          let user_data = {
            "clientId": user._id.toString(),
            "client_api_key": user.client_api_key,
            "address": user.address,
            "first_name": user.first_name,
            "last_name": user.last_name,
          }
console.log("user id----------------",user._id)
          let reqOps = {
            url: _.get(appConst, ['hyperledger'], '') + '/Client',
            method: 'POST',
            json: user_data
          };
          REQUEST.makeRequest(reqOps)
            .then(LC => {
              logger.info("HL add LC response: ", LC);
              resolve(user.email_verification_code);
            })
            .catch(error => {
              logger.error("HL add LC error: ", error);
              reject(new Error(error));
            });
        }

      })
      .catch(error => {
        logger.error("DB Signup error: ", error);
        reject(Error(error));
      });
  });

/** @function verifyEmail
 * @desc This is a service used for verification of the user
 * @param {JSON Object} userHash includes the unique hash of the user sent via email
 */
const verifyEmail = (userHash, email) =>
  new Promise((resolve, reject) => {
    User.find({ email })
      .then(async userResult => {
        const expiryTime = Math.abs(
          userResult[0].email_verification_expiry - timer.getNow()
        );
        await User.findOneAndUpdate({ email }, { $set: { status: true } });
          logger.info("DB verify response: Verified");
        if (
          userResult[0].email_verification_code === userHash &&
          appConst.expiry_time >= expiryTime

        ) {
          //await User.findOneAndUpdate({ email }, { $set: { status: true } });
          logger.info("DB verify response: Verified $$$$$$$$$$%%%%");
          resolve("User verified");
        } else {
          reject(new Error("verification link expired"));
        }
      })
      .catch(error => {
        logger.error("DB Verify Email error: ", error);
        reject(Error(error));
      });
  });

/* eslint-disable max-statements */
/** @function login
 * @desc This is a service used for login by providing user email and password
 * @param email includes user's email
 * @param password include user's password
 */
const login = (email, password) =>
  new Promise((resolve, reject) => {
    const projections = {
      email: true,
      last_name: true,
      first_name: true,
      password: true,
      status: true,
      id: true
    };
    User.find({ email }, projections)
      .then(async userResult => {
        if (userResult[0]) {
          const isCorrectPassword = await bcrypt.compare(
            password,
            userResult[0].password
          );
          if (userResult[0].status) {
            if (isCorrectPassword) {
              const token = authentication.createToken(userResult[0]);
              const loginResult = {
                first_name: userResult[0].first_name,
                last_name: userResult[0].last_name,
                user_id: userResult[0].id,
                email: userResult[0].email,
                token
              };
              logger.info("DB login response: ", loginResult);
              resolve(loginResult);
            } else {
              reject(new Error("Invalid password"));
            }
          } else {
            reject(new Error("Email not verified!"));
          }
        } else {
          reject(new Error("Email does not exist!"));
        }
      })
      .catch(error => {
        logger.error("DB login error: ", error);
        reject(new Error(error));
      });
  });

/** @function getUserDetails
 * @desc This is a service used for fetching user details by email
 * @param email includes user's email
 */
const getUserDetails = email =>
  new Promise((resolve, reject) => {
    const projections = {
      email: true,
      last_name: true,
      first_name: true,
      id: true,
      contact: true,
      address: true,
      city: true,
      state: true,
      country: true,
      zip_code: true,
      organisation: true,
      role: true,
      status: true
    };
    User.find({ email }, projections)
      .then(async userResult => {

        let reqOps = {
          url: _.get(appConst, ['hyperledger'], '') + '/Client/' + userResult[0]._id,
          method: 'GET'
        };
        let hl_detail = await REQUEST.makeRequest(reqOps);
        let LC = {
          "pendingLC": hl_detail.result.pendingLC,
          "activeLC": hl_detail.result.activeLC,
          "closedLC": hl_detail.result.closedLC
        };
        _.set(userResult, [0, "LC"], LC);
        logger.info("DB get user details response: ", userResult[0]);
        resolve(userResult[0]);
      })
      .catch(error => {
        logger.error("DB get user details error: ", error);
        reject(new Error(error));
      });
  });

/** @function getUserPackage
 * @desc This is a service used for fetching user package details by email
 * @param email includes user's email
 */
const getUserPackage = email =>
  new Promise((resolve, reject) => {
    const projections = {
      package: true,
      package_expiry: true,
      api_hits: true,
      api_hits_history: true
    };
    User.find({ email }, projections)
      .then(userResult => {
        logger.info("DB get user package details response: ", userResult[0]);
        resolve(userResult[0]);
      })
      .catch(error => {
        logger.error("DB get user package details error: ", error);
        reject(new Error(error));
      });
  });

/** @function updatePassword
 * @desc This is a service used for updating user password
 * @param email include user's email
 * @param oldPassword include user's oldPassword
 * @param newPassword include user's newPassword
 */
const updatePassword = (email, oldPassword, newPassword) =>
  new Promise((resolve, reject) => {
    const projections = {
      password: true,
      id: true
    };
    User.find({ email }, projections)
      .then(async result => {
        if (await bcrypt.compare(oldPassword, result[0].password)) {
          const passwordHash = await bcrypt.hash(
            newPassword,
            appConfig.saltRounds
          );
          const updateResult = User.findOneAndUpdate(
            { email },
            { $set: { password: passwordHash } }
          );
          logger.info("DB update password response: ", updateResult);
          resolve(updateResult);
        } else {
          reject(new Error("invalid old password"));
        }
      })
      .catch(error => {
        logger.error("DB Update password error: ", error);
        reject(new Error(error));
      });
  });


/** @function addBeneficiary
* @desc This is a service used for adding the list beneficiaries in db
* @param userEmail include user's email
* @param beneficiary include beneficiaries's emails
*/
const addBeneficiary = (userEmail, beneficiary) =>
  new Promise((resolve, reject) => {
    const projections = {
      id: true,
      first_name: true,
      last_name: true,
      country: true,
      email: true
    };
    User.find({ email: { $in: beneficiary } }, projections)
      .then(result => {
        if (result) {
          const updateResult = User.findOneAndUpdate(
            { email: userEmail },
            { $push: { beneficiary: result } }
          );
          logger.info("DB add beneficiaries response: ", updateResult);
          resolve(updateResult);
        } else {
          reject(new Error("invalid beneficiaries"));
        }
      })
      .catch(error => {
        logger.error("DB add beneficiaries error: ", error);
        reject(new Error(error));
      });
  });


/** @function getAllBeneficiary
* @desc This is a service used for getting all beneficiaries
* @param email includes keyword to be searched
*/
const getAllBeneficiary = (userEmail) =>
  new Promise((resolve, reject) => {
    const projections = {
      email: true,
      last_name: true,
      first_name: true,
      id: true,
      address: true,
    };
    User.find({email: {$nin: [userEmail]}}, projections)
      .then(userResult => {
        if (userResult) {
          logger.info("get all beneficiary response: ", userResult);
          resolve(userResult);
        } else {
          reject(new Error("No get all beneficiary result"));
        }
      })
      .catch(error => {
        logger.error("get all beneficiary error: ", error);
        reject(new Error(error));
      });
  });



/** @function updatePackage
 * @desc This is a service used for updating user package
 * @param email include user's email
 * @param packageId include user's oldPassword
 * @param packageExpiry include user's packageExpiry
 */
const updatePackage = (email, packageId, packageExpiry) =>
  new Promise((resolve, reject) => {
    User.findOneAndUpdate(
      { email },
      { $set: { package: packageId, package_expiry: packageExpiry } }
    )
      .then(updateResult => {
        if (updateResult) {
          logger.info("DB update package response: ", updateResult);
          resolve(updateResult);
        } else {
          reject(new Error("invalid user"));
        }
      })
      .catch(error => {
        logger.error("DB Update package error: ", error);
        reject(new Error(error));
      });
  });

/** @function resetPassword
 * @desc This is a service used for user password reset
 * @param email include user's email
 * @param password include user's password
 */
const resetPassword = (email, password) =>
  new Promise((resolve, reject) => {
    bcrypt
      .hash(password, appConfig.saltRounds)
      .then(passwordHash =>
        User.findOneAndUpdate({ email }, { $set: { password: passwordHash } })
      )
      .then(resetResult => {
        logger.info("DB reset password response: ", resetResult);
        resolve(resetResult);
      })
      .catch(error => {
        logger.error("DB reset password error: ", error);
        reject(new Error(error));
      });
  });

/** @function searchBeneficiary
* @desc This is a service used for searching beneficiaries
* @param email includes keyword to be searched
*/
const searchBeneficiary = (keyword) =>
  new Promise((resolve, reject) => {
    const projections = {
      email: true,
      last_name: true,
      first_name: true,
      id: true,
      address: true,
    };
    User.find({}, projections)
      .or([{ "first_name": { $regex: keyword, $options: 'i' } }, { "last_name": { $regex: keyword, $options: 'i' } }, { "email": { $regex: keyword, $options: 'i' } }])
      .then(userResult => {
        if (userResult) {
          logger.info("Search response: ", userResult);
          resolve(userResult);
        } else {
          reject(new Error("No search result"));
        }
      })
      .catch(error => {
        logger.error("Search error: ", error);
        reject(new Error(error));
      });
  });


/** @function getAllBank
 * @desc This is a service used for getting all  banks
 * @param email includes keyword to be searched
 */
const getAllBank = () =>
  new Promise((resolve, reject) => {
    const projections = {
      name: true,
      email: true,
      IFSC: true,
      branch: true,
      city: true,
      state: true,
      country: true,
    };
    Bank.find({}, projections)
      .then(bankResult => {
        if (bankResult) {
          logger.info("Get all bank response: ", bankResult);
          resolve(bankResult);
        } else {
          reject(new Error("No get all bank result"));
        }
      })
      .catch(error => {
        logger.error("Search error: ", error);
        reject(new Error(error));
      });
  });

/** @function searchBank
 * @desc This is a service used for searching banks
 * @param email includes keyword to be searched
 */
const searchBank = (keyword) =>
  new Promise((resolve, reject) => {
    const projections = {
      name: true,
      email: true,
      IFSC: true,
      branch: true,
      city: true,
      state: true,
      country: true,
    };
    Bank.find({}, projections)
      .or([{ "name": { $regex: keyword, $options: 'i' } }, { "email": { $regex: keyword, $options: 'i' } }])
      .then(bankResult => {
        if (bankResult) {
          logger.info("Search response: ", bankResult);
          resolve(bankResult);
        } else {
          reject(new Error("No search result"));
        }
      })
      .catch(error => {
        logger.error("Search error: ", error);
        reject(new Error(error));
      });
  });

export default {
  getZipcodeData,
  checkKYC,
  signUp,
  verifyEmail,
  login,
  getUserDetails,
  updatePassword,
  updatePackage,
  resetPassword,
  getUserPackage,
  addBeneficiary,
  getAllBeneficiary,
  searchBeneficiary,
  searchBank,
  getAllBank
};
