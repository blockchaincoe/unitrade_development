/** ********************** Import node modules ************************ */
import Promise from "bluebird";
import _ from "lodash";
/** ********************** Import local modules ************************ */
import REQUEST from "../helpers/request";
import logger from "../utils/logger";
import mongodb from "mongodb";
// import ObjectID from mongodb.ObjectID;
import appConst from "../utils/appConst";

/** @function requestLC
 * @desc This is a service used for request LC
 */
const requestLC = (userId, LCData) =>
  new Promise((resolve, reject) => {
    var objectId = new mongodb.ObjectID();
    LCData.LC_id = objectId;
    LCData.applicant = userId;
    LCData.status = appConst.enum.status[0];
    LCData.pendingAt = appConst.enum.lc_pending_at[0];
    let reqOps = {
      url: _.get(appConst, ['hyperledger'], '') + '/LC',
      method: 'POST',
      json: LCData
    };
    REQUEST.makeRequest(reqOps)
      .then(LC => {
        logger.info("HL add LC response: ", LC);
        resolve(LC.result);
      })
      .catch(error => {
        logger.error("HL add LC error: ", error);
        reject(new Error(error));
      });
  });


/** @function newLC
 * @desc This is a service used for newLC
 */
const newLC = (userId, LCData) =>
  new Promise((resolve, reject) => {

    LCData.LCnotify = appConst.hl_id_lc + LCData.LC_id;
    delete LCData.LC_id;
    let reqOps = {
      url: _.get(appConst, ['hyperledger'], '') + '/newLC',
      method: 'POST',
      json: LCData
    };
    REQUEST.makeRequest(reqOps)
      .then(LC => {
        logger.info("HL new LC response: ", LC);
        resolve(LC.result);
      })
      .catch(error => {
        logger.error("HL new LC error: ", error);
        reject(new Error(error));
      });
  });


/** @function getLC
* @desc This is a service used for newLC
*/
const getLC = (userId, LC_id) =>
  new Promise((resolve, reject) => {

    let reqOps = {
      url: _.get(appConst, ['hyperledger'], '') + '/LC/' + LC_id,
      method: 'GET',
    };
    REQUEST.makeRequest(reqOps)
      .then(LC => {
        logger.info("HL get LC response: ", LC);
        resolve(LC.result);
      })
      .catch(error => {
        logger.error("HL get LC error: ", error);
        reject(new Error(error));
      });
  });


/** @function getAllLC
* @desc This is a service used for getting all LC
*/
const getAllLC = (userId, role) =>
  new Promise((resolve, reject) => {
    let detail_lc;

    let reqOps = {
      url: _.get(appConst, ['hyperledger'], '') + '/LC',
      method: 'GET',
    };

    REQUEST.makeRequest(reqOps)
      .then(async LC => {
        if (role == "Issuing" || role == "Advising") {

          let req_LC = {
            url: _.get(appConst, ['hyperledger'], '') + '/Bank/' + userId,
            method: 'GET',
          };
          detail_lc = await REQUEST.makeRequest(req_LC);
        }
        else if (role == "Beneficiary" || role == "Applicant") {

          let req_LC = {
            url: _.get(appConst, ['hyperledger'], '') + '/Client/' + userId,
            method: 'GET',
          };
          detail_lc = await REQUEST.makeRequest(req_LC);
        }

        let pendingLC = _.get(detail_lc, ['result', 'pendingLC'], []),
        activeLC = _.get(detail_lc, ['result', 'activeLC'], []),
          closedLC = _.get(detail_lc, ['result', 'closedLC'], []);

        let pendingLC_data = pendingLC.map(value => {
          let data = LC.result.filter(b_value => {
            return b_value.LC_id == value
          });
          return data[0];
        });

        let activeLC_data = activeLC.map(value => {
          let data = LC.result.filter(b_value => {
            return b_value.LC_id == value
          });
          return data[0];
        });

        let closedLC_data = closedLC.map(value => {
          let data = LC.result.filter(b_value => { return b_value.LC_id == value });
          return data[0];
        });

        let lc_data = {
          pendingLC: pendingLC_data,
          activeLC: activeLC_data,
          closedLC: closedLC_data
        }

        console.log(lc_data);

        logger.info("HL get LC response: ", lc_data);
        resolve(lc_data);
      })
      .catch(error => {
        logger.error("HL get LC error: ", error);
        reject(new Error(error));
      });
  });


/** @function forwardLC
* @desc This is a service used for adding banks to the db
*/
const forwardLC = (userId, LCData) =>
  new Promise((resolve, reject) => {

    LCData.LCforwarded = appConst.hl_id_lc + LCData.LC_id;

    LCData.pendingAt = appConst.enum.lc_pending_at[appConst.enum.lc_pending_at.indexOf(LCData.pendingAt) + 1];
    LCData.status = appConst.enum.status[appConst.enum.status.indexOf(LCData.status) + 1];
    delete LCData.LC_id;
    let reqOps = {
      url: _.get(appConst, ['hyperledger'], '') + '/forwardLC',
      method: 'POST',
      json: LCData
    };
    REQUEST.makeRequest(reqOps)
      .then(LC => {
        logger.info("HL forward LC response: ", LC);
        resolve(LC.result);
      })
      .catch(error => {
        logger.error("HL forward LC error: ", error);
        reject(new Error(error));
      });
  });


/** @function editLC
* @desc This is a service used for editing LC
*/
const editLC = (userId, LCData) =>
  new Promise((resolve, reject) => {

    LCData.LCedited = appConst.hl_id_lc + LCData.LC_id;

    delete LCData.LC_id;

    let reqOps = {
      url: _.get(appConst, ['hyperledger'], '') + '/editLC',
      method: 'POST',
      json: LCData
    };

    REQUEST.makeRequest(reqOps)
      .then(LC => {
        logger.info("HL edit LC response: ", LC);
        resolve(LC.result);
      })
      .catch(error => {
        logger.error("HL edit LC error: ", error);
        reject(new Error(error));
      });
  });

/** @function approveLC
* @desc This is a service used for approving a LC
*/
const approveLC = (userId, LCData) =>
  new Promise((resolve, reject) => {

    LCData.LCapproved = appConst.hl_id_lc + LCData.LC_id;
    LCData.status = appConst.enum.status[appConst.enum.status.indexOf(LCData.status) + 1];
    delete LCData.LC_id;

    let reqOps = {
      url: _.get(appConst, ['hyperledger'], '') + '/approveLC',
      method: 'POST',
      json: LCData
    };

    REQUEST.makeRequest(reqOps)
      .then(LC => {
        logger.info("HL approve LC response: ", LC);
        resolve(LC.result);
      })
      .catch(error => {
        logger.error("HL approve LC error: ", error);
        reject(new Error(error));
      });
  });

/** @function uploadBillsUnderLC
* @desc This is a service used for uploading Bill Under LC
*/
const uploadBillsUnderLC = (userId, LCData) =>
  new Promise((resolve, reject) => {

    LCData.LCbillsUploaded = appConst.hl_id_lc + LCData.LC_id;

    LCData.pendingAt = appConst.enum.lc_pending_at[appConst.enum.lc_pending_at.indexOf(LCData.pendingAt) - 1];
    LCData.status = appConst.enum.status[appConst.enum.status.indexOf(LCData.status) + 1];
    // LCData.billsDocs = LCData.;
    delete LCData.LC_id;

    let reqOps = {
      url: _.get(appConst, ['hyperledger'], '') + '/uploadBillsUnderLC',
      method: 'POST',
      json: LCData
    };
    REQUEST.makeRequest(reqOps)
      .then(LC => {
        logger.info("HL upload Bill Under LC response: ", LC);
        resolve(LC.result);
      })
      .catch(error => {
        logger.error("HL upload Bill Under LC error: ", error);
        reject(new Error(error));
      });
  });


/** @function verifyBillsUnderLC
* @desc This is a service used for verify Bills Under LC
*/
const verifyBillsUnderLC = (userId, LCData) =>
  new Promise((resolve, reject) => {

    LCData.LCbillsVerified = appConst.hl_id_lc + LCData.LC_id;

    LCData.pendingAt = appConst.enum.lc_pending_at[appConst.enum.lc_pending_at.indexOf(LCData.pendingAt) - 1];
    LCData.status = appConst.enum.status[appConst.enum.status.indexOf(LCData.status) + 1];
    delete LCData.LC_id;
    let reqOps = {
      url: _.get(appConst, ['hyperledger'], '') + '/verifyBillsUnderLC',
      method: 'POST',
      json: LCData
    };
    REQUEST.makeRequest(reqOps)
      .then(LC => {
        logger.info("HL verify Bills Under LC response: ", LC);
        resolve(LC.result);
      })
      .catch(error => {
        logger.error("HL verify Bills Under LC error: ", error);
        reject(new Error(error));
      });
  });


/** @function releaseBills
* @desc This is a service used for releasing Bills
*/
const releaseBills = (userId, LCData) =>
  new Promise((resolve, reject) => {

    LCData.LCbillsReleased = appConst.hl_id_lc + LCData.LC_id;
    LCData.status = appConst.enum.status[appConst.enum.status.indexOf(LCData.status) + 1];
    delete LCData.LC_id;

    let reqOps = {
      url: _.get(appConst, ['hyperledger'], '') + '/releaseBills',
      method: 'POST',
      json: LCData
    };

    REQUEST.makeRequest(reqOps)
      .then(LC => {
        logger.info("HL release Bills response: ", LC);
        resolve(LC.result);
      })
      .catch(error => {
        logger.error("HL release Bills error: ", error);
        reject(new Error(error));
      });
  });

/** @function releaseFunds
* @desc This is a service used for releasing Funds
*/
const releaseFunds = (userId, LCData) =>
  new Promise((resolve, reject) => {

    LCData.LCfundsReleased = appConst.hl_id_lc + LCData.LC_id;
    LCData.status = appConst.enum.status[appConst.enum.status.indexOf(LCData.status) + 1];
    delete LCData.LC_id;

    let reqOps = {
      url: _.get(appConst, ['hyperledger'], '') + '/releaseFunds',
      method: 'POST',
      json: LCData
    };

    REQUEST.makeRequest(reqOps)
      .then(LC => {
        logger.info("HL release Funds response: ", LC);
        resolve(LC.result);
      })
      .catch(error => {
        logger.error("HL release Funds error: ", error);
        reject(new Error(error));
      });
  });

export default {
  requestLC,
  newLC,
  getLC,
  getAllLC,
  forwardLC,
  editLC,
  approveLC,
  uploadBillsUnderLC,
  verifyBillsUnderLC,
  releaseBills,
  releaseFunds

};
