/** ********************** Import local modules ************************ */
import appConst from "../utils/appConst";
import logger from "../utils/logger";
import nodemailer from "../helpers/nodemailer";
import userService from "../services/userService";

/** ********************** Variable listing ************************ */
/* This regular expression checks the string contains atleast one lowercase letter,
  one uppercase letter, one special character and one digit */
const validPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@#$!%*?&])[A-Za-z\d$@#$!%*?&]{8,}/;

/* eslint-disable max-statements */
/* eslint max-lines: "off" */
/** @function getZipcodeData
 * @desc This function is for retrieveing city and state from pincode
 * @param {JSON Object} req includes the user's pincode
 * @param {JSON Object} res includes success and message
 */
const getZipcodeData = (req, res) => {
  const pin = req.body;
  logger.info("get Zip data of pincode : ", pin);
  userService
    .getZipcodeData(pin)
    .then(zipcodeResponse => {
      logger.info(`Zipcode fetched: ${zipcodeResponse}`);
      res.status(200).json({
        success: true,
        data: zipcodeResponse
      });
    })
    .catch(error => {
      logger.error(`zipcode fetching failed: ${error}`);
      res.status(500).json({
        success: false,
        error: error.message
      });
    });
};

const checkKYC = (req, res) => {
  const {kyc} = req.body;
  logger.info("kyc check", kyc);
  userService.checkKYC(kyc)
  .then((response)=>{
    logger.info(`Kyc checked: ${response}`);
    res.status(200).json({
      success: true,
      message: "active"
    });
  })
  .catch((error)=>{
    logger.error("kyc check failed");
    res.status(400).json({
      success: false,
      error: "Invalid/Pending KYC!"
    });
  });
}


/* eslint-disable max-statements */
/* eslint max-lines: "off" */
/** @function signUp
* @desc This function is for user registration 
* @param {JSON Object} req includes the user details (email, password, firstname, zipcode
    lastname, contact, address, organisation, city, country, state)
* @param {JSON Object} res includes success and message
*/
const signUp = (req, res) => {
  const userDetails = req.body;
  logger.info("signUp request: ", userDetails);
  if (userDetails.organisation !== undefined) {
    userDetails.role = "organisation";
  }
  if (
    userDetails !== undefined &&
    userDetails.email !== undefined &&
    userDetails.kyc !== undefined &&
    userDetails.contact !== undefined &&
    userDetails.zip_code !== undefined &&
    userDetails.first_name !== undefined &&
    userDetails.last_name !== undefined &&
    userDetails.address !== undefined &&
    userDetails.city !== undefined &&
    userDetails.state !== undefined &&
    userDetails.country !== undefined &&
    userDetails.password !== undefined &&
    userDetails.client_api_key !== undefined
  ) {
    if (userDetails.password.match(validPassword)) {

      userService.checkKYC(userDetails.kyc)
      .then((kyc_status)=>{
      //kyc_status == "active" 
      if(true){

      userService
        .signUp(userDetails)
        .then(async signUpResponse => {
          logger.info(`Email verification code: ${signUpResponse}`);
          const URL = `${appConst.verification_link + signUpResponse}&email=${
            userDetails.email
            }`;
          const details = {
            to_email: userDetails.email,
            subject: appConst.verification_subject,
            body: `${appConst.verification_body}<a href=${URL}> Click here </a>`
          };
          nodemailer.sendMail(details);
          res.status(200).json({
            success: true,
            message: "User registered successfully. Please verify your email!"
          });
        })
        .catch(error => {
          logger.error("SignUp failed: ", error);
          res.status(500).json({
            success: false,
            error: error.message
          });
        });

      }
      else{
        logger.error("kyc status: Invalid/Pending");
      res.status(400).json({
        success: false,
        error: "Invalid/Pending KYC!"
      });
      }
    }
    )

    } else {
      logger.error("SignUp failed: Invalid Password!");
      res.status(400).json({
        success: false,
        error: "Invalid Password!"
      });
    }
  } else {
    logger.error("SignUp failed: Invalid or Empty fields!");
    res.status(400).json({
      success: false,
      error: "Invalid or Empty fields!"
    });
  }
};

/** @function verifyEmail
 * @desc This function is for user verification
 * @param {JSON Object} req includes the user verification hash
 * @param {JSON Object} res includes success and message
 */
const verifyEmail = (req, res) => {
  const { email, link } = req.query;
  logger.info("verification request: ", req.query);

  if (link !== undefined || email !== undefined) {
    userService
      .verifyEmail(link, email)
      .then(verificationResponse => {
        logger.info(`Verification successful: ${verificationResponse}`);
        res.status(200).json({
          success: true,
          message: "User verification successful!"
        });
      })
      .catch(error => {
        logger.error(`Verification failed: ${error}`);
        res.status(500).json({
          success: false,
          error: error.message
        });
      });
  } else {
    logger.error("Verification failed!");
    res.status(400).json({
      success: false,
      error: "Invalid link!"
    });
  }
};

/** @function login
 * @desc This function is for user login
 * @param {JSON Object} req includes the user email and password
 * @param {JSON Object} res includes success and data
 */
const login = (req, res) => {
  const { email, password } = req.body;
  logger.info(`login request: ${req.body}`);

  if (email !== undefined || password !== undefined) {
    userService
      .login(email, password)
      .then(loginResponse => {
        logger.info(`Login successful: ${loginResponse}`);
        res.status(200).json({
          success: true,
          data: loginResponse
        });
      })
      .catch(error => {
        logger.error(`Login failed: ${error}`);
        res.status(500).json({
          success: false,
          error: error.message
        });
      });
  } else {
    logger.error("Login failed!");
    res.status(400).json({
      success: false,
      error: "Invalid credentials!"
    });
  }
};

/** @function getUserDetails
 * @desc This function is for fetching user details
 * @param {JSON Object} req includes the user email in the token
 * @param {JSON Object} res includes success and data
 */
const getUserDetails = (req, res) => {
  const { userEmail } = req.user;
  logger.info(`Get User Details request: ${userEmail}`);

  if (userEmail !== undefined) {
    userService
      .getUserDetails(userEmail)
      .then(userResponse => {
        logger.info(`Get User Details successful: ${userResponse}`);
        res.status(200).json({
          success: true,
          data: {
            user: userResponse,
            LC: userResponse.LC
          }
        });
      })
      .catch(error => {
        logger.error(`Get User Details failed: ${error}`);
        res.status(500).json({
          success: false,
          error: error.message
        });
      });
  } else {
    logger.error("Get User Details failed!");
    res.status(400).json({
      success: false,
      error: "Unable to fetch user's email from token!"
    });
  }
};

/** @function updatePassword
 * @desc This function is for updating user password
 * @param {JSON Object} req includes the user email, old password and new password
 * @param {JSON Object} res includes success and message
 */
const updatePassword = (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const { userEmail } = req.user;
  logger.info(`Update Password request: ${req.body}`);

  if (oldPassword !== undefined || newPassword !== undefined) {
    userService
      .updatePassword(userEmail, oldPassword, newPassword)
      .then(updatePasswordResponse => {
        logger.info(`Update Password successful: ${updatePasswordResponse}`);
        res.status(200).json({
          success: true,
          message: "Password changed successfully!"
        });
      })
      .catch(error => {
        logger.error(`Update Password failed: ${error}`);
        res.status(500).json({
          success: false,
          error: error.message
        });
      });
  } else {
    logger.error("Update Password failed!");
    res.status(400).json({
      success: false,
      error: "Invalid credentials!"
    });
  }
};


/** @function forgotPassword
 * @desc This function is for submitting forgot password request
 * @param {JSON Object} req includes the user email
 * @param {JSON Object} res includes success and message
 */
const forgotPassword = (req, res) => {
  const { email } = req.body;
  logger.info("Forgot password request: ", email);

  if (email !== undefined) {
    userService
      .getUserDetails(email)
      .then(userResponse => {
        logger.info(`Forgot password response: ${userResponse}`);
        const URL = `${appConst.reset_password_link + email}`;
        const details = {
          to_email: email,
          subject: appConst.reset_password_subject,
          body: `${
            appConst.reset_password_body
            }<a href=${URL}> Reset Password </a>`
        };
        nodemailer.sendMail(details);
        res.status(200).json({
          success: true,
          message: "Reset Password link has been sent to your email!"
        });
      })
      .catch(error => {
        logger.error(`Verification failed: ${error}`);
        res.status(500).json({
          success: false,
          error: error.message
        });
      });
  } else {
    logger.error("Invalid Email!");
    res.status(400).json({
      success: false,
      error: "Invalid Email!"
    });
  }
};

/** @function resetPassword
 * @desc This function is for user password reset after forgot password
 * @param {JSON Object} req includes the user email and password
 * @param {JSON Object} res includes success and message
 */
const resetPassword = (req, res) => {
  const { email, password } = req.body;
  logger.info(`Reset Password request: ${req.body}`);

  if (email !== undefined || password !== undefined) {
    if (password.match(validPassword)) {
      userService
        .resetPassword(email, password)
        .then(resetPasswordResponse => {
          logger.info(`Reset Password successful: ${resetPasswordResponse}`);
          res.status(200).json({
            success: true,
            message: "Password reset successfully!"
          });
        })
        .catch(error => {
          logger.error(`Reset Password failed: ${error}`);
          res.status(500).json({
            success: false,
            error: error.message
          });
        });
    } else {
      logger.error("Reset password failed: Invalid Password!");
      res.status(400).json({
        success: false,
        error: "Invalid Password!"
      });
    }
  } else {
    logger.error("Reset Password failed!");
    res.status(400).json({
      success: false,
      error: "Invalid credentials!"
    });
  }
};


/** @function addBeneficiary
 * @desc This function is for adding the list of beneficiaries in db
 * @param {JSON Object} req includes the user email and beneficiaries emails
 * @param {JSON Object} res includes success and message
 */
const addBeneficiary = (req, res) => {
  const { beneficiary } = req.body;
  const { userEmail } = req.user;
  if (beneficiary !== undefined) {
    userService
      .addBeneficiary(userEmail, beneficiary)
      .then(addBeneficiaryResponse => {
        logger.info(`Adding beneficiaries successful: ${addBeneficiaryResponse}`);
        res.status(200).json({
          success: true,
          message: "Beneficiaries info added successfully!"
        });
      })
      .catch(error => {
        logger.error(`Adding beneficiaries failed: ${error}`);
        res.status(500).json({
          success: false,
          error: error.message
        });
      });
  } else {
    logger.error("Adding beneficiaries failed!");
    res.status(400).json({
      success: false,
      error: "Invalid beneficiaries!"
    });
  }
};

/** @function getAllBeneficiary
 * @desc This function is for getting all beneficiaries
 * @param {JSON Object} res includes success and message
 */
const getAllBeneficiary = (req, res) => {
  const { userEmail } = req.user;
  logger.info(`Get all beneficiary request: ${userEmail}`);

  if (userEmail !== undefined) {
    userService
      .getAllBeneficiary(userEmail)
      .then(beneficiaryResponse => {
        logger.info(`Get all beneficiary successful: ${beneficiaryResponse}`);
        res.status(200).json({
          success: true,
          data: beneficiaryResponse
        });
      })
      .catch(error => {
        logger.error(`Get all beneficiary failed: ${error}`);
        res.status(500).json({
          success: false,
          error: error.message
        });
      });
  } else {
    logger.error("Get all beneficiary failed!");
    res.status(400).json({
      success: false,
      error: "Unable to fetch user's email from token!"
    });
  }
};

/** @function searchBeneficiary
 * @desc This function is for searching beneficiaries
 * @param {JSON Object} req includes the keyword to be searched
 * @param {JSON Object} res includes success and message
 */
const searchBeneficiary = (req, res) => {
  const { keyword } = req.body;
  logger.info(`Get User Details request: ${keyword}`);

  if (keyword !== undefined) {
    userService
      .searchBeneficiary(keyword)
      .then(userResponse => {
        logger.info(`Beneficiary search successful: ${userResponse}`);
        res.status(200).json({
          success: true,
          data: userResponse
        });
      })
      .catch(error => {
        logger.error(`Beneficiary search failed: ${error}`);
        res.status(500).json({
          success: false,
          error: error.message
        });
      });
  } else {
    logger.error("Beneficiary search failed!");
    res.status(400).json({
      success: false,
      error: " Unable to find beneficiary with the given keyword!"
    });
  }
};


/** @function getAllBank
 * @desc This function is for getting all banks
 * @param {JSON Object} res includes success and message
 */
const getAllBank = (req, res) => {
  const { userEmail } = req.user;
  logger.info(`Get all bank Details request: ${userEmail}`);

  if (userEmail !== undefined) {
    userService
      .getAllBank()
      .then(getAllBankResponse => {
        logger.info(`Get all bank successful: ${getAllBankResponse}`);
        res.status(200).json({
          success: true,
          data: getAllBankResponse
        });
      })
      .catch(error => {
        logger.error(`Get all bank failed: ${error}`);
        res.status(500).json({
          success: false,
          error: error.message
        });
      });
  } else {
    logger.error("Get all bank failed!");
    res.status(400).json({
      success: false,
      error: " Unable to Get all bank!"
    });
  }
};



/** @function searchBank
 * @desc This function is for searching banks
 * @param {JSON Object} req includes the keyword to be searched
 * @param {JSON Object} res includes success and message
 */
const searchBank = (req, res) => {
  const { keyword } = req.body;
  logger.info(`Get User Details request: ${keyword}`);

  if (keyword !== undefined) {
    userService
      .searchBank(keyword)
      .then(userResponse => {
        logger.info(`Beneficiary search successful: ${userResponse}`);
        res.status(200).json({
          success: true,
          data: userResponse
        });
      })
      .catch(error => {
        logger.error(`Beneficiary search failed: ${error}`);
        res.status(500).json({
          success: false,
          error: error.message
        });
      });
  } else {
    logger.error("Beneficiary search failed!");
    res.status(400).json({
      success: false,
      error: " Unable to find beneficiary with the given keyword!"
    });
  }
};


export default {
  getZipcodeData,
  checkKYC,
  signUp,
  verifyEmail,
  login,
  getUserDetails,
  updatePassword,
  forgotPassword,
  resetPassword,
  addBeneficiary,
  getAllBeneficiary,
  searchBeneficiary,
  searchBank,
  getAllBank
};
