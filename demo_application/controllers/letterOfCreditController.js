/** ********************** Import local modules ************************ */
import logger from "../utils/logger";
import letterOfCreditService from "../services/letterOfCreditService";
import storage from "../helpers/storage";

/** @function uploadDoc
 * @desc This function is for uploading documents
 * @param {JSON Object} res includes success and data
 */
const uploadDoc = (req, res) => {
  logger.info("request LC");
  const { LCData } = req.body;
  const { userId } = req.user;
  
  letterOfCreditServices
    .uploadDoc(userId, LCData)
    .then(docResponse => {
      logger.info(`doc uploaded: ${docResponse}`);
      res.status(200).json({
        success: true,
        data: docResponse
      });
    })
    .catch(error => {
      logger.error(`doc uploadation failed: ${error}`);
      res.status(500).json({
        success: false,
        error: error.message
      });
    });
};

/** @function requestLC
 * @desc This function is for requesting a letter of  credit
 * @param {JSON Object} res includes success and data
 */
const requestLC = (req, res) => {
  logger.info("request LC");
  const { LCData } = req.body;
  const { userId } = req.user;
  letterOfCreditService
    .requestLC(userId, LCData)
    .then(LCResponse => {
      logger.info(`LC created: ${LCResponse}`);
      res.status(200).json({
        success: true,
        data: LCResponse
      });
    })
    .catch(error => {
      logger.error(`LC creation failed: ${error}`);
      res.status(500).json({
        success: false,
        error: error.message
      });
    });
};

/** @function newLC
 * @desc This function is for new LC
 * @param {JSON Object} res includes success and data
 */
const newLC = (req, res) => {
  logger.info("request LC");
  const { LCData } = req.body;
  const { userId } = req.user;
  letterOfCreditService
    .newLC(userId, LCData)
    .then(LCResponse => {
      logger.info(`newLC created: ${LCResponse}`);
      res.status(200).json({
        success: true,
        data: LCResponse
      });
    })
    .catch(error => {
      logger.error(`newLC creation failed: ${error}`);
      res.status(500).json({
        success: false,
        error: error.message
      });
    });
};

/** @function getLC
 * @desc This function is for getting LC
 * @param {JSON Object} res includes success and data
 */
const getLC = (req, res) => {
  logger.info("request LC");
  const { LC_id } = req.query;
  const { userId } = req.user;
  letterOfCreditService
    .getLC(userId, LC_id)
    .then(LCResponse => {
      logger.info(`get LC created: ${LCResponse}`);
      res.status(200).json({
        success: true,
        data: LCResponse
      });
    })
    .catch(error => {
      logger.error(`get LC creation failed: ${error}`);
      res.status(500).json({
        success: false,
        error: error.message
      });
    });
};

/** @function getAllLC,
 * @desc This function is for getting all LC
 * @param {JSON Object} res includes success and data
 */
const getAllLC = (req, res) => {
  logger.info("request LC");
  const { role } = req.query;
  const { userId } = req.user;
  letterOfCreditService
    .getAllLC(userId,role)
    .then(LCResponse => {
      logger.info(`get all LC  ${LCResponse}`);
      res.status(200).json({
        success: true,
        data: LCResponse
      });
    })
    .catch(error => {
      logger.error(`get all LC failed: ${error}`);
      res.status(500).json({
        success: false,
        error: error.message
      });
    });
};



/** @function forwardLC
 * @desc This function is for forwarding a letter of  credit
 * @param {JSON Object} res includes success and data
 */
const forwardLC = (req, res) => {
  logger.info("forward LC");
  const { LCData } = req.body;
  const { userId } = req.user;
  letterOfCreditService
    .forwardLC(userId, LCData)
    .then(LCResponse => {
      logger.info(`LC forwarded: ${LCResponse}`);
      res.status(200).json({
        success: true,
        data: LCResponse
      });
    })
    .catch(error => {
      logger.error(`forwarding LC failed: ${error}`);
      res.status(500).json({
        success: false,
        error: error.message
      });
    });
};

/** @function editLC
 * @desc This function is for editing a letter of  credit
 * @param {JSON Object} res includes success and data
 */
const editLC = (req, res) => {
  logger.info("edit LC");
  const { LCData } = req.body;
  const { userId } = req.user;
  letterOfCreditService
    .editLC(userId, LCData)
    .then(LCResponse => {
      logger.info(`edit LC : ${LCResponse}`);
      res.status(200).json({
        success: true,
        data: LCResponse
      });
    })
    .catch(error => {
      logger.error(`edit LC failed: ${error}`);
      res.status(500).json({
        success: false,
        error: error.message
      });
    });
};

/** @function approveLC
 * @desc This function is for approving a letter of  credit
 * @param {JSON Object} res includes success and data
 */
const approveLC = (req, res) => {
  logger.info("edit LC");
  const { LCData } = req.body;
  const { userId } = req.user;
  letterOfCreditService
    .approveLC(userId, LCData)
    .then(LCResponse => {
      logger.info(`approve LC: ${LCResponse}`);
      res.status(200).json({
        success: true,
        data: LCResponse
      });
    })
    .catch(error => {
      logger.error(`approve LC failed: ${error}`);
      res.status(500).json({
        success: false,
        error: error.message
      });
    });
};

/** @function uploadBillsUnderLC
 * @desc This function is for approving a letter of  credit
 * @param {JSON Object} res includes success and data
 */
const uploadBillsUnderLC = (req, res) => {
  logger.info("upload Bill Under LC");
  const { LCData } = req.body;
  const { userId } = req.user;
  letterOfCreditService
    .uploadBillsUnderLC(userId, LCData)
    .then(LCResponse => {
      logger.info(`upload Bill Under LC: ${LCResponse}`);
      res.status(200).json({
        success: true,
        data: LCResponse
      });
    })
    .catch(error => {
      logger.error(`upload Bill Under LC failed: ${error}`);
      res.status(500).json({
        success: false,
        error: error.message
      });
    });
};


/** @function verifyBillsUnderLC
 * @desc This function is for verifying a bill under a letter of  credit
 * @param {JSON Object} res includes success and data
 */
const verifyBillsUnderLC = (req, res) => {
  logger.info("forward LC");
  const { LCData } = req.body;
  const { userId } = req.user;
  letterOfCreditService
    .verifyBillsUnderLC(userId, LCData)
    .then(LCResponse => {
      logger.info(`verifyBillsUnderLC: ${LCResponse}`);
      res.status(200).json({
        success: true,
        data: LCResponse
      });
    })
    .catch(error => {
      logger.error(`verifyBillsUnderLC failed: ${error}`);
      res.status(500).json({
        success: false,
        error: error.message
      });
    });
};

/** @function releaseBills
 * @desc This function is for releasing Bills
 * @param {JSON Object} res includes success and data
 */
const releaseBills = (req, res) => {
  logger.info("edit LC");
  const { LCData } = req.body;
  const { userId } = req.user;
  letterOfCreditService
    .releaseBills(userId, LCData)
    .then(LCResponse => {
      logger.info(`releaseBills : ${LCResponse}`);
      res.status(200).json({
        success: true,
        data: LCResponse
      });
    })
    .catch(error => {
      logger.error(`releaseBills failed: ${error}`);
      res.status(500).json({
        success: false,
        error: error.message
      });
    });
};

/** @function releaseFunds
 * @desc This function is for releasing Funds
 * @param {JSON Object} res includes success and data
 */
const releaseFunds = (req, res) => {
  logger.info("edit LC");
  const { LCData } = req.body;
  const { userId } = req.user;
  letterOfCreditService
    .releaseFunds(userId, LCData)
    .then(LCResponse => {
      logger.info(`releaseFunds : ${LCResponse}`);
      res.status(200).json({
        success: true,
        data: LCResponse
      });
    })
    .catch(error => {
      logger.error(`releaseFunds failed: ${error}`);
      res.status(500).json({
        success: false,
        error: error.message
      });
    });
};

export default {
  uploadDoc,
  requestLC,
  newLC,
  getLC,
  getAllLC,
  forwardLC,
  editLC,
  approveLC,
  uploadBillsUnderLC,
  verifyBillsUnderLC,
  releaseBills,
  releaseFunds
};
