/** ********************** Import local modules ************************ */
import logger from "../utils/logger";
import bankService from "../services/bankService";

/** ********************** Variable listing ************************ */
/* This regular expression checks the string contains atleast one lowercase letter,
  one uppercase letter, one special character and one digit */
  const validPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@#$!%*?&])[A-Za-z\d$@#$!%*?&]{8,}/;



/** @function bankLogin
 * @desc This function is for user login
 * @param {JSON Object} req includes the user email and password
 * @param {JSON Object} res includes success and data
 */
const bankLogin = (req, res) => {
  const { email, password } = req.body;
  logger.info(`login request: ${req.body}`);

  if (email !== undefined || password !== undefined) {
    bankService
      .bankLogin(email, password)
      .then(loginResponse => {
        logger.info(`Login successful: ${loginResponse}`);
        res.status(200).json({
          success: true,
          data: loginResponse
        });
      })
      .catch(error => {
        logger.error(`Login failed: ${error}`);
        res.status(500).json({
          success: false,
          error: error.message
        });
      });
  } else {
    logger.error("Login failed!");
    res.status(400).json({
      success: false,
      error: "Invalid credentials!"
    });
  }
};

/** @function getBankDetails
 * @desc This function is for fetching bank details
 * @param {JSON Object} req includes the bank email in the token
 * @param {JSON Object} res includes success and data
 */
const getBankDetails = (req, res) => {
  const { userEmail } = req.user;
  logger.info(`Get Bank Details request: ${userEmail}`);

  if (userEmail !== undefined) {
    bankService
      .getBankDetails(userEmail)
      .then(userResponse => {
        logger.info(`Get Bank Details successful: ${userResponse}`);
        res.status(200).json({
          success: true,
          data: {
            user: userResponse,
            LC: userResponse.LC
          }
        });
      })
      .catch(error => {
        logger.error(`Get Bank Details failed: ${error}`);
        res.status(500).json({
          success: false,
          error: error.message
        });
      });
  } else {
    logger.error("Get Bank Details failed!");
    res.status(400).json({
      success: false,
      error: "Unable to fetch Bank's email from token!"
    });
  }
};

export default {
  bankLogin,
  getBankDetails
};
