import $ from 'jquery';
import {
    API_URL
} from '../constants';

class BackendServices {

    SignUp(email, password, firstName, lastName, contact, address, city, province, zip, country, organisation, key) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            data: {
                email: email,
                password: password,
                first_name: firstName,
                last_name: lastName,
                contact: contact,
                address: address,
                city: city,
                state: province,
                zip_code: zip,
                country: country,
                organisation: organisation,
                client_api_key: key,
                kyc: key,
            },
            dataType: 'json',
            processData: true,
            type: 'POST',
            url: API_URL + '/signUp'
        });
    }

    getHistorian() {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            processData: true,
            type: 'GET',
            url: 'http://40.76.70.180:3000/api/system/historian'
        });
    }

    uploadDoc(token, doc) {

        var formData = new FormData();
        formData.append('files', doc);
        return $.ajax({
            url: API_URL + '/uploadDoc',
            processData: false,
            contentType: false,
            data: formData,
            type: 'POST',
            headers: {
                'x-access-token': token,
            }
        })
    }

    UploadBillsUnderLC(token, LC_id, pendingAt, status, billsDocs) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            data: {
                LCData: {
                    LC_id: LC_id,
                    pendingAt: pendingAt,
                    status: status,
                    billsDocs: billsDocs,
                }
            },
            dataType: 'json',
            processData: true,
            type: 'POST',
            url: API_URL + '/uploadBillsUnderLC',
            headers: {
                'x-access-token': token
            },
        });
    }

    requestLC(
        token,
        beneficiary,
        typeOfLC,
        applicantFirstName,
        applicantLastName,
        applicantAddress,
        beneficiaryFirstName,
        beneficiaryLastName,
        beneficiaryAddress,
        issuingBank,
        issuingBankName,
        advisingBankName,
        issuingBankIFSCcode,
        advisingBankIFSCcode,
        issuingBankBranch,
        advisingBankBranch,
        advisingBank,
        dateOfIssue,
        dateOfExpiry,
        currencyCode,
        amount,
        percentageCreditAmountTolerance,
        maximumCreditAmount,
        additionalAmountsCovered,
        requestType,
        subrequestType,
        creditAvailableWith,
        creditAvailableBy,
        usanceOfDrafts,
        drawee,
        defferedPaymentDetails,
        partialShipment,
        transshipment,
        shipmentFrom,
        portOfLoading,
        portOfDischarge,
        placeOfDelivery,
        latestDateOfShipment,
        typeOfGoods,
        importLicense,
        exportImportCodeNumber,
        POnumber,
        countryOfOrigin,
        documentsRequired,
        addtionalConditions,
        chargesToBeneficiary,
        periodOfPresentationOfDocuments,
        LCinvoice,
        LCinsurance,
        LCotherDocs,
        billsDocs,
    ) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            data: {
                LCData: {
                    beneficiary: beneficiary,
                    applicantFirstName: applicantFirstName,
                    applicantLastName: applicantLastName,
                    applicantAddress: applicantAddress,
                    beneficiaryFirstName: beneficiaryFirstName,
                    beneficiaryLastName: beneficiaryLastName,
                    beneficiaryAddress: beneficiaryAddress,
                    typeOfLC: typeOfLC,
                    issuingBank: issuingBank,
                    issuingBankName: issuingBankName,
                    advisingBankName: advisingBankName,
                    issuingBankIFSCcode: issuingBankIFSCcode,
                    advisingBankIFSCcode: advisingBankIFSCcode,
                    issuingBankBranch: issuingBankBranch,
                    advisingBankBranch: advisingBankBranch,
                    advisingBank: advisingBank,
                    dateOfIssue: dateOfIssue,
                    dateOfExpiry: dateOfExpiry,
                    currencyCode: currencyCode,
                    amount: amount,
                    percentageCreditAmountTolerance: percentageCreditAmountTolerance,
                    maximumCreditAmount: maximumCreditAmount,
                    additionalAmountsCovered: additionalAmountsCovered,
                    requestType: requestType,
                    subrequestType: subrequestType,
                    creditAvailableWith: creditAvailableWith,
                    creditAvailableBy: creditAvailableBy,
                    usanceOfDrafts: usanceOfDrafts,
                    drawee: drawee,
                    defferedPaymentDetails: defferedPaymentDetails,
                    partialShipment: partialShipment,
                    transshipment: transshipment,
                    shipmentFrom: shipmentFrom,
                    portOfLoading: portOfLoading,
                    portOfDischarge: portOfDischarge,
                    placeOfDelivery: placeOfDelivery,
                    latestDateOfShipment: latestDateOfShipment,
                    typeOfGoods: typeOfGoods,
                    importLicense: importLicense,
                    exportImportCodeNumber: exportImportCodeNumber,
                    POnumber: POnumber,
                    countryOfOrigin: countryOfOrigin,
                    documentsRequired: documentsRequired,
                    addtionalConditions: addtionalConditions,
                    chargesToBeneficiary: chargesToBeneficiary,
                    periodOfPresentationOfDocuments: periodOfPresentationOfDocuments,
                    LCinvoice: LCinvoice,
                    LCinsurance: LCinsurance,
                    LCotherDocs: LCotherDocs,
                    billsDocs: billsDocs,
                }
            },
            dataType: 'json',
            processData: true,
            type: 'POST',
            url: API_URL + '/requestLC',
            headers: {
                'x-access-token': token
            },
        });
    }

    verifyOtp(otp, email) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            processData: true,
            type: 'GET',
            url: API_URL + `/verifyEmail?link=${otp}&email=${email}`
        });
    }

    getAllLC(token, role) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            cache: false,
            dataType: 'json',
            processData: true,
            type: 'GET',
            url: API_URL + `/getAllLC?role=${role}`,
            headers: {
                'x-access-token': token
            },
        });
    }

    getLC(token, lcId) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            cache: false,
            dataType: 'json',
            processData: true,
            type: 'GET',
            url: API_URL + `/getLC?LC_id=${lcId}`,
            headers: {
                'x-access-token': token
            },
        });
    }

    getUserDetails(token) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            processData: true,
            type: 'GET',
            url: API_URL + '/getUserDetails',
            headers: {
                'x-access-token': token
            },
        });
    }

    getBankDetails(token) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            processData: true,
            type: 'GET',
            url: API_URL + '/getBankDetails',
            headers: {
                'x-access-token': token
            },
        });
    }

    getAllBank(token) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            processData: true,
            type: 'GET',
            url: API_URL + '/getAllBank',
            headers: {
                'x-access-token': token
            },
        });
    }

    getAllBeneficiary(token) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            processData: true,
            type: 'GET',
            url: API_URL + '/getAllBeneficiary',
            headers: {
                'x-access-token': token
            },
        });
    }

    changePassword(token, oldPassword, newPassword) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            data: {
                oldPassword: oldPassword,
                newPassword: newPassword,
            },
            processData: true,
            type: 'POST',
            url: API_URL + '/updatePassword',
            headers: {
                'x-access-token': token
            },
        });
    }

    resetPassword(email, password) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            data: {
                email: email,
                password: password,
            },
            processData: true,
            type: 'POST',
            url: API_URL + '/resetPassword',
        });
    }

    ForgotPassword(email) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            data: {
                email: email
            },
            processData: true,
            type: 'POST',
            url: API_URL + '/forgotPassword',
        });
    }

    SignIn(email, password) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            data: {
                email: email,
                password: password,
            },
            dataType: 'json',
            processData: true,
            type: 'POST',
            url: API_URL + '/login'
        });
    }

    BankSignIn(email, password) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            data: {
                email: email,
                password: password,
            },
            dataType: 'json',
            processData: true,
            type: 'POST',
            url: API_URL + '/bankLogin'
        });
    }

    ForwardLC(token, LC_id, pendingAt, status) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            data: {
                LCData: {
                    LC_id: LC_id,
                    pendingAt: pendingAt,
                    status: status
                }
            },
            dataType: 'json',
            processData: true,
            type: 'POST',
            url: API_URL + '/forwardLC',
            headers: {
                'x-access-token': token
            },
        });
    }

    ApproveLC(token, LC_id, status) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            data: {
                LCData: {
                    LC_id: LC_id,
                    status: status
                }
            },
            dataType: 'json',
            processData: true,
            type: 'POST',
            url: API_URL + '/approveLC',
            headers: {
                'x-access-token': token
            },
        });
    }

    VerifyBillsUnderLC(token, LC_id, pendingAt, status) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            data: {
                LCData: {
                    LC_id: LC_id,
                    pendingAt: pendingAt,
                    status: status
                }
            },
            dataType: 'json',
            processData: true,
            type: 'POST',
            url: API_URL + '/verifyBillsUnderLC',
            headers: {
                'x-access-token': token
            },
        });
    }

    EditLC(token, LC_id) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            data: {
                LCData: {
                    LC_id: LC_id,
                }
            },
            dataType: 'json',
            processData: true,
            type: 'POST',
            url: API_URL + '/editLC',
            headers: {
                'x-access-token': token
            },
        });
    }

    ReleaseFunds(token, LC_id, status) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            data: {
                LCData: {
                    LC_id: LC_id,
                    status: status,
                }
            },
            dataType: 'json',
            processData: true,
            type: 'POST',
            url: API_URL + '/releaseFunds',
            headers: {
                'x-access-token': token
            },
        });
    }

    ReleaseBills(token, LC_id, status) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            data: {
                LCData: {
                    LC_id: LC_id,
                    status: status,
                }
            },
            dataType: 'json',
            processData: true,
            type: 'POST',
            url: API_URL + '/releaseBills',
            headers: {
                'x-access-token': token
            },

        });
    }

    newLC(token, LC_id) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            data: {
                LCData: {
                    LC_id: LC_id,
                }
            },
            dataType: 'json',
            processData: true,
            type: 'POST',
            url: API_URL + '/newLC',
            headers: {
                'x-access-token': token
            },
        });
    }

    AddBeneficiary(token, beneficiary) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            data: {
                LCData: {
                    beneficiary: beneficiary,
                }
            },
            dataType: 'json',
            processData: true,
            type: 'POST',
            url: API_URL + '/addBeneficiary',
            headers: {
                'x-access-token': token
            },
        });
    }

    SearchBeneficiary(token, keyword) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            data: {
                keyword: keyword,
            },
            dataType: 'json',
            processData: true,
            type: 'POST',
            url: API_URL + '/searchBeneficiary',
            headers: {
                'x-access-token': token
            },
        });
    }

    SearchBank(token, keyword) {
        return $.ajax({
            //contentType: 'application/x-www-form-urlencoded',
            data: {
                keyword: keyword,
            },
            dataType: 'json',
            processData: true,
            type: 'POST',
            url: API_URL + '/searchBank',
            headers: {
                'x-access-token': token
            },
        });
    }

}

export default new BackendServices();
