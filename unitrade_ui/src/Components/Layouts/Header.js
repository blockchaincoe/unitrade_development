import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import { AppBar, Grid, Button } from '@material-ui/core';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { HeaderStyles } from '../styles';

function TabContainer(props) {
  return (
    <Toolbar component="div" style={{ background: 'transparent' ,padding: 1 * 0.33, flex: 1 }}>
      {props.children}
    </Toolbar>
  );
}
TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};


class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
    };
  }
  render() {
    const { classes } = this.props;
      return (
        <React.Fragment>
          <AppBar position="static" style={{opacity: 0.9}}>
      <Toolbar variant ="dense" className={classes.toolbarMain} color="secondary">
         <img
          alt="Newgen Software Technologies Pvt. Ltd."
          src="./logo.png"
          className={classes.bigAvatar}
          />
          {sessionStorage.getItem('isLoggedIn')  ? (
                <NavLink to="/">
                <Button 
                  color="textPrimary"
                  onClick={() =>{
                    this.props.handleLogout();
                    sessionStorage.clear('user');
                  }}
                  className={classes.button}
                >
                  Log Out
                </Button>
                </NavLink>    
          ):(
            <div>
            <NavLink to="/signup" className={classes.textStyle}>
            <Button onClick={() => {sessionStorage.setItem('activePage' , 'Sign Up')}} color="secondary" >
              Sign Up
            </Button>
            </NavLink>
            <NavLink to="/" className={classes.textStyle}>
            <Button onClick={() => {sessionStorage.setItem('activePage' , 'Login')}} color="secondary" >
              Login
            </Button>
            </NavLink>
            <a href="http://tradefcf.eastus.cloudapp.azure.com:3333" className={classes.textStyle}>
            <Button color="secondary" style={{marginLeft: 10}} >
              Home
            </Button>
            </a>
            </div>
          )}
      </Toolbar>
      </AppBar>
      </React.Fragment>
      );
    }
  }
  export default withStyles(HeaderStyles)(Header);
