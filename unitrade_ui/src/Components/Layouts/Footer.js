import React, {
  Component
} from 'react';
import {
  Typography,
  Grid
} from '@material-ui/core';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import {
  FooterStyles
} from '../styles';


class Footer extends Component {
  constructor(props) {
    super(props);
  };
  render() {
    const {
      classes
    } = this.props;
    return (

      <
      footer className={
          classes.footer
        } >
        <
      Grid container spacing={
            40
          }
          justify='center'
          sm={
            12
          } >
          <
      Grid item sm={
              4
            } > <
      Typography
              align="center"
              component="p" >
              <
      font color="#ffffff" > Newgen Software Technologies Limited < /font> < /
      Typography > <
      Typography variant="subtitle2"
                  align="center"
                  component="p" >
                  <
      /Typography> <
      Typography variant="caption"
                    align="center"
                    component="p" >
                    <
      font color="#ffffff" > A - 6, Satsang Vihar Marg, Qutub Inst.Area, New Delhi - 110 067 < /font> < /
      Typography > <
      Typography variant="caption"
                        align="center"
                        component="p" >
                        <
      font color="#ffffff" > Tel: +91 - 11 - 26964733, 26963571 < /font> < /
      Typography > <
      /Grid> <
      Grid item sm={
                              4
                            } >
                            <
      Typography
                              align="center"
                              component="p" >
                              <
      font color="#ffffff" > A BlockChain powered Trade Finance Solution by <b>Newgen</b> < /font> < /
      Typography > 
      <
      /Grid> <
      Grid item sm={
                                    4
                                  } > <
      p style={
                                      {
                                        textAlign: 'center'
                                      }
                                    } >
                                    <
      a href="http://facebook.com"
                                      target="_blank" > < img alt="FaceBook"
                                        src="./facebook.png" /> < /a>&nbsp; <
      a href="http://plus.google.com"
                                          target="_blank" > < img alt="Google+"
                                            src="./google-plus.png" /> < /a>&nbsp; <
      a href="http://twitter.com"
                                              target="_blank" > < img alt="Twitter"
                                                src="./twitter.png" /> < /a>&nbsp; <
      a href="https://github.com/"
                                                  target="_blank" > < img alt="Github"
                                                    src="./github-circle.png" /> < /a> < /
      p > <
      /Grid> < /
      Grid > <
      /footer>
                                      
                                          );
                                        }
                                      }
Footer.propTypes = {
                                              classes: PropTypes.object.isRequired,
                                          };
export default withStyles(FooterStyles)(Footer);