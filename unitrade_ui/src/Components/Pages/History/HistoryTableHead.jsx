import React from 'react';
import PropTypes from 'prop-types';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Tooltip from '@material-ui/core/Tooltip';

const rows = [
    { id: 'lc_id', numeric: false, disablePadding: true, label: 'LC Id' },
    { id: 'applicant', numeric: true, disablePadding: false, label: 'Applicant' },
    { id: 'doi', numeric: true, disablePadding: false, label: 'Date of Issue' },
    { id: 'doe', numeric: true, disablePadding: false, label: 'Date of Expiry' },
    { id: 'amount', numeric: true, disablePadding: false, label: 'Amount' },
    { id: 'issuingbank', numeric: true, disablePadding: false, label: 'Issuing Bank' },
    { id: 'beneficiary', numeric: true, disablePadding: false, label: 'Beneficiary' },
  ];

class HistoryTableHead extends React.Component {
    createSortHandler = property => event => {
      this.props.onRequestSort(event, property);
    };
  
    render() {
      const {  order, orderBy } = this.props;
      return (
        <TableHead>
          <TableRow>
            <TableCell padding="checkbox"/>
            {rows.map(row => {
              return (
                <TableCell
                  key={row.id}
                  numeric={row.numeric}
                  padding={row.disablePadding ? 'none' : 'default'}
                  sortDirection={orderBy === row.id ? order : false}
                >
                  <Tooltip
                    title="Sort"
                    placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                    enterDelay={300}
                  >
                    <TableSortLabel
                      active={orderBy === row.id}
                      direction={order}
                      proteinlick={this.createSortHandler(row.id)}
                    >
                      {row.label}
                    </TableSortLabel>
                  </Tooltip>
                </TableCell>
              );
            }, this)}
          </TableRow>
        </TableHead>
      );
    }
  }
  
  HistoryTableHead.propTypes = {
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
  };

  export default HistoryTableHead;