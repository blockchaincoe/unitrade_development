import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';
import ViewIcon from '@material-ui/icons/VisibilityRounded';
import FilterListIcon from '@material-ui/icons/FilterList';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { UserDetailsStyles } from '../../styles';
import blue from '@material-ui/core/colors/blue';
import Paper from '@material-ui/core/Paper';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import { BackEnd_URL } from '../../../constants';

const styles = {
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  textDialog: {
    minWidth: '500px',
    maxWidth: '100%',
    width: '600px',
  },
  textStyle: {
    textDecoration: 'none',
  },
};

class SimpleDialog extends React.Component {

  render() {
    const { classes, onClose, ...other } = this.props;

    return (
      <Dialog onClose={onClose} aria-labelledby="simple-dialog-title" {...other}  >
        <div className={classes.textDialog}>
          <DialogTitle id="simple-dialog-title">LC Details</DialogTitle>
          <div style={{ width: '33%', display: 'inline-grid' }}>
            <List disablePadding>
              {console.log(this.props.selectedLC)}
              <ListItem style={{ paddingRight: '0' }}>
                <ListItemText primary="Applicant" secondary={this.props.selectedLC.applicantFirstName + ' ' + this.props.selectedLC.applicantLastName} />
              </ListItem>
              <ListItem style={{ paddingRight: '0' }}>
                <ListItemText primary="Applicant Address" secondary={this.props.selectedLC.applicantAddress} />
              </ListItem>
              <ListItem style={{ paddingRight: '0' }}>
                <ListItemText primary="Beneficiary" secondary={this.props.selectedLC.beneficiaryFirstName + ' ' + this.props.selectedLC.beneficiaryLastName} />
              </ListItem>
              <ListItem style={{ paddingRight: '0' }}>
                <ListItemText primary="Beneficiary Address" secondary={this.props.selectedLC.beneficiaryAddress} />
              </ListItem>
              <ListItem style={{ paddingRight: '0' }}>
                <ListItemText primary="Issuing Bank" secondary={this.props.selectedLC.issuingBankName} />
              </ListItem>
              <ListItem style={{ paddingRight: '0' }}>
                <ListItemText primary="Issuing Bank IFSC code" secondary={this.props.selectedLC.issuingBankIFSCcode} />
              </ListItem>
              <ListItem style={{ paddingRight: '0' }}>
                <ListItemText primary="Advising Bank" secondary={this.props.selectedLC.advisingBankName} />
              </ListItem>
              <ListItem style={{ paddingRight: '0' }}>
                <ListItemText primary="Advising Bank IFSC code" secondary={this.props.selectedLC.advisingBankIFSCcode} />
              </ListItem>
              <ListItem style={{ paddingRight: '0' }}>
                <ListItemText primary="Date of Issue" secondary={this.props.selectedLC.dateOfIssue} />
              </ListItem>
            </List>
          </div>

          <div style={{ width: '33%', display: 'inline-grid' }}>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Date of Expiry" secondary={this.props.selectedLC.dateOfExpiry} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Amount" secondary={this.props.selectedLC.amount} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="LC Type" secondary={this.props.selectedLC.typeOfLC} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Type of Goods" secondary={this.props.selectedLC.typeOfGoods} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Request Type" secondary={this.props.selectedLC.requestType} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Sub-Request Type" secondary={this.props.selectedLC.subrequestType} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Port of Loading" secondary={this.props.selectedLC.portOfLoading} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Port of Discharge" secondary={this.props.selectedLC.portOfDischarge} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Place of Delivery" secondary={this.props.selectedLC.placeOfDelivery} />
            </ListItem>
            
          </div>
          <div style={{ width: '33%', display: 'inline-grid' }}>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Import License No." secondary={this.props.selectedLC.importLicense} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Export/Import Code No." secondary={this.props.selectedLC.exportImportCodeNumber} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Country of Origin" secondary={this.props.selectedLC.countryOfOrigin} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Currency Code" secondary={this.props.selectedLC.currencyCode} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Credit Available By" secondary={this.props.selectedLC.creditAvailableBy} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Credit Available With" secondary={this.props.selectedLC.creditAvailableWith} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Charge to Beneficiary" secondary={this.props.selectedLC.chargesToBeneficiary} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }} >
              <ListItemText primary="Latest Date of Shipment" secondary={this.props.selectedLC.latestDateOfShipment} />
            </ListItem>

          </div>
          <div style={{display: 'flex'}}>
           <a href={`${BackEnd_URL}${this.props.selectedLC.LCinvoice}`} target="blank" className={classes.textStyle}><Button variant="contained" size="small"style={{marginRight: 10, marginLeft: 5}} >View LC Invoive </Button></a> 
            <a href={`${BackEnd_URL}${this.props.selectedLC.LCinsurance}`} target="blank" className={classes.textStyle}><Button variant="contained" size="small" style={{marginRight: 10}}> View LC Insurance </Button></a>
            <a href={`${BackEnd_URL}${this.props.selectedLC.LCotherDocs}`} target="blank" className={classes.textStyle}> <Button variant="contained" size="small" style={{marginRight: 10}}>View Other Documents </Button></a>
            {this.props.selectedLC.billsDocs !== '' && <a href={`${BackEnd_URL}${this.props.selectedLC.billsDocs}`} target="blank" className={classes.textStyle}><Button variant="contained" size="small" style={{marginRight: 5}}> View Bills</Button></a>}
            </div>
        </div>
      </Dialog >
    );
  }
}

SimpleDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  selectedLC: PropTypes.object,
};

const SimpleDialogWrapped = withStyles(styles)(SimpleDialog);

const toolbarStyles = theme => ({
    root: {
      paddingRight: theme.spacing.unit,
      overflowY: 'auto',
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: theme.palette.secondary.main,
            backgroundColor: lighten(theme.palette.secondary.light, 0.85),
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
          },
    spacer: {
      flex: '1 1 100%',
    },
    actions: {
      color: theme.palette.text.secondary,
    },
    title: {
      flex: '0 0 auto',
    },
  });
  
  class HistoryTableToolbar extends React.Component {
    constructor(props) {
      super(props);
    }
    state = {
      open: false,
      selectedLC: {},
    };
    handleClickOpen = () => {
      this.setState({
        open: true,
      });
    };

    handleClose = () => {
      this.setState({ open: false });
    };
    render() {
      const { numSelected, classes } = this.props;
    return (
      <Toolbar
        className={classNames(classes.root, {
          [classes.highlight]: numSelected > 0,
        })}
      >
      <SimpleDialogWrapped
          selectedLC={this.props.selectedLC}
          open={this.state.open}
          onClose={this.handleClose}
          />          
        <div className={classes.title}>
          {numSelected !== 0 ? (
            <Typography color="inherit" variant="subtitle1">
              LC selected
            </Typography>
          ) : (
            <Typography variant="h6" id="tableTitle">
              Letter of Credit History
            </Typography>
          )}
        </div>
        <div className={classes.spacer} />
        <div className={classes.actions}>
          {numSelected !== 0 && (
            <Tooltip title="View">
              <IconButton aria-label="View Full Description" onClick={this.handleClickOpen}>
                <ViewIcon />
              </IconButton>
            </Tooltip>
          ) }
        </div>
      </Toolbar>
    );
  }
}
  
  HistoryTableToolbar.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
  };
  
  export default withStyles(toolbarStyles)(HistoryTableToolbar);
