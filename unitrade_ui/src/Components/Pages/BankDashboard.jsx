import React from 'react'
import { Toolbar, Button, Typography, Paper } from '@material-ui/core';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import History from './History/History';
import Inbox from './Inbox/Inbox';
import Transactions from './Transactions/Transactions';
import Avatar from '@material-ui/core/Avatar';
import BankIcon from '@material-ui/icons/AccountBalanceRounded';
import Active from './Active/Active';
import BackendServices from "../../Services/BackendServices";
import BankProfile from './BankProfile/Profile';
import { NavLink } from 'react-router-dom';


const styles = theme => ({
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  bigAvatar: {
    width: 100,
    height: 75,
  },
  toolbarMain: {
    backgroundImage: 'linear-gradient(to right, black , black)',
    color: 'white',
    borderBottom: `1px solid ${theme.palette.grey[300]}`,
    justifyContent: 'space-between'
  },
  toolbarTitle: {
    flex: 1,
  },
  toolbarSecondary: {
    justifyContent: 'space-between',
    height: 500,
    overflowY: 'auto',
  },
  formControl: {
    margin: theme.spacing.unit,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
  button: {
    color: 'white',
  },
  contained: {
    backgroundColor: '#cccccc',
    color: 'black'
  },
  bigAvatar: {
    width: 100,
    height: 64,
  },
});

class BankDashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pendingLC: [],
      activeLC: [],
      closedLC: [],
    }
  }

  componentWillMount = () => {
    //sessionStorage.setItem('userRole', 'Issuing' );
    //sessionStorage.setItem('activePage', 'active');
    BackendServices.getAllLC(this.getUserDetails().token, sessionStorage.getItem('userRole'))
      .then(res => {
        console.log(res.data.pendingLC);
        this.setState({
          activeLC: res.data.activeLC,
          pendingLC: res.data.pendingLC,
          closedLC: res.data.closedLC
        });
        console.log(this.state.pendingLC);
      }, error => {
        console.log(error.responseJSON.success)
      });
  }

  handleChange = name => event => {
    sessionStorage.setItem('userRole', event.target.value);
    window.location.reload();
  };

  getUserDetails() {
    return JSON.parse(sessionStorage.getItem('user'));
  }
  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <Toolbar className={classes.toolbarMain}>
          <Typography
            component="h6"
            variant="h6"
            color="inherit"
            noWrap
            className={classes.toolbarTitle}
          >
            <img
              alt="Newgen Software Technologies Pvt. Ltd."
              src="./logo.png"
              className={classes.bigAvatar}
            />
            <div style={{fontSize: '17px'}}>
            &nbsp;Hi,&nbsp;{this.getUserDetails().name}
            </div>
          </Typography>

          <FormControl className={classes.formControl}>
            <NativeSelect
              value={sessionStorage.getItem('userRole')}
              onChange={this.handleChange('userRole')}
              style={{color: '#ffffff', borderBottom:'solid 1px #ffffff'}}
              input={<Input name="userRole" id="age-native-helper" />}
            >
              <option value='Issuing' style={{color: '#000000'}}>Issuing Bank </option>
              <option value='Advising'style={{color: '#000000'}}>Advising Bank</option>
            </NativeSelect>
          </FormControl>
          <React.Fragment>
            <flex>

              <Button size="small" variant={sessionStorage.getItem('activePage') === 'active' && "contained"} className={sessionStorage.getItem('activePage') === 'active' ? classes.contained : classes.button} style={{ marginRight: 20 }} onClick={() => { sessionStorage.setItem('activePage', 'active'); window.location.reload(); }}>Active</Button>
              <Button size="small" variant={sessionStorage.getItem('activePage') === 'history' && "contained"} className={sessionStorage.getItem('activePage') === 'history' ? classes.contained : classes.button} style={{ marginRight: 20 }} onClick={() => { sessionStorage.setItem('activePage', 'history'); window.location.reload(); }}>History</Button>
              <Button size="small" variant={sessionStorage.getItem('activePage') === 'profile' && "contained"} className={sessionStorage.getItem('activePage') === 'profile' ? classes.contained : classes.button} style={{ marginRight: 20 }} onClick={() => { sessionStorage.setItem('activePage', 'profile'); window.location.reload(); }}>Profile</Button>
              <Button size="small" variant={sessionStorage.getItem('activePage') === 'inbox' && "contained"} className={sessionStorage.getItem('activePage') === 'inbox' ? classes.contained : classes.button} style={{ marginRight: 20 }} onClick={() => { sessionStorage.setItem('activePage', 'inbox'); window.location.reload(); }}>Inbox</Button>
              <Button size="small" variant={sessionStorage.getItem('activePage') === 'transactions' && "contained"} className={sessionStorage.getItem('activePage') === 'transactions' ? classes.contained : classes.button} style={{ marginRight: 20 }} onClick={() => { sessionStorage.setItem('activePage', 'transactions'); window.location.reload(); }}>Transactions</Button>
              <NavLink to="/">
                <Button
                  size="small"
                  onClick={() => {
                    this.props.handleLogout();
                    sessionStorage.clear('user');
                  }}
                  style={{color: "#ffffff"}}
                >
                  Log Out
                </Button>
              </NavLink>
            </flex>
          </React.Fragment>

        </Toolbar>
        <div className={classes.toolbarSecondary}>
          {sessionStorage.getItem('activePage') === 'history' && <History closedLC={this.state.closedLC} />}
          {sessionStorage.getItem('activePage') === 'inbox' && <Inbox pendingLC={this.state.pendingLC} />}
          {sessionStorage.getItem('activePage') === 'transactions' && <Transactions/>}
          {sessionStorage.getItem('activePage') === 'active' && <Active activeLC={this.state.activeLC} />}
          {sessionStorage.getItem('activePage') === 'profile' && <BankProfile />}
        </div>
      </React.Fragment>
    );
  }
}
BankDashboard.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(BankDashboard);
