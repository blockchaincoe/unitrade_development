
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import ViewIcon from '@material-ui/icons/VisibilityRounded';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import blue from '@material-ui/core/colors/blue';
import { css } from 'react-emotion';

const override = css`
    display: block;
    margin:  30px 7px 300px 626px;
    border-color: red;
`;

const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit,
    overflowY: 'auto',
  },
  highlight:
    theme.palette.type === 'light'
      ? {
        color: theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85),
      }
      : {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark,
      },
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    color: theme.palette.text.secondary,
  },
  title: {
    flex: '0 0 auto',
  },
});

class InboxTableToolbar extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    loading: false,
  };

  render() {
    const { selectedTxn, numSelected, classes } = this.props;
    return (
      <Toolbar
        className={classNames(classes.root, {
          [classes.highlight]: numSelected > 0,
        })}
      >
        <div className={classes.title}>
            <Typography variant="h6" id="tableTitle">
                Transactions on Blockchain
            </Typography>
        </div>
        <div className={classes.spacer} />
      </Toolbar>
    );
  }
}


export default withStyles(toolbarStyles)(InboxTableToolbar);