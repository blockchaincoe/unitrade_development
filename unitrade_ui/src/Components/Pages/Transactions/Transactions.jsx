import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TransactionsTableHead from './TransactionsTableHead';
import TransactionsTableToolbar from './TransactionsTableToolbar';
import BackendServices from "../../../Services/BackendServices";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import blue from '@material-ui/core/colors/blue';

const styless = {
    avatar: {
      backgroundColor: blue[100],
      color: blue[600],
    },
    textDialog: {
      minWidth: '500px',
      maxWidth: '100%',
      width: '600px',
    },
    textStyle: {
      textDecoration: 'none',
    },
  };

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

class ViewDialog extends React.Component {
    
    render() {
      const { classes, onClose, ...other } = this.props;
      return (
        <Dialog onClose={onClose} aria-labelledby="simple-dialog-title" {...other}  >
          <div className={classes.textDialog}>
            <DialogTitle id="simple-dialog-title">LC Details</DialogTitle>
            <div style={{ width: '70%', display: 'inline-grid' }}>
              <List disablePadding>
                {console.log(this.props.selectedTxn)}
                <ListItem style={{ paddingRight: '0' }}>
                  <ListItemText primary="Transaction ID" secondary={this.props.selectedTxn.transactionId} />
                </ListItem>
                <ListItem style={{ paddingRight: '0' }}>
                  <ListItemText primary="Transaction Type" secondary={this.props.txnType} />
                </ListItem>
                <ListItem style={{ paddingRight: '0' }}>
                  <ListItemText primary="Timestamp" secondary={this.props.selectedTxn.transactionTimestamp} />
                </ListItem>
                <ListItem style={{ paddingRight: '0' }}>
                  <ListItemText primary="Invoked By" secondary={this.props.participant} />
                </ListItem>
                <ListItem style={{ paddingRight: '0' }}>
                  <ListItemText primary="Changes made by the Transaction" secondary= {this.props.change} />
                </ListItem>
              </List>
            </div>
          </div>
        </Dialog >
      );
    }
  }
  
  ViewDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    onClose: PropTypes.func,
    selectedLC: PropTypes.object,
  };
  
  const ViewDialogWrapped = withStyles(styless)(ViewDialog);
  const styles = {
    avatar: {
      backgroundColor: blue[100],
      color: blue[600],
    },
  };

class Transactions extends React.Component {
  state = {
    order: 'asc',
    orderBy: 'calories',
    selected: 0,
    page: 0,
    rowsPerPage: 5,
    selectedTxn: {},
    opens: false,
    transactions: [],
    change: 'New User Added.',
    participant: 'Network Admin',
    txnType: 'Add Participant'
  };

  componentWillMount = () => {
    BackendServices.getHistorian()
    .then(res => {
        console.log(res);
        this.setState({transactions: res});
      }, error => {
        console.log(error.responseJSON.success);
      });
  }

  setValues=()=>{
    this.state.selectedTxn.transactionType === 'org.hyperledger.composer.system.AddParticipant' && this.setState({change: 'New User Added.', participant: 'Network Admin', txnType: 'Add Participant' });
    this.state.selectedTxn.transactionType === 'org.hyperledger.composer.system.ActivateCurrentIdentity' && this.setState({change: 'Validate New User', participant: 'Network Admin', txnType: 'Activate Current Identity' });
    this.state.selectedTxn.transactionType === 'org.hyperledger.composer.system.AddAsset' && this.setState({change: 'New Letter of Credit has been Created on HyperLedger.', participant: 'Applicant', txnType: 'Add Asset' });
    this.state.selectedTxn.transactionType === 'org.hyperledger.composer.system.StartBusinessNetwork' && this.setState({change: 'Hyperledger Business network started.', participant: 'Network Admin', txnType: 'Start Business Network' });
    this.state.selectedTxn.transactionType === 'org.hyperledger.composer.system.IssueIdentity' && this.setState({change: 'Network Admin ID generated.', participant: 'Network Admin', txnType: 'Issue Identity'});
    this.state.selectedTxn.transactionType === 'trade.finance.demo.uploadBillsUnderLC' && this.setState({change: 'Bills uploaded by Beneficiary User.', participant: 'Beneficiary', txnType: 'Upload bills under Letter of Credit'});
    this.state.selectedTxn.transactionType === 'trade.finance.demo.releaseFunds' && this.setState({change: 'Funds released by Applicant User.', participant: 'Issuing Bank', txnType: 'Release Funds'});
    this.state.selectedTxn.transactionType === 'trade.finance.demo.approveLC' && this.setState({change: 'Letter of Credit has been Approved.', participant: 'Beneficiary', txnType: 'Approve Letter of Credit'});
    this.state.selectedTxn.transactionType === 'trade.finance.demo.forwardLC' && this.setState({change: 'Letter of Credit Forwarded.', participant: 'Advising Bank', txnType: 'Forward Letter of Credit'});
    this.state.selectedTxn.transactionType === 'trade.finance.demo.editLC' && this.setState({change: 'Letter of Credit Edited.', participant: 'Issuing Bank', txnType: 'Edit Letter of Credit'});
    this.state.selectedTxn.transactionType === 'trade.finance.demo.verifyBillsUnderLC' && this.setState({change: 'Bills associated with LC has been verified.', participant: 'Advising Bank', txnType: 'Verify Bills Under Letter of Credit'});
    this.state.selectedTxn.transactionType === 'trade.finance.demo.newLC' && this.setState({change: 'New Letter of Credit has been validated in Network', participant: 'Applicant', txnType: 'New Letter of Credit'});
    this.state.selectedTxn.transactionType === 'trade.finance.demo.releaseBills' && this.setState({change: 'Bills asscociated with LC has been released.', participant: 'Issuing Bank', txnType: 'Release bills under Letter of Credit'});
};

  handleClickOpens = () => {
    this.setState({
      opens: true,
    });
  };

  handleCloses = value => {
    this.setState({ selectedValue: value, opens: false });
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  };

  handleClick = (event, id, transaction) => {
    this.setState({ selected: id, selectedTxn: transaction });
    this.setValues();
    this.handleClickOpens();
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  isSelected = (id) => this.state.selected === id ? true : false;

  render() {
    const { classes } = this.props;
    const { order, orderBy, rowsPerPage, page } = this.state;
    console.log(this.state.transactions);
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, this.state.transactions.length - page * rowsPerPage);

    return (
      <Paper className={classes.root}>
        <ViewDialogWrapped
            selectedTxn={this.state.selectedTxn}
            open={this.state.opens}
            onClose={this.handleCloses}
            change={this.state.change}
            participant={this.state.participant}
            txnType={this.state.txnType}
          />
        <TransactionsTableToolbar selectedTxn={this.state.selectedTxn} numSelected={this.state.selected} />
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <TransactionsTableHead
              numSelected={this.state.selected}
              order={order}
              orderBy={orderBy}
              onRequestSort={this.handleRequestSort}
              rowCount={this.state.transactions.length}
            />
            <TableBody>
              {stableSort(this.state.transactions, getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(n => {
                  const isSelected = this.isSelected(n.transactionId);
                  return (
                    <TableRow
                      hover
                      onClick={event => this.handleClick(event, n.transactionId, n)}
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={n.id}
                      selected={isSelected}
                    >
                      <TableCell width ="150px" numeric>{n.transactionId}</TableCell>
                      <TableCell numeric>{n.transactionType}</TableCell>
                      <TableCell numeric>{n.participantInvoking}</TableCell>
                      <TableCell numeric>{n.transactionTimestamp}</TableCell>
                      <TableCell numeric>{n.identityUsed}</TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={this.state.transactions.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    );
  }
}

Transactions.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Transactions);
