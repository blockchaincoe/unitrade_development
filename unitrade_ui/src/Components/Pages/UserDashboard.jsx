import React from 'react'
import { Toolbar, Button, Typography, Paper } from '@material-ui/core';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import Avatar from '@material-ui/core/Avatar';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import History from './History/History';
import Inbox from './Inbox/Inbox';
import Active from './Active/Active';
import Profile from './Profile/Profile';
import Transactions from './Transactions/Transactions';
import BackendServices from "../../Services/BackendServices";
//import ChangePassword from './Profile/ChangePassword'
import RequestLcForm from './RequestLcForm';
import { NavLink } from 'react-router-dom';
//import styles from '../styles';
import UserIcon from '@material-ui/icons/Person';

const styles = theme => ({
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto',
    },

  },
  avatar: {
    marginTop: '10px',
    marginLeft: '40px',
    backgroundColor: 'white',
    color: '#ddd',
  },
  bigAvatar: {
    width: 100,
    height: 64,
  },
  textStyle: {
    textDecoration: 'none',
  },
  toolbarMain: {
    backgroundImage: 'linear-gradient(to right, black , black)',
    color: 'white',
    borderBottom: `1px solid ${theme.palette.grey[300]}`,
    justifyContent: 'space-between'
  },
  toolbarTitle: {
    flex: 1,
  },
  toolbarSecondary: {
    backgroundColor: 'white',
    justifyContent: 'space-between',
    height: 500,
    overflowY: 'auto',
  },
  formControl: {
    margin: theme.spacing.unit,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
  button: {
    color: 'white',
  },
  contained: {
    backgroundColor: '#cccccc',
    color: 'black'
  },

});

class UserDashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pendingLC: [],
      activeLC: [],
      closedLC: [],
    }
  }

  componentWillMount = () => {
    //sessionStorage.setItem('userRole', 'Applicant' );
    //sessionStorage.setItem('activePage', 'active');
    BackendServices.getAllLC(this.getUserDetails().token, sessionStorage.getItem('userRole'))
      .then(res => {
        console.log(res.data.pendingLC);

        this.setState({
          activeLC: res.data.activeLC,
          pendingLC: res.data.pendingLC.filter(LC => LC.pendingAt === sessionStorage.getItem('userRole')),
          closedLC: res.data.closedLC
        });
        console.log(this.state.pendingLC);
      }, error => {
        console.log(error.responseJSON.success)
      });
  }

  handleChange = name => event => {
    sessionStorage.setItem('userRole', event.target.value);
    window.location.reload();
  };

  getUserDetails() {
    return JSON.parse(sessionStorage.getItem('user'));
  }

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <Toolbar className={classes.toolbarMain}>
          <Typography
            component="h6"
            variant="h6"
            color="inherit"
            noWrap
            className={classes.toolbarTitle}
          >
            <img
              alt="Newgen Software Technologies Pvt. Ltd."
              src="./logo.png"
              className={classes.bigAvatar}
            />
            <div style={{fontSize: '17px'}}>
            &nbsp;Hi,&nbsp;{this.getUserDetails().first_name}
            </div>
          </Typography>

          <FormControl className={classes.formControl}>
            <NativeSelect
              value={sessionStorage.getItem('userRole')}
              onChange={this.handleChange('userRole')}
              style={{color: '#ffffff' , borderBottom:'solid 1px #ffffff'}}
              input={<Input name="userRole" id="age-native-helper"  />}
            >
              <option value='Applicant' style={{color: '#000000'}}>Applicant / Importer</option>
              <option value='Beneficiary' style={{color: '#000000'}}>Beneficiary/ Exporter</option>
            </NativeSelect>
          </FormControl>
          <Button variant={sessionStorage.getItem('activePage') === 'active' && "contained"} className={sessionStorage.getItem('activePage') === 'active' ? classes.contained : classes.button} size="small" style={{ marginRight: 20 }} onClick={() => { sessionStorage.setItem('activePage', 'active'); window.location.reload(); }}>Active</Button>
          <Button variant={sessionStorage.getItem('activePage') === 'history' && "contained"} className={sessionStorage.getItem('activePage') === 'history' ? classes.contained : classes.button} size="small" style={{ marginRight: 20 }} onClick={() => { sessionStorage.setItem('activePage', 'history'); window.location.reload(); }}>History</Button>
          <Button variant={sessionStorage.getItem('activePage') === 'profile' && "contained"} className={sessionStorage.getItem('activePage') === 'profile' ? classes.contained : classes.button} size="small" style={{ marginRight: 20 }} onClick={() => { sessionStorage.setItem('activePage', 'profile'); window.location.reload(); }}>Profile</Button>
          {sessionStorage.getItem('userRole') === 'Applicant' && <React.Fragment>
            <flex>
              <Button size="small" variant={sessionStorage.getItem('activePage') === 'requestlc' && "contained"} className={sessionStorage.getItem('activePage') === 'requestlc' ? classes.contained : classes.button} style={{ marginRight: 20 }} onClick={() => { sessionStorage.setItem('activePage', 'requestlc'); window.location.reload(); }}>Request Letter of Credit</Button>
            </flex>
          </React.Fragment>}

          {sessionStorage.getItem('userRole') === 'Beneficiary' && <React.Fragment>
            <flex>
              <Button variant={sessionStorage.getItem('activePage') === 'inbox' && "contained"} className={sessionStorage.getItem('activePage') === 'inbox' ? classes.contained : classes.button} size="small" style={{ marginRight: 20 }} onClick={() => { sessionStorage.setItem('activePage', 'inbox'); window.location.reload(); }}>Inbox</Button>
            </flex>
          </React.Fragment>}
          <Button size="small" variant={sessionStorage.getItem('activePage') === 'transactions' && "contained"} className={sessionStorage.getItem('activePage') === 'transactions' ? classes.contained : classes.button} style={{ marginRight: 20 }} onClick={() => { sessionStorage.setItem('activePage', 'transactions'); window.location.reload(); }}>Transactions</Button>
          <NavLink to="/" className={classes.textStyle}>
            <Button
              className={classes.button}
              size="small"
              onClick={() => {
                this.props.handleLogout();
                sessionStorage.clear('user');
              }}
              style={{color: "#ffffff"}}
            >
              Log Out
                </Button>
          </NavLink>
        </Toolbar>
        <div className={classes.toolbarSecondary}>
          {/* {(sessionStorage.getItem('activePage') !== 'history'|| 'inbox' || 'active' || 'requestlc' || 'profile') && sessionStorage.getItem('userRole') === 'dashboard'} */}
          {sessionStorage.getItem('activePage') === 'history' && <History closedLC={this.state.closedLC} role={this.state.userRole} />}
          {sessionStorage.getItem('activePage') === 'transactions' && <Transactions/>}
          {sessionStorage.getItem('activePage') === 'inbox' && <Inbox onReload={this.onReload} pendingLC={this.state.pendingLC} role={this.state.userRole} />}
          {sessionStorage.getItem('activePage') === 'active' && <Active activeLC={this.state.activeLC} role={this.state.userRole} />}
          {sessionStorage.getItem('activePage') === 'requestlc' && <RequestLcForm />}
          {sessionStorage.getItem('activePage') === 'profile' && <Profile />}
        </div>
      </React.Fragment>
    );
  }
}
UserDashboard.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(UserDashboard);
