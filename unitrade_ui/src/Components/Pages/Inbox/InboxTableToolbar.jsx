
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import ViewIcon from '@material-ui/icons/VisibilityRounded';
import FilterListIcon from '@material-ui/icons/FilterList';
import TrendingFlatIcon from '@material-ui/icons/TrendingFlat';
import DoneOutline from '@material-ui/icons/DoneOutline';
import AssignmentTurnedIn from '@material-ui/icons/AssignmentTurnedIn';
import Create from '@material-ui/icons/Create';
import EditAttributes from '@material-ui/icons/EditAttributes';
import HowToVote from '@material-ui/icons/HowToVote';
import Funds from '@material-ui/icons/MoneyTwoTone';
import AttachMoney from '@material-ui/icons/AttachMoney';
import VerticalAlignTop from '@material-ui/icons/VerticalAlignTop';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import React from 'react';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import PersonIcon from '@material-ui/icons/Person';
import AddIcon from '@material-ui/icons/Add';
import Typography from '@material-ui/core/Typography';
import blue from '@material-ui/core/colors/blue';
import BackendServices from "../../../Services/BackendServices";
import Simplert from 'react-simplert';
import ImageUploader from 'react-images-upload';
import { Grid } from '@material-ui/core';
import { BackEnd_URL } from '../../../constants';
import { css } from 'react-emotion';
import { ClipLoader } from 'react-spinners';

const override = css`
    display: block;
    margin:  30px 7px 300px 626px;
    border-color: red;
`;

const styless = {
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  textDialog: {
    minWidth: '500px',
    maxWidth: '100%',
    width: '600px',
  },
  textStyle: {
    textDecoration: 'none',
  },
};

class ViewDialog extends React.Component {

  render() {
    const { classes, onClose, ...other } = this.props;

    return (
      <Dialog onClose={onClose} aria-labelledby="simple-dialog-title" {...other}  >
        <div className={classes.textDialog}>
          <DialogTitle id="simple-dialog-title">LC Details</DialogTitle>
          <div style={{ width: '33%', display: 'inline-grid' }}>
            <List disablePadding>
              {console.log(this.props.selectedLC)}
              <ListItem style={{ paddingRight: '0' }}>
                <ListItemText primary="Applicant" secondary={this.props.selectedLC.applicantFirstName + ' ' + this.props.selectedLC.applicantLastName} />
              </ListItem>
              <ListItem style={{ paddingRight: '0' }}>
                <ListItemText primary="Applicant Address" secondary={this.props.selectedLC.applicantAddress} />
              </ListItem>
              <ListItem style={{ paddingRight: '0' }}>
                <ListItemText primary="Beneficiary" secondary={this.props.selectedLC.beneficiaryFirstName + ' ' + this.props.selectedLC.beneficiaryLastName} />
              </ListItem>
              <ListItem style={{ paddingRight: '0' }}>
                <ListItemText primary="Beneficiary Address" secondary={this.props.selectedLC.beneficiaryAddress} />
              </ListItem>
              <ListItem style={{ paddingRight: '0' }}>
                <ListItemText primary="Issuing Bank" secondary={this.props.selectedLC.issuingBankName} />
              </ListItem>
              <ListItem style={{ paddingRight: '0' }}>
                <ListItemText primary="Issuing Bank IFSC code" secondary={this.props.selectedLC.issuingBankIFSCcode} />
              </ListItem>
              <ListItem style={{ paddingRight: '0' }}>
                <ListItemText primary="Advising Bank" secondary={this.props.selectedLC.advisingBankName} />
              </ListItem>
              <ListItem style={{ paddingRight: '0' }}>
                <ListItemText primary="Advising Bank IFSC code" secondary={this.props.selectedLC.advisingBankIFSCcode} />
              </ListItem>
              <ListItem style={{ paddingRight: '0' }}>
                <ListItemText primary="Date of Issue" secondary={this.props.selectedLC.dateOfIssue} />
              </ListItem>


            </List>
          </div>

          <div style={{ width: '33%', display: 'inline-grid' }}>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Date of Expiry" secondary={this.props.selectedLC.dateOfExpiry} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Amount" secondary={this.props.selectedLC.amount} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="LC Type" secondary={this.props.selectedLC.typeOfLC} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Type of Goods" secondary={this.props.selectedLC.typeOfGoods} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Request Type" secondary={this.props.selectedLC.requestType} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Sub-Request Type" secondary={this.props.selectedLC.subrequestType} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Port of Loading" secondary={this.props.selectedLC.portOfLoading} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Port of Discharge" secondary={this.props.selectedLC.portOfDischarge} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Place of Delivery" secondary={this.props.selectedLC.placeOfDelivery} />
            </ListItem>
           
          </div>
          <div style={{ width: '33%', display: 'inline-grid' }}>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Import License No." secondary={this.props.selectedLC.importLicense} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Export/Import Code No." secondary={this.props.selectedLC.exportImportCodeNumber} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Country of Origin" secondary={this.props.selectedLC.countryOfOrigin} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Currency Code" secondary={this.props.selectedLC.currencyCode} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Credit Available By" secondary={this.props.selectedLC.creditAvailableBy} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Credit Available With" secondary={this.props.selectedLC.creditAvailableWith} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }}>
              <ListItemText primary="Charge to Beneficiary" secondary={this.props.selectedLC.chargesToBeneficiary} />
            </ListItem>
            <ListItem style={{ paddingRight: '0' }} >
              <ListItemText primary="Latest Date of Shipment" secondary={this.props.selectedLC.latestDateOfShipment} />
            </ListItem>
          </div>
          <div style={{display: 'flex'}}>
           <a href={`${BackEnd_URL}${this.props.selectedLC.LCinvoice}`} target="blank" className={classes.textStyle} ><Button variant="contained" size="small"style={{marginRight: 10, marginLeft: 5}} >View LC Invoive </Button></a> 
            <a href={`${BackEnd_URL}${this.props.selectedLC.LCinsurance}`} target="blank" className={classes.textStyle}><Button variant="contained" size="small" style={{marginRight: 10}}> View LC Insurance </Button></a>
            <a href={`${BackEnd_URL}${this.props.selectedLC.LCotherDocs}`} target="blank" className={classes.textStyle}> <Button variant="contained" size="small" style={{marginRight: 10}}>View Other Documents </Button></a>
            {this.props.selectedLC.billsDocs !== '' && <a href={`${BackEnd_URL}${this.props.selectedLC.billsDocs}`} target="blank" className={classes.textStyle}><Button variant="contained" size="small" style={{marginRight: 5}}> View Bills</Button></a>}
            </div>
        </div>
      </Dialog >
    );
  }
}

ViewDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  selectedLC: PropTypes.object,
};

const ViewDialogWrapped = withStyles(styless)(ViewDialog);

const emails = ['username@gmail.com', 'user02@gmail.com'];
const styles = {
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
};

class SimpleDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      LCdocs: [],
    };
  }
  handleClose = () => {
    this.props.onClose(this.props.selectedValue);
  };

  handleListItemClick = value => {
    this.props.onClose(value);
  };

  onDropDocs = (picture) => {
    console.log(this.state.LCdocs)
    this.setState({
      LCdocs: this.state.LCdocs.concat(picture),
    });
  }

  onUploadDocs = (event) => {
    if (this.state.LCdocs.length > 0) {
      BackendServices.uploadDoc(this.props.getToken().token, this.state.LCdocs[0])
        .then(res => {
          console.log(res);
          this.handleClose();
        }, error => {
          console.log(error);
        });
    }
    else {
      alert("Please upload the required document.")
    }
    event.preventDefault();
  }


  render() {
    const { classes, onClose, selectedValue, ...other } = this.props;

    return (
      <Dialog onClose={this.handleClose} aria-labelledby="simple-dialog-title" {...other}>
        <div style={{ height: '300px', marginLeft: "30%" }}>
          <Grid item xs={12} sm={6} justify="center" style={{ width: "300px" }}>
            <ImageUploader
              withIcon={true}
              withPreview={true}
              buttonText='Upload Bills'
              onChange={this.onDropDocs}
              imgExtension={['.jpg', '.gif', '.png']}
              maxFileSize={5242880}
            />
            <Button color="primary"
              variant="contained"
              onClick={(e) => {
                this.onUploadDocs(e);
                this.props.handleUploadBillsUnderLC(this.state.LCdocs[0].name);
              }}>
              Upload
                    </Button>
          </Grid>

        </div>
      </Dialog>
    );
  }
}

SimpleDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  selectedValue: PropTypes.string,
};

const SimpleDialogWrapped = withStyles(styles)(SimpleDialog);


const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit,
    overflowY: 'auto',
  },
  highlight:
    theme.palette.type === 'light'
      ? {
        color: theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85),
      }
      : {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark,
      },
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    color: theme.palette.text.secondary,
  },
  title: {
    flex: '0 0 auto',
  },
});

class InboxTableToolbar extends React.Component {
  constructor(props) {
    super(props);
    this.handleForwardLC = this.handleForwardLC.bind(this);
    this.handleApproveLC = this.handleApproveLC.bind(this);
    this.handleUploadBillsUnderLC = this.handleUploadBillsUnderLC.bind(this);
    this.handleVerifyBillsUnderLC = this.handleVerifyBillsUnderLC.bind(this);
    this.handleEditLC = this.handleEditLC.bind(this);
    this.handleReleaseBills = this.handleReleaseBills.bind(this);
    this.closeSimplert = this.closeSimplert.bind(this);
    this.openModal = this.openModal.bind(this);
    this.getToken = this.getToken.bind(this);
  }
  state = {
    open: false,
    opens: false,
    selectedValue: emails[1],
    showAlert: false,
    alertType: '',
    alertTitle: '',
    loading: false,
  };

  getToken() {
    return JSON.parse(sessionStorage.getItem('user'));
  }

  handleClickOpen = () => {
    this.setState({
      open: true,
    });
  };

  handleClickOpens = () => {
    this.setState({
      opens: true,
    });
  };

  handleForwardLC = () => {
    BackendServices.ForwardLC(this.getToken().token, this.props.selectedLC.LC_id, this.props.selectedLC.pendingAt, this.props.selectedLC.status)
      .then(res => {
        console.log(res);
        this.creatAlert(true, "success", "LC has been successfully forwarded");
        //this.location.reload();

      }, error => {
        console.log(error.responseJSON.success)
        this.creatAlert(true, "error", "Oops! Something went wrong.");
      });
  }

  openModal = () => {
    this.creatAlert(true, "warning", "Are you sure you want to move the LC forward?");
  }

  handleApproveLC = (event) => {
    event.preventDefault();
    BackendServices.ApproveLC(this.getToken().token, this.props.selectedLC.LC_id, this.props.selectedLC.status)
      .then(res => {
        console.log(res);
        this.creatAlert(true, "success", "LC has been successfully approved");
      }, error => {
        this.creatAlert(true, "error", "Oops! Something went wrong.");
        console.log(error.responseJSON.success)
      });
  }

  handleUploadBillsUnderLC = (billsDocs) => {
    BackendServices.UploadBillsUnderLC(this.getToken().token, this.props.selectedLC.LC_id, this.props.selectedLC.pendingAt, this.props.selectedLC.status, billsDocs)
      .then(res => {
        console.log(res);
        this.creatAlert(true, "success", "Bills has been successfully uploaded");
      }, error => {
        this.creatAlert(true, "error", "Oops! Something went wrong.");
        console.log(error.responseJSON.success)
      });
  }


  handleVerifyBillsUnderLC = (event) => {
    event.preventDefault();
    BackendServices.VerifyBillsUnderLC(this.getToken().token, this.props.selectedLC.LC_id, this.props.selectedLC.pendingAt, this.props.selectedLC.status)
      .then(res => {
        console.log(res);
        this.creatAlert(true, "success", "Bills has been successfully verified.");
      }, error => {
        console.log(error.responseJSON.success)
        this.creatAlert(true, "error", "Oops! Something went wrong.");
      });
  }

  handleEditLC = (event) => {
    event.preventDefault();
    BackendServices.EditLC(this.getToken().token, this.props.selectedLC.LC_id)
      .then(res => {
        console.log(res);
        this.closeSimplert();
      }, error => {
        console.log(error.responseJSON.success);
        this.creatAlert(true, "error", "Something went wrong.");
      });
  }

  handleReleaseBills = (event) => {
    event.preventDefault();
    BackendServices.ReleaseBills(this.getToken().token, this.props.selectedLC.LC_id, this.props.selectedLC.status)
      .then(res => {
        console.log("release bills", res)
        this.creatAlert(true, "success", "Bills has been successfully released.");
      }, error => {
        console.log(error.responseJSON.success);
        this.creatAlert(true, "error", "Oops! Something went wrong.");
      });
  }

  handleReleaseFunds = (event) => {
    event.preventDefault();
    BackendServices.ReleaseFunds(this.getToken().token, this.props.selectedLC.LC_id, this.props.selectedLC.status)
      .then(res => {
        console.log("release funds", res);
        this.creatAlert(true, "success", "Funds has been successfully released.");

      }, error => {
        console.log(error.responseJSON.success);
        this.creatAlert(true, "error", "Oops! Something went wrong.");
      });
  }

  handleClose = value => {
    this.setState({ selectedValue: value, open: false });
  };

  handleCloses = value => {
    this.setState({ selectedValue: value, opens: false });
  };

  creatAlert(Alert, Type, Title) {
    this.setState(state => ({
      showAlert: Alert,
      alertType: Type,
      alertTitle: Title,
    }))
  };

  closeSimplert() {
    this.setState({ showAlert: false });
    window.location.reload();
  };

  render() {
    const { selectedLC, numSelected, classes } = this.props;
    return (
      <Toolbar
        className={classNames(classes.root, {
          [classes.highlight]: numSelected > 0,
        })}
      >
        <Simplert
          showSimplert={this.state.showAlert}
          type={this.state.alertType}
          title={this.state.alertTitle}
          disableOverlayClick={true}
          onClose={this.closeSimplert}
          customConfirmBtnClass='btn-warning'
        />

        <div className={classes.title}>
          {numSelected !== 0 ? (
            <Typography color="inherit" variant="subtitle1">
              LC selected to perform Actions
            </Typography>
          ) : (
              <Typography variant="h6" id="tableTitle">
                Inbox / Track
            </Typography>
            )}
        </div>
        <div className={classes.spacer} />
        <div className={classes.actions}>
          <SimpleDialogWrapped
            selectedValue={this.state.selectedValue}
            open={this.state.open}
            onClose={this.handleClose}
            handleUploadBillsUnderLC={this.handleUploadBillsUnderLC}
            getToken={this.getToken}
          />
          <ViewDialogWrapped
            selectedLC={this.props.selectedLC}
            open={this.state.opens}
            onClose={this.handleCloses}
          />
          {numSelected !== 0 && (
            <div style={{ display: 'flex' }}>
              <Tooltip title="View">
                <IconButton aria-label="View Full Description" onClick={this.handleClickOpens}>
                  <ViewIcon />
                </IconButton>
              </Tooltip>
              {selectedLC.pendingAt === 'Issuing Bank' && (
                <React.Fragment>
                  {/* {selectedLC.status === 'Requested' && <Tooltip title="Edit LC">
                    <IconButton aria-label="Edit LC">
                      <Button onClick={(event) => { this.handleEditLC(event) }}> <Create />      </Button>
                    </IconButton>
                  </Tooltip>} */}
                  {selectedLC.status === 'Requested' && <Tooltip title="Forward LC">
                    <IconButton aria-label="Forward LC">
                      <Button onClick={(event) => {this.handleForwardLC(event) }}>  <TrendingFlatIcon /> </Button>
                    </IconButton>
                  </Tooltip>}
                  {selectedLC.status === 'Bills Verified by Advising Bank' && <Tooltip title="Release Fund">
                    <IconButton aria-label="Release Bills">
                      <Button onClick={(event) => { this.handleReleaseFunds(event) }}>  <Funds />      </Button>
                    </IconButton>
                  </Tooltip>}
                  {selectedLC.status === 'Funds Released' && <Tooltip title="Release Bill">
                    <IconButton aria-label="Release Bills">
                      <Button onClick={(event) => { this.handleReleaseBills(event) }}>  <HowToVote />      </Button>
                    </IconButton>
                  </Tooltip>}
                </React.Fragment>
              )}
              {selectedLC.pendingAt === 'Advising Bank' && (
                <React.Fragment>
                  {selectedLC.status === 'Issuing Bank Validated' && <Tooltip title="Forward LC">
                    <IconButton aria-label="Forward LC">
                      <Button onClick={(event) => {this.handleForwardLC(event) }}>  <TrendingFlatIcon /> </Button>
                    </IconButton>
                  </Tooltip>}
                  {selectedLC.status === 'Bills Uploaded by Beneficiary' && <Tooltip title="Verify Bills">
                    <IconButton aria-label="Verify Bills">
                      <Button onClick={(event) => { this.handleVerifyBillsUnderLC(event) }}> <AssignmentTurnedIn />      </Button>
                    </IconButton>
                  </Tooltip>}
                </React.Fragment>
              )}

              {selectedLC.pendingAt === 'Beneficiary' && (
                <React.Fragment>
                  {selectedLC.status === 'Advising Bank Validated' && <Tooltip title="Approve">
                    <IconButton aria-label="Approve">
                      <Button onClick={(event) => { this.handleApproveLC(event) }}>  <DoneOutline />      </Button>
                    </IconButton>
                  </Tooltip>}
                  {selectedLC.status === 'Beneficiary Approved' && <Tooltip title="Upload">
                    <IconButton aria-label="Upload">
                      <Button onClick={this.handleClickOpen}>  <VerticalAlignTop />      </Button>
                    </IconButton>
                  </Tooltip>}
                </React.Fragment>
              )}
            </div>
          )}
        </div>
      </Toolbar>
    );
  }
}


export default withStyles(toolbarStyles)(InboxTableToolbar);
