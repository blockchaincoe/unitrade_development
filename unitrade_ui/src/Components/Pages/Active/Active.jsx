import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import ActiveTableHead from './ActiveTableHead';
import ActiveTableToolbar from './ActiveTableToolbar';


let counter = 0;
function createData(lc_id, applicant, doi, doe, amount, issuingbank, beneficiary) {
  counter += 1;
  return { id: counter, lc_id, applicant, doi, doe, amount, issuingbank, beneficiary };
}

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 1020,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
});

class Active extends React.Component {
  state = {
    order: 'asc',
    orderBy: 'calories',
    selected: 0,
    data: [
      createData('LC-1', 'dena bhai', '11/11/11', '11/11/11', 3.7, 'dena bank', 'lena bhai'),
      createData('LC-2', 'dena bhai','11/11/11', '11/11/11', 3.7, 'dena bank', 'lena bhai'),
      createData('LC-4', 'dena bhai','11/11/11', '11/11/11', 3.7, 'dena bank', 'lena bhai'),
      createData('LC-5', 'dena bhai','11/11/11', '11/11/11', 3.7, 'dena bank', 'lena bhai'),
      createData('LC-6', 'dena bhai','11/11/11', '11/11/11', 3.7, 'dena bank', 'lena bhai'),
      createData('LC-7', 'dena bhai','11/11/11', '11/11/11', 3.7, 'dena bank', 'lena bhai'),
      createData('LC-8', 'dena bhai','11/11/11', '11/11/11', 3.7, 'dena bank', 'lena bhai'),
      createData('LC-9', 'dena bhai','11/11/11', '11/11/11', 3.7, 'dena bank', 'lena bhai'),
      createData('LC-10', 'dena bhai','11/11/11', '11/11/11', 3.7, 'dena bank', 'lena bhai'),
      createData('LC-11', 'dena bhai','11/11/11', '11/11/11', 3.7, 'dena bank', 'lena bhai'),
      createData('LC-12', 'dena bhai','11/11/11', '11/11/11', 3.7, 'dena bank', 'lena bhai'),
      createData('LC-13', 'dena bhai','11/11/11', '11/11/11', 3.7, 'dena bank', 'lena bhai'),
      createData('LC-14', 'dena bhai','11/11/11', '11/11/11', 3.7, 'dena bank', 'lena bhai'),
      createData('LC-15', 'dena bhai','11/11/11', '11/11/11', 3.7, 'dena bank', 'lena bhai'),
    ],
    selectedLC: {},
    page: 0,
    rowsPerPage: 5,
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  };


  handleClick = (event, id, LC) => {
    event.target.checked ? this.setState({ selected: id, selectedLC: LC}) : this.setState({ selected: 0 });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  isSelected = id => this.state.selected === id ? true : false;

  render() {
    const { classes } = this.props;
    const { data, order, orderBy, rowsPerPage, page } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

    return (
      <Paper className={classes.root}>
        <ActiveTableToolbar selectedLC={this.state.selectedLC} numSelected={this.state.selected}/>
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <ActiveTableHead
              numSelected={this.state.selected}
              order={order}
              orderBy={orderBy}
              onRequestSort={this.handleRequestSort}
              rowCount={data.length}
            />
            <TableBody>
              {
                stableSort(this.props.activeLC , getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(n => {
                  const isSelected = this.isSelected(n.LC_id);
                  return (
                    <TableRow
                      hover
                      onChange={event => this.handleClick(event, n.LC_id, n)}
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={n.LC_id}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox checked={isSelected} style={{ color: 'black' }}/>
                      </TableCell>
                      <TableCell component="th" scope="row" padding="none">
                        {n.LC_id}
                      </TableCell>
                      <TableCell numeric>{n.applicantFirstName}&nbsp;{n.applicantLastName}</TableCell>
                      <TableCell numeric>{n.status}</TableCell>
                      <TableCell numeric>{n.pendingAt}</TableCell>
                      <TableCell numeric>{n.amount}</TableCell>
                      <TableCell numeric>{n.issuingBankName}</TableCell>
                      <TableCell numeric>{n.beneficiaryFirstName}&nbsp;{n.beneficiaryLastName}</TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    );
  }
}

Active.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Active);