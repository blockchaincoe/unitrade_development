import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Button, MenuItem, FormControl, InputLabel, NativeSelect, Input } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ImageUploader from 'react-images-upload';
import BackendServices from "../../Services/BackendServices";
import Simplert from 'react-simplert';
import { withRouter } from 'react-router';
import Autocomplete from './Autocomplete';
import { css } from 'react-emotion';
import { ClipLoader } from 'react-spinners';

const override = css`
    display: block;
    margin: -3px 7px -3px 14px;
    border-color: red;
`;

const styles = theme => ({
  container: {
    marginTop: theme.spacing.unit * 4,
  },
  flexContainer: {
  },
  root: {
    ...theme.mixins.gutters(),
    width: '70%',
    marginTop: theme.spacing.unit * 1,
    marginBottom: theme.spacing.unit * 1,
    marginRight: theme.spacing.unit * 8,
  },
  margin: {
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 1,
    float: 'right',
  }
});

class RequestLcForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showAlert: false,
      alertType: '',
      alertTitle: '',
      beneficiary: '',
      typeOfLC: '',
      applicantFirstNam: '',
      applicantLastName: '',
      applicantAddress: '',
      beneficiaryFirstName: '',
      beneficiaryLastName: '',
      beneficiaryAddress: '',
      issuingBank: '',
      issuingBankName: '',
      advisingBankName: '',
      issuingBankIFSCcode: '',
      advisingBankIFSCcode: '',
      issuingBankBranch: '',
      advisingBankBranch: '',
      advisingBank: '',
      dateOfIssue: '',
      dateOfExpiry: '',
      currencyCode: '',
      amount: '',
      percentageCreditAmountTolerance: '',
      maximumCreditAmount: '',
      additionalAmountsCovered: '',
      requestType: '',
      subrequestType: '',
      creditAvailableWith: '',
      creditAvailableBy: '',
      usanceOfDrafts: '',
      drawee: '',
      defferedPaymentDetails: '',
      partialShipment: '',
      transshipment: '',
      shipmentFrom: '',
      portOfLoading: '',
      portOfDischarge: '',
      placeOfDelivery: '',
      latestDateOfShipment: '',
      typeOfGoods: '',
      importLicense: '',
      exportImportCodeNumber: '',
      POnumber: '',
      countryOfOrigin: '',
      documentsRequired: '',
      addtionalConditions: '',
      chargesToBeneficiary: '',
      periodOfPresentationOfDocuments: '',
      LCinvoice: '',
      LCinsurance: '',
      LCotherDocs: '',
      billsDocs: '',
      LCdocs: [],
      BillsDocs: [],
      items: [],
      autoCompleteAdvisingValues: {},
      autoCompleteIssuingValues: {},
      autoCompleteBeneficiaryValues: {},
      loading: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.closeSimplert = this.closeSimplert.bind(this);
  }

  handleSubmit = (event) => {
    this.setState({
      loading: true,
    })
    if (this.state.LCdocs.length === 3) {
      BackendServices.requestLC(
        this.getToken().token,
        this.state.autoCompleteBeneficiaryValues._id,
        this.state.typeOfLC,
        this.getToken().first_name,
        this.getToken().last_name,
        this.getToken().email,
        this.state.autoCompleteBeneficiaryValues.first_name,
        this.state.autoCompleteBeneficiaryValues.last_name,
        this.state.autoCompleteBeneficiaryValues.address,
        this.state.autoCompleteIssuingValues._id,
        this.state.autoCompleteIssuingValues.name,
        this.state.autoCompleteAdvisingValues.name,
        this.state.autoCompleteIssuingValues.IFSC,
        this.state.autoCompleteAdvisingValues.IFSC,
        this.state.autoCompleteIssuingValues.branch,
        this.state.autoCompleteAdvisingValues.branch,
        this.state.autoCompleteAdvisingValues._id,
        this.state.dateOfIssue,
        this.state.dateOfExpiry,
        this.state.currencyCode,
        this.state.amount,
        this.state.percentageCreditAmountTolerance,
        this.state.maximumCreditAmount,
        this.state.additionalAmountsCovered,
        this.state.requestType,
        this.state.subrequestType,
        this.state.creditAvailableWith,
        this.state.creditAvailableBy,
        this.state.usanceOfDrafts,
        this.state.drawee,
        this.state.defferedPaymentDetails,
        this.state.partialShipment,
        this.state.transshipment,
        this.state.shipmentFrom,
        this.state.portOfLoading,
        this.state.portOfDischarge,
        this.state.placeOfDelivery,
        this.state.latestDateOfShipment,
        this.state.typeOfGoods,
        this.state.importLicense,
        this.state.exportImportCodeNumber,
        this.state.POnumber,
        this.state.countryOfOrigin,
        this.state.documentsRequired,
        this.state.addtionalConditions,
        this.state.chargesToBeneficiary,
        this.state.periodOfPresentationOfDocuments,
        this.state.LCdocs[0].name,
        this.state.LCdocs[1].name,
        this.state.LCdocs[2].name,
        this.state.billsDocs,
      )
        .then(res => {
          console.log(res);
          BackendServices.newLC(this.getToken().token, res.data.LC_id)
            .then(res => {
              this.setState({
                loading: false,
              })
              console.log("New Lc generated.", res);
              this.creatAlert(true, "success", "New LC requested.");

            }, error => {
              this.setState({
                loading: false,
              })
              console.log(error.responseJSON.success)
            });
        }, error => {
          console.log(error)
          if (error.responseJSON.error.error)
            this.creatAlert(true, "error", JSON.stringify(error.responseJSON.error.error));
          else {
            this.creatAlert(true, "error", "Somethig went wrong.");
          }
        });
    }
    else {
      alert("Please check the required documents to upload.")
    }
    event.preventDefault();
  }

  getToken() {
    return JSON.parse(sessionStorage.getItem('user'));
  }

  creatAlert(Alert, Type, Title) {
    this.setState(state => ({
      showAlert: Alert,
      alertType: Type,
      alertTitle: Title,
    }))
  };

  closeSimplert() {
    this.setState({ showAlert: false });
    sessionStorage.setItem('activePage', 'active');
  }

  onDropDocs = (picture) => {
    console.log(this.state.LCdocs)
    this.setState({
      LCdocs: this.state.LCdocs.concat(picture),
    });

  }

  onUploadDocs = (event, docs) => {

    if (this.state.LCdocs.length > 0) {
      BackendServices.uploadDoc(this.getToken().token, docs)
        .then(res => {
          this.creatAlert(true, "success", "Document Uploaded.");
        }, error => {
          console.log(error)
          if (error.responseJSON.error.error)
            this.creatAlert(true, "error", JSON.stringify(error.responseJSON.error.error));
          else {
            this.creatAlert(true, "error", "Somethig went wrong.");
          }
        });
    }
    else {
      alert("Please upload the required document.")
    }
    event.preventDefault();
  }

  onDropBills = (picture) => {
    console.log(this.state.BillsDocs);
    this.setState({
      BillsDocs: this.state.BillsDocs.concat(picture),
    });
  }

  onUploadBills = (event) => {
    BackendServices.uploadDoc(this.getToken().token, this.state.BillsDocs)
      .then(res => {
        this.creatAlert(true, "success", "Document has been successfully uploaded.");
      }, error => {
        console.log(error)
        if (error.responseJSON.error.error)
          this.creatAlert(true, "error", JSON.stringify(error.responseJSON.error.error));
        else {
          this.creatAlert(true, "error", "Oops! Somethig went wrong.");
        }
      });
    event.preventDefault();
  }

  AutoCompleteBeneficiary = (item) => {
    this.setState({ autoCompleteBeneficiaryValues: item });
  }

  AutoCompleteAdvisingBank = (item) => {
    this.setState({ autoCompleteAdvisingValues: item });
  }

  AutoCompleteIssuingBank = (item) => {
    this.setState({ autoCompleteIssuingValues: item });
  }

  handleChange(event) {
    var obj = {};
    obj[event.target.id] = event.target.value;
    this.setState(obj);
  }

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <Simplert
          showSimplert={this.state.showAlert}
          type={this.state.alertType}
          title={this.state.alertTitle}
          disableOverlayClick={true}
          onClose={this.closeSimplert} />
        <div className={classes.container}>
          <Typography variant="h4" gutterBottom align='center' style={{marginBottom: '35px'}}>  Letter Of Credit</Typography>
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.heading}>Application Details</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <div className={classes.root}>
                <fieldset>
                  <legend>
                    <Typography variant="h6" gutterBottom>Applicant Details</Typography></legend>
                  <Typography variant="caption" gutterBottom>
                    <i> Fields marked with * are Mandatory.</i>
                  </Typography>
                  <Grid container spacing={24}>
                    <Grid item xs={12} sm={6}>
                      <FormControl style={{ width: '100%' }}>
                        <InputLabel htmlFor="typeOfLC">Type of LC</InputLabel>

                        <NativeSelect
                          fullWidth
                          required
                          value={this.state.typeOfLC}
                          onChange={this.handleChange}
                          input={<Input name="typeOfLC" id="typeOfLC" />}
                        >
                          <option></option>
                          <option value='REVOCABLE'>Revocable</option>
                          <option value='IRREVOCABLE'>Irrevocable</option>
                          <option value='BACK TO BACK'>Back to back</option>
                        </NativeSelect>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="dateOfIssue"
                        name="dateOfIssue"
                        label="Date of Issue"
                        type="date"
                        value={this.state.dateOfIssue}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="dateOfExpiry"
                        name="dateOfExpiry"
                        label="Date of Expiry"
                        type="date"
                        value={this.state.dateOfExpiry}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <FormControl style={{ width: '100%' }}>
                        <InputLabel htmlFor="currencyCode">Currency Code</InputLabel>

                        <NativeSelect
                          fullWidth
                          required
                          value={this.state.currencyCode}
                          onChange={this.handleChange}
                          input={<Input name="currencyCode" id="currencyCode" />}
                        >
                          <option></option>
                          <option value='INR'>INR</option>
                          <option value='USD'>USD</option>
                          <option value='EUR'>EUR</option>
                        </NativeSelect>
                      </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="amount"
                        type="number"
                        name="amount"
                        label="Amount"
                        value={this.state.amount}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="percentageCreditAmountTolerance"
                        name="percentageCreditAmountTolerance"
                        label="% Credit Amount Tolerance"
                        type="number"
                        value={this.state.percentageCreditAmountTolerance}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="maximumCreditAmount"
                        name="maximumCreditAmount"
                        label="Maximum Credit Amount"
                        type="number"
                        value={this.state.maximumCreditAmount}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="additionalAmountsCovered"
                        name="additionalAmountsCovered"
                        label="Additional Amounts Covered"
                        type="number"
                        value={this.state.additionalAmountsCovered}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <FormControl style={{ width: '100%' }} >
                        <InputLabel htmlFor="requestType">Request Type</InputLabel>
                        <NativeSelect
                          fullWidth
                          required
                          value={this.state.requestType}
                          onChange={this.handleChange}
                          input={<Input name="requestType" id="requestType" />}
                        >
                          <option></option>
                          <option value='EXPORT'>Export</option>
                          <option value='IMPORT'>Import</option>
                        </NativeSelect>
                      </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <FormControl style={{ width: '100%' }}>
                        <InputLabel htmlFor="subrequestType">Sub-Request Type</InputLabel>
                        <NativeSelect
                          fullWidth
                          required
                          value={this.state.subrequestType}
                          onChange={this.handleChange}
                          input={<Input name="subrequestType" id="subrequestType" />}
                        >
                          <option></option>
                          <option value='ISSUANCE '>Issuance</option>
                          <option value='AMENDMENT'>Amendment</option>
                          <option value='CANCELLATION'>Cancellation</option>
                          <option value='DRAFT'>Draft</option>
                        </NativeSelect>
                      </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={4}>
                      <FormControl style={{ width: '100%' }}>
                        <InputLabel htmlFor="beneficiary">Beneficiary</InputLabel>
                        <Autocomplete for="beneficiary" AutoCompleteValue={this.AutoCompleteBeneficiary} />
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} sm={4}>
                      <FormControl style={{ width: '100%' }}>
                        <InputLabel htmlFor="issuingBank">Issuing Bank</InputLabel>
                        <Autocomplete for="Bank" AutoCompleteValue={this.AutoCompleteIssuingBank} />
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} sm={4}>
                      <FormControl style={{ width: '100%' }}>
                        <InputLabel htmlFor="advisingBank" >Advising Bank</InputLabel>
                        <Autocomplete for="Bank" AutoCompleteValue={this.AutoCompleteAdvisingBank} />
                      </FormControl>
                    </Grid>
                  </Grid>
                </fieldset>
              </div>
            </ExpansionPanelDetails>type="number"
          </ExpansionPanel>

          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.heading}>Documents Details</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <div className={classes.root}>
                <fieldset>
                  <legend>
                    <Typography variant="h6" gutterBottom align='left'>
                      Goods Documents Conditions Details
              </Typography>
                  </legend>
                  <Typography variant="caption" gutterBottom>
                    <i> Fields marked with * are Mandatory.</i>
                  </Typography>
                  <Grid container spacing={24}>
                    <Grid item xs={12} sm={6}>
                      <FormControl style={{ width: '100%' }}>
                        <InputLabel htmlFor="typeOfGoods">Type of Goods</InputLabel>
                        <NativeSelect
                          fullWidth
                          required
                          value={this.state.typeOfGoods}
                          onChange={this.handleChange}
                          input={<Input name="typeOfGoods" id="typeOfGoods" />}
                        >
                          <option></option>
                          <option value='NON CAPITAL '>Non Capital</option>
                          <option value='CAPITAL'>Capital</option>
                        </NativeSelect>
                      </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="importLicense"
                        name="importLicense"
                        label="Import License"
                        value={this.state.importLicense}
                        onChange={this.handleChange}
                        fullWidth
                        autoComplete="address-level2"
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField id="exportImportCodeNumber" name="exportImportCodeNumber" value={this.state.exportImportCodeNumber} onChange={this.handleChange} label="Export/Import Code Number" fullWidth />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="POnumber"
                        name="POnumber"
                        label="PO number"
                        value={this.state.POnumber}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="countryOfOrigin"
                        name="countryOfOrigin"
                        label="Country of Origin"
                        value={this.state.countryOfOrigin}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="addtionalConditions"
                        name="addtionalConditions"
                        label="Addtional Conditions"
                        value={this.state.addtionalConditions}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={6} justify="center" style=
                      {{ border: 'solid 1px black', margin: '5px' }}>
                      <ImageUploader
                        withIcon={true}
                        withPreview={true}
                        buttonText='Upload Invoice'
                        onChange={this.onDropDocs}
                        imgExtension={['.jpg', '.gif', '.png']}
                        maxFileSize={5242880}
                        required
                      />
                      <Button style=
                        {{ float: 'right' }} color="primary" variant="contained" onClick={e => this.onUploadDocs(e, this.state.LCdocs[0])}>
                        Upload
                    </Button>
                    </Grid>
                    <Grid item xs={12} sm={6} justify="center" style=
                      {{ border: 'solid 1px black', margin: '5px' }}>
                      <ImageUploader
                        withIcon={true}
                        withPreview={true}
                        buttonText='Upload Insurance'
                        onChange={this.onDropDocs}
                        imgExtension={['.jpg', '.gif', '.png']}
                        maxFileSize={5242880}
                      />
                      <Button style=
                        {{ float: 'right' }} color="primary" variant="contained" onClick={e => this.onUploadDocs(e, this.state.LCdocs[1])}>
                        Upload
                    </Button>
                    </Grid>
                    <Grid item xs={12} sm={6} justify="center" style=
                      {{ border: 'solid 1px black', margin: '5px' }}>
                      <ImageUploader
                        withIcon={true}
                        withPreview={true}
                        buttonText='Upload Other Documents'
                        onChange={this.onDropDocs}
                        imgExtension={['.jpg', '.gif', '.png']}
                        maxFileSize={5242880}
                      />
                      <Button style=
                        {{ float: 'right' }} color="primary" variant="contained" onClick={e => this.onUploadDocs(e, this.state.LCdocs[2])}>
                        Upload
                    </Button>
                    </Grid>
                  </Grid>
                </fieldset>
              </div>

            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.heading}>Payment Details</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <div className={classes.root}>
                <fieldset>
                  <legend>
                    <Typography variant="h6" gutterBottom align='left'>
                      Payment Details
              </Typography>
                  </legend>
                  <Typography variant="caption" gutterBottom>
                    <i> Fields marked with * are Mandatory.</i>
                  </Typography>
                  <Grid container spacing={24}>
                    <Grid item xs={12} sm={6}>
                      <FormControl style={{ width: '100%' }}>
                        <InputLabel htmlFor="creditAvailableWith">Credit Available With</InputLabel>
                        <NativeSelect
                          fullWidth
                          required
                          value={this.state.creditAvailableWith}
                          onChange={this.handleChange}
                          input={<Input name="creditAvailableWith" id="creditAvailableWith" />}
                        >
                          <option></option>
                          <option value='ANY BANK '>Any Bank</option>
                          <option value='ADVISING BANK'>Advising Bank</option>
                        </NativeSelect>
                      </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <FormControl style={{ width: '100%' }}>
                        <InputLabel htmlFor="creditAvailableBy">Credit Available By</InputLabel>
                        <NativeSelect
                          fullWidth
                          required
                          value={this.state.creditAvailableBy}
                          onChange={this.handleChange}
                          input={<Input name="creditAvailableBy" id="creditAvailableBy" />}
                        >
                          <option></option>
                          <option value='ACCEPTANCE  '>Acceptance</option>
                          <option value='DEFPAYMENT'>Def-Payment</option>
                          <option value='NEGOTIATION'>Negotiation</option>
                          <option value='SIGHTPAYMENT'>Sight Payment</option>
                        </NativeSelect>
                      </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <FormControl style={{ width: '100%' }} >
                        <InputLabel htmlFor="usanceOfDrafts">Issuance of Drafts</InputLabel>
                        <NativeSelect
                          fullWidth
                          required
                          value={this.state.usanceOfDrafts}
                          onChange={this.handleChange}
                          input={<Input name="usanceOfDrafts" id="usanceOfDrafts" />}
                        >
                          <option></option>
                          <option value='SIGHT  '>Sight</option>
                          <option value='USANSE'>Usanse</option>
                        </NativeSelect>
                      </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <FormControl style={{ width: '100%' }}>
                        <InputLabel htmlFor="drawee">Type of Drawee</InputLabel>
                        <NativeSelect
                          fullWidth
                          required
                          value={this.state.drawee}
                          onChange={this.handleChange}
                          input={<Input name="drawee" id="drawee" />}
                        >
                          <option></option>
                          <option value='ISSUING BANK  '>Issuing Bank</option>
                          <option value='NEGOTIATING BANK'>Negotiating Bank</option>
                        </NativeSelect>
                      </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="defferedPaymentDetails"
                        name="defferedPaymentDetails"
                        label="Deffered Payment Details"
                        value={this.state.defferedPaymentDetails}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>
                  </Grid>
                </fieldset>
              </div>

            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.heading}>Shipments Details</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <div className={classes.root}>
                <fieldset>
                  <legend>
                    <Typography variant="h6" gutterBottom align='left'>
                      Shipment Details
              </Typography>
                  </legend>
                  <Typography variant="caption" gutterBottom>
                    <i> Fields marked with * are Mandatory.</i>
                  </Typography>
                  <Grid container spacing={24}>
                    <Grid item xs={12} sm={6}>
                      <FormControl style={{ width: '100%' }}>
                        <InputLabel htmlFor="partialShipment">Partial Shipment</InputLabel>
                        <NativeSelect
                          fullWidth
                          required
                          value={this.state.partialShipment}
                          onChange={this.handleChange}
                          input={<Input name="partialShipment" id="partialShipment" />}
                        >
                          <option></option>
                          <option value='PROHIBITED '>Prohibited</option>
                          <option value='PERMITTED'>Permitted</option>
                        </NativeSelect>
                      </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <FormControl style={{ width: '100%' }}>
                        <InputLabel htmlFor="transshipment">Trans-Shipment</InputLabel>
                        <NativeSelect
                          fullWidth
                          required
                          value={this.state.transshipment}
                          onChange={this.handleChange}
                          input={<Input name="transshipment" id="transshipment" />}
                        >
                          <option></option>
                          <option value='PROHIBITED '>Prohibited</option>
                          <option value='PERMITTED'>Permitted</option>
                        </NativeSelect>
                      </FormControl>
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="shipmentFrom"
                        name="shipmentFrom"
                        label="Shipment From"
                        value={this.state.shipmentFrom}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="portOfLoading"
                        name="portOfLoading"
                        label="Port of Landing"
                        value={this.state.portOfLoading}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="portOfDischarge"
                        name="portOfDischarge"
                        label="Port of Discharge"
                        value={this.state.portOfDischarge}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="placeOfDelivery"
                        name="placeOfDelivery"
                        label="Place of Delivery"
                        value={this.state.placeOfDelivery}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="latestDateOfShipment"
                        name="latestDateOfShipment"
                        label="Latest Date of Shipment"
                        type="date"
                        value={this.state.latestDateOfShipment}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>
                  </Grid>
                </fieldset>
              </div>

            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.heading}>Other Details</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <div className={classes.root}>
                <fieldset>
                  <legend>
                    <Typography variant="h6" gutterBottom align='left'>
                      Other Details
              </Typography>
                  </legend>
                  <Typography variant="caption" gutterBottom>
                    <i> Fields marked with * are Mandatory.</i>
                  </Typography>
                  <Grid container spacing={24}>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="chargesToBeneficiary"
                        name="chargesToBeneficiary"
                        label="Charges To Beneficiary"
                        type="number"
                        value={this.state.chargesToBeneficiary}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="periodOfPresentationOfDocuments"
                        name="periodOfPresentationOfDocuments"
                        label="Period of Presentation of Documents"
                        value={this.state.periodOfPresentationOfDocuments}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>
                  </Grid>
                </fieldset>
              </div>

            </ExpansionPanelDetails>
          </ExpansionPanel>
          <Button className={classes.margin} variant="contained" color="primary" onClick={e => this.handleSubmit(e)} >
            Submit
            <div className='sweet-loading'>
              <ClipLoader
                className={override}
                sizeUnit={"px"}
                size={20}
                color={'white'}
                loading={this.state.loading}
              />
            </div>
          </Button>
        </div>
      </React.Fragment>
    );
  }
}

RequestLcForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(RequestLcForm));