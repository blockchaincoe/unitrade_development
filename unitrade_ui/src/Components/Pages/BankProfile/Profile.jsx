import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Paper, Grid } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import UserDetails from './UserDetails';
import BackendServices from "../../../Services/BackendServices";
import { ProfileStyles } from '../../styles';
import { NavLink } from 'react-router-dom';

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      IFSC: '',
      branch: '',
      city: '',
      province: '',
      country: '',
    };
    this.getToken = this.getToken.bind(this);
    this.componentWillMount = this.componentWillMount.bind(this);
  }

  getToken() {
    return JSON.parse(sessionStorage.getItem('user'));
  }

  componentWillMount() {
    BackendServices.getBankDetails(this.getToken().token)
      .then(res => {
        console.log(res.data.user)
        this.setState({
          name: res.data.user.name,
          email: res.data.user.email,
          IFSC: res.data.user.IFSC,
          branch: res.data.user.branch,
          city: res.data.user.city,
          province: res.data.user.state,
          country: res.data.user.country,
        })
      }, error => {
        console.log(error)
        this.props.history.push('/login');
        if (error.responseJSON.error.error)
          this.creatAlert(true, "error", JSON.stringify(error.responseJSON.error.error));
        else {
          this.creatAlert(true, "error", "Please Enter Valid Credentials.");
        }
      });
  }

  handleClickOpen = () => {
    this.setState({
      open: true,
    });
  };

  handleAddBeneficiary = () => {
    this.setState({
      addModal: true,
    });
  };

  handleClose = value => {
    this.setState({ selectedValue: value, open: false, addModal: false });
  };

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <CssBaseline />
        <main className={classes.layout}>
          <Paper className={classes.paper}>
            <Typography component="h1" variant="h4" align="center">
              Bank Details
            </Typography>
            <UserDetails values={this.state} />
            <Grid container justify="center" style={{ marginTop: 20 }}>
            {/* <NavLink to="/">
              <Button
                variant="contained"
                color="primary"
                className={classes.button}
                style={{ marginRight: 20 }}
              >
                Ok
              </Button>
              </NavLink> */}
            </Grid>
          </Paper>
        </main>
      </React.Fragment>
    );
  }
}

Profile.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(ProfileStyles)(Profile);