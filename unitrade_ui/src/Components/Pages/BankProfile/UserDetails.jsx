import React from 'react';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { UserDetailsStyles } from '../../styles'


class UserDetails extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
   return (
    <React.Fragment>
      <List disablePadding>   
          <ListItem className={UserDetailsStyles.listItem}>
            <ListItemText primary="Name" secondary="Name of the Bank" />
            <Typography variant="body2">{this.props.values.name}</Typography>
          </ListItem>
          <ListItem style={UserDetailsStyles.listItem}>
            <ListItemText primary="Branch" secondary="Branch of the Bank" />
            <Typography variant="body2">{this.props.values.branch}</Typography>
          </ListItem>
          <ListItem style={UserDetailsStyles.listItem}>
            <ListItemText primary="Email Address" secondary="Official E-Mail ID " />
            <Typography variant="body2">{this.props.values.email}</Typography>
          </ListItem>
          <ListItem style={UserDetailsStyles.listItem}>
            <ListItemText primary="IFSC Code" secondary="IFSC Code of the bank branch" />
            <Typography variant="body2">{this.props.values.IFSC}</Typography>
          </ListItem>
          <ListItem style={UserDetailsStyles.listItem}>
            <ListItemText primary="City" secondary="City where you live" />
            <Typography variant="body2">{this.props.values.city}</Typography>
          </ListItem>
          <ListItem style={UserDetailsStyles.listItem}>
            <ListItemText primary="State/Province" secondary="Province where you're from" />
            <Typography variant="body2">{this.props.values.province}</Typography>
          </ListItem>
          <ListItem style={UserDetailsStyles.listItem}>
            <ListItemText primary="Country" secondary="Country you belong to" />
            <Typography variant="body2">{this.props.values.country}</Typography>
          </ListItem>
      </List>
    </React.Fragment>
  );
}
}


export default UserDetails;