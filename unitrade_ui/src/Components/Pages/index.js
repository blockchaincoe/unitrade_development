import Login from './Login/Login';
import SignUp from './SignUp/SignUp';
import RequestLcForm from './RequestLcForm';
import BankDashboard from './BankDashboard';
import UserDashboard from './UserDashboard';
import ForgotPassword from './Login/ForgotPassword'

export {
    ForgotPassword, Login, SignUp, RequestLcForm, BankDashboard, UserDashboard,
}