import React, { Component } from 'react';
import { Grid, Paper, ListItem, List, ListItemText, FormControl, InputLabel, NativeSelect, Input } from '@material-ui/core';
import BackendServices from "../../Services/BackendServices";
import TextField from '@material-ui/core/TextField';
import './Autocomplete.css';

class Autocomplete extends Component {
  constructor(props) {
    super(props);
    //this.textInput = React.createRef();
    this.state = {
      text: '',
      items: [],
      show: true,
    }
  }
  

  componentDidMount() {
    this.setState({
      text: '',
    });
  }

  getToken() {
    return JSON.parse(sessionStorage.getItem('user'));
  }

  selectSuggestion = e => {
    e.preventDefault();
    const text = e.currentTarget.textContent;
    this.proceedAutoComplete(text);
  };

  proceedAutoComplete(text) {
    //alert(text);
    // this.textInput.value = text;
    this.setState({ text: text, show: false });
  }

  handleClick = e => {
    e.preventDefault();
    this.setState({ show: !this.state.show });
  }

  handleChange = e => {
    e.preventDefault();
    const { value } = e.target;
    this.props.for === 'beneficiary' && BackendServices.SearchBeneficiary(this.getToken().token, value)
      .then(res => {
        console.log(res.data)
        this.setState({
          text: value,
          items: res.data
        }, () => {
          let newState = this.state;
          if (value.length > 0) {
            newState = { ...newState, show: true };
          } else {
            newState = { ...newState, show: false };
          };
          newState = {
            ...newState,
            items: this.getFiltered(value)
          };
          this.setState(newState);
        });
      }, error => {
        console.log(error.responseJSON.success)
      });
      this.props.for === 'Bank' && BackendServices.SearchBank(this.getToken().token, value)
      .then(res => {
        console.log(res.data)
        this.setState({
          text: value,
          items: res.data
        }, () => {
          let newState = this.state;
          if (value.length > 0) {
            newState = { ...newState, show: true };
          } else {
            newState = { ...newState, show: false };
          };
          newState = {
            ...newState,
            items: this.getFilteredBank(value)
          };
          this.setState(newState);
        });
      }, error => {
        console.log(error.responseJSON.success)
      });
  };

  getFiltered(value) {
    return this.state.items.filter(i => i.first_name.toLowerCase().includes(value.toLowerCase()))
  }
  getFilteredBank(value) {
    return this.state.items.filter(i => i.name.toLowerCase().includes(value.toLowerCase()))
  }


  handleKeyPress = e => {
    if (e.key === 'Enter' && this.getFiltered(e.target.value).length === 1) {
      this.proceedAutoComplete(this.getFiltered(e.target.value));
    }
  }

  render() {
    const showSuggest = {
      display: this.state.show ? 'block' : 'none',
      background: '#ccc'
    }
    return (
      <React.Fragment>
        <Input type="text"
          className="autoCompleteInput"
          onChange={this.handleChange}
          // ref={ { this.textInput = Input; }}
          value={this.state.text}
          onClick={this.handleClick}
          onKeyPress={this.handleKeyPress}
        />
        <Paper className='suggestWrapper' style={showSuggest}>
          <List className='suggestAutocomplete'>
            {this.state.items.map((item, i) => {
              return <ListItem
                button
                key={i}
                onClick={(e) => {
                  this.selectSuggestion(e);
                  this.props.AutoCompleteValue(item);
                }}
              >
                <ListItemText>
                  {this.props.for && item.first_name}
                  {this.props.for && item.name}
                </ListItemText>
              </ListItem>
            })}

          </List>
        </Paper>
        </React.Fragment>
    )
  }
};


export default Autocomplete;