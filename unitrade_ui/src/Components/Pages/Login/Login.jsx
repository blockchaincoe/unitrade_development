import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import { NativeSelect } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import BankIcon from '@material-ui/icons/AccountBalanceRounded';
import UserIcon from '@material-ui/icons/Person';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
import CardActions from '@material-ui/core/CardActions';
import { NavLink } from 'react-router-dom';
import BackendServices from "../../../Services/BackendServices";
import Simplert from 'react-simplert';
import { withRouter } from 'react-router'
import { LoginStyles } from '../../styles'
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import { credentials } from '../../../constants'
import { css } from 'react-emotion';
import { ClipLoader } from 'react-spinners';

const override = css`
    display: block;
    margin: -3px -36px -5px 48px;
    border-color: red;
`;


class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      alertType: '',
      alertTitle: '',
      showAlert: false,
      email: '',
      password: 'User@123',
      isLoggedIn: false,
      value: 0,
      loading: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSignin = this.handleSignin.bind(this);
    this.closeSimplert = this.closeSimplert.bind(this);
  }

  creatAlert(Alert, Type, Title) {
    this.setState({
      showAlert: Alert,
      alertType: Type,
      alertTitle: Title
    })
  };

  handleSignin(event) {
    event.preventDefault();
    this.setState({
      loading: true,
    })
    BackendServices.SignIn(this.state.email, this.state.password)
      .then(res => {
        this.setState({
          loading: false,
        })
        console.log(res)
        this.props.handleLogin();
        sessionStorage.setItem('user', JSON.stringify(res.data));
        sessionStorage.setItem('userRole' , 'Applicant');
       sessionStorage.setItem('activePage' , 'active');
        this.props.history.push('/userdashboard');

      }, error => {
        this.setState({
          loading: false,
        })
        console.log(error.responseJSON.success)
        if (error.responseJSON.success)
          this.creatAlert(true, "error", error.responseJSON.error);
        else {
          this.creatAlert(true, "error", "Incorrect Email/Password.");
        }
        this.props.history.push('/');
      });
  };

  handleBankSignin(event) {
    event.preventDefault();
    BackendServices.BankSignIn(this.state.email, this.state.password)
    .then(res => {
       console.log(res)
       this.props.handleLogin();
       sessionStorage.setItem('user' , JSON.stringify(res.data));
       sessionStorage.setItem('userRole' , 'Issuing');
       sessionStorage.setItem('activePage' , 'active');
       this.props.history.push('/bankdashboard');

  }, error => {
      console.log(error.responseJSON.success)
      if (error.responseJSON.success) 
          this.creatAlert(true, "error", error.responseJSON.error);
        else {
          this.creatAlert(true, "error", "Incorrect Email/Password.");
        }
        this.props.history.push('/');
      });
  };

  closeSimplert() {
    this.setState({ showAlert: false });
  };

  handleChange(event) {
    var obj = {};
    obj[event.target.id] = event.target.value;
    this.setState(obj);
  };

  handleChangeTab = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <CssBaseline />
        <Simplert
          showSimplert={this.state.showAlert}
          type={this.state.alertType}
          title={this.state.alertTitle}
          disableOverlayClick={true}
          onClose={this.closeSimplert} />
        <main className={classes.layout}>
          <AppBar position="static" color="default" className={classes.margin}>
            <Tabs
              value={this.state.value}
              indicatorColor="primary"
              textColor="primary"
              onChange={this.handleChangeTab}
              centered
            >
              <Tab label="User Login" />
              <Tab label="Bank Login" />
            </Tabs>
          </AppBar>
          <Paper className={classes.paper}>
            <Avatar className={classes.avatar}>
              {this.state.value === 0 && <UserIcon />}
              {this.state.value === 1 && <BankIcon />}
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign in
          </Typography>
            <form className={classes.form}>
              <InputLabel htmlFor="email"> {this.state.value === 0 ? 'User ID' : 'Bank ID'}</InputLabel>
              <FormControl margin="normal" required fullWidth>

                <NativeSelect
                  fullWidth
                  required
                  value={this.state.email}
                  onChange={this.handleChange}
                  input={<Input name="email" id="email" />}
                > <option value="">---Please Select---</option>
                  {this.state.value === 0 && <option value={credentials.applicant.email}>{credentials.applicant.email}</option>}
                  {this.state.value === 0 && <option value={credentials.beneficiary.email}>{credentials.beneficiary.email}</option>}
                  {this.state.value === 1 && <option value={credentials.issuingBank.email}>{credentials.issuingBank.email}</option>}
                  {this.state.value === 1 && <option value={credentials.advisingBank.email}>{credentials.advisingBank.email}</option>}
                </NativeSelect>
              </FormControl>
              <InputLabel htmlFor="password">Password</InputLabel>
              <FormControl margin="normal" required fullWidth>

                <Input
                  name="password"
                  type="password"
                  id="password"
                  value="User@123"
                  onChange={this.handleChange}
                  autoComplete="current-password"
                />
              </FormControl>
              <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Remember me"
              />
              <Button
                fullWidth
                type="submit"
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={(event) => { this.state.value === 0 ? this.handleSignin(event) : this.handleBankSignin(event) }}
              >

                Sign in
                <div className='sweet-loading'>
                  <ClipLoader
                    className={override}
                    sizeUnit={"px"}
                    size={30}
                    color={'white'}
                    loading={this.state.loading}
                  />
                </div>
              </Button>

            </form>
            <Grid container style={{ marginTop: 20 }}>
              <Grid elememt>
                <CardActions>
                  <NavLink to="/forgotpassword" className={classes.textStyle}>
                    <Button size="small" color="primary">
                      Forgot Password
              </Button>
                  </NavLink>
                </CardActions>
              </Grid>
              <Grid elememt>
                <CardActions>
                  <NavLink to="/signup" className={classes.textStyle}>
                    <Button size="small" color="primary">
                      Register
              </Button>
                  </NavLink>
                </CardActions>
              </Grid>
            </Grid>
          </Paper>
        </main>
      </React.Fragment>
    );
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(LoginStyles)(Login));