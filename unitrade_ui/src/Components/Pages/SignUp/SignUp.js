import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import { Typography, Grid } from '@material-ui/core';
import BasicInfoForm from './BasicInfoForm';
import Review from './Review';
import { NavLink } from 'react-router-dom';
import BackendServices from "../../../Services/BackendServices";
import Simplert from 'react-simplert';
import { withRouter } from 'react-router';
import { SignupStyles } from '../../styles';


const steps = ['Basic Info.', 'Verify Details'];

function getStepContent(step, values, handleChange) {
  switch (step) {
    case 0:
      return <BasicInfoForm values={values} handleChange={handleChange}/>;
    case 1:
      return <Review values={values} handleChange={handleChange}/>;
    default:
      throw new Error('Unknown step');
  }
}

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeStep: 0,
      showAlert: false,
      alertType: '',
      alertTitle:'',
      firstName: '',
      lastName: '',
      email: '',
      contact: '',
      address: '',
      city: '',
      zip: '',
      province:'',
      country: '',
      organisation: '',
      password: '',
      confirmpassword: '',
      apikey: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.closeSimplert = this.closeSimplert.bind(this);
  }

  handleChange(event) {
    var obj = {};
    obj[event.target.id] = event.target.value;
    this.setState(obj);
  }

  creatAlert(Alert, Type, Title) {
    this.setState(state => ({
      showAlert: Alert, 
      alertType: Type, 
      alertTitle: Title 
    }))
  };

  handleSubmit(event) {
    BackendServices.SignUp(this.state.email, this.state.password, this.state.firstName, this.state.lastName, this.state.contact, this.state.address, this.state.city, this.state.province,  this.state.zip, this.state.country, this.state.organisation, this.state.apikey )
    .then(res => {
      this.creatAlert(true, "success", "User successfully Signed Up.");
  }, error => {
      this.props.history.push('/signup');
      console.log(error)
      if (error) 
          this.creatAlert(true, "error", error.responseJSON.error);
      else {
          this.creatAlert(true, "error", "Somethig went wrong.");
      }
  });
    this.handleNext();
    event.preventDefault();
  }


  handleNext = () => {
    this.setState(state => ({
      activeStep: state.activeStep + 1,
    }));
  };

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }));
  };

  handleReset = () => {
    this.setState({
      activeStep: 0,
    });
  };
  closeSimplert() {
    this.setState({showAlert: false});
}

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <CssBaseline />
        <Simplert
          showSimplert={this.state.showAlert}
          type={this.state.alertType}
          title={this.state.alertTitle}
          disableOverlayClick={true}
          onClose={this.closeSimplert}/>
        <main className={classes.layout}>
          <Paper className={classes.paper}>
            <Typography component="h1" variant="h4" align="center">
              Sign Up
            </Typography>
            <Stepper activeStep={this.state.activeStep} className={classes.stepper}>
              {steps.map(label => (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              ))}
            </Stepper>
            <React.Fragment>
              {this.state.activeStep === steps.length ? (
                <React.Fragment>
                  {this.state.alertType === "success" ? (
                    <React.Fragment>
                  <Typography variant="h5" gutstateterBottom>
                    Thank you for registering with us.
                  </Typography>
                  <Typography variant="subtitle1">
                    You are now a certified user for UniTrade Blockchain Trade-Finance platform powered by Newgen Software Technologies Pvt.Ltd..
                  </Typography>
                  </React.Fragment>
                  ) : (
                    <React.Fragment>
                  <Typography variant="h5" gutstateterBottom>
                    Oops ! Registration Request declined.
                  </Typography>
                  <Typography variant="subtitle1">
                    Server is unable to register you due to some discrepancy in the entered values. Please try registering again..
                  </Typography>
                  </React.Fragment>
                  )}
                  <Grid container spacing={24}><Grid item xs={4}></Grid>
                  <Grid item xs={4}>
                  <NavLink to="/">
                  <Button size="contained" color="primary" style = {{align: 'center', marginTop :10, marginBottom :10}}>
                    Back to Login 
                  </Button>
                  </NavLink>
                  </Grid><Grid item xs={4}></Grid>
                  </Grid>  
                </React.Fragment>
              ) : (
                <React.Fragment>
                  {getStepContent(this.state.activeStep, this.state, this.handleChange)}
                  <div className={classes.buttons}>
                    {this.state.activeStep !== 0 && (
                      <Button onClick={this.handleBack} className={classes.button}>
                        Back
                      </Button>
                    )}
                    <Button
                      variant="contained"
                      color="secondary"
                      onClick={this.state.activeStep === steps.length - 1 ? this.handleSubmit : this.handleNext}
                      className={classes.button}
                    >
                      {this.state.activeStep === steps.length - 1 ? 'Submit' : (this.state.activeStep === 0 ? 'Next':'Done')}
                    </Button>
                  </div>
                </React.Fragment>
              )}
            </React.Fragment>
          </Paper>
        </main>
      </React.Fragment>
    );
  }
}

SignUp.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(SignupStyles)(SignUp));