import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Paper, Grid } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import UserDetails from './UserDetails';
import BackendServices from "../../../Services/BackendServices";
import { ProfileStyles } from '../../styles';
import Dialog from '@material-ui/core/Dialog';
import blue from '@material-ui/core/colors/blue';
import ChangePassword from './ChangePassword';
import AddBeneficiary from './AddBeneficiary';

const styles = {
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  position: {
    float: 'right',
  }
};

class SimpleDialog extends React.Component {
  handleClose = () => {
    this.props.onClose(this.props.selectedValue);
  };

  handleListItemClick = value => {
    this.props.onClose(value);
  };

  render() {
    const { classes, onClose, selectedValue, ...other } = this.props;

    return (
      <Dialog onClose={this.handleClose} aria-labelledby="simple-dialog-title" {...other}>
        <div>
          <ChangePassword />
        </div>
      </Dialog>
    );
  }
}

class AddModal extends React.Component {
  handleClose = () => {
    this.props.onClose(this.props.selectedValue);
  };

  handleListItemClick = value => {
    this.props.onClose(value);
  };

  render() {
    const { classes, onClose, selectedValue, ...other } = this.props;

    return (
      <Dialog onClose={this.handleClose} aria-labelledby="simple-dialog-title" {...other}>
        <div>
          <AddBeneficiary />
        </div>
      </Dialog>
    );
  }
}

SimpleDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  selectedValue: PropTypes.string,
};

AddModal.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  selectedValue: PropTypes.string,
};


const SimpleDialogWrapped = withStyles(styles)(SimpleDialog);
const AddModalWrapped = withStyles(styles)(AddModal);

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      addModal: false,
      open: false,
      firstName: 'a',
      lastName: 'b',
      email: 'c',
      contact: 'd',
      address: 'e',
      city: 'f',
      zip: 'g',
      province: 'h',
      country: 'i',
      organisation: 'j',
    };
    this.getToken = this.getToken.bind(this);
    this.componentWillMount = this.componentWillMount.bind(this);
  }

  getToken() {
    return JSON.parse(sessionStorage.getItem('user'));
  }

  componentWillMount() {
    BackendServices.getUserDetails(this.getToken().token)
      .then(res => {
        console.log(res.data.user)
        this.setState({
          firstName: res.data.user.first_name,
          lastName: res.data.user.last_name,
          email: res.data.user.email,
          contact: res.data.user.contact,
          address: res.data.user.address,
          city: res.data.user.city,
          zip: res.data.user.zip_code,
          province: res.data.user.state,
          country: res.data.user.country,
          organisation: res.data.user.organisation,
        })
      }, error => {
        console.log(error)
        this.props.history.push('/login');
        if (error.responseJSON.error.error)
          this.creatAlert(true, "error", JSON.stringify(error.responseJSON.error.error));
        else {
          this.creatAlert(true, "error", "Incorrect Email/Password.");
        }
      });
  }

  handleClickOpen = () => {
    this.setState({
      open: true,
    });
  };

  handleAddBeneficiary = () => {
    this.setState({
      addModal: true,
    });
  };

  handleClose = value => {
    this.setState({ selectedValue: value, open: false, addModal: false });
  };

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <CssBaseline />
        <main className={classes.layout}>
          <Paper className={classes.paper}  style={{ backgroundImage: 'linear-gradient(to right, #cccccc , #ffffff)', }}>
            <Typography component="h1" variant="h4" align="center">
              User Details
            </Typography>
            <UserDetails values={this.state} />
            <SimpleDialogWrapped
              open={this.state.open}
              onClose={this.handleClose}
            />
            <AddModalWrapped
              open={this.state.addModal}
              onClose={this.handleClose}
            />
            <Grid container style={{ marginTop: 20}}>
              <Button
                variant="contained"
                color="primary"
                className={classes.button}
                onClick={this.handleClickOpen}
                style={{ marginLeft: 350 }}
              >
                Change Password
              </Button>
            </Grid>
          </Paper>
        </main>
      </React.Fragment>
    );
  }
}

Profile.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(ProfileStyles)(Profile);