import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import AddRounded from '@material-ui/icons/AddRounded';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import BackendServices from "../../../Services/BackendServices";
import Simplert from 'react-simplert';
import { withRouter } from 'react-router'
import { ChangePasswordStyles } from '../../styles'


class AddBeneficiary extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alertType: '',
            alertTitle: '',
            showAlert: false,
            newBeneficiary: '',
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.creatAlert = this.creatAlert.bind(this);
        this.getToken = this.getToken.bind(this);
        this.closeSimplert = this.closeSimplert.bind(this);
    }
    creatAlert(Alert, Type, Title) {
        this.setState(state => ({
            showAlert: Alert,
            alertType: Type,
            alertTitle: Title
        }))
    };

    handleChange(event) {
        var obj = {};
        obj[event.target.id] = event.target.value;
        this.setState(obj);
    };

    getToken() {
        return JSON.parse(sessionStorage.getItem('user'));
    }

    handleSubmit(event) {
        BackendServices.AddBeneficiary(this.getToken().token, this.state.newBeneficiary)
            .then(res => {
                this.creatAlert(true, "success", "You have successfully added the beneficiary.");
            }, error => {

                console.log(error)
                if (error.responseJSON.error.error)
                    this.creatAlert(true, "error", JSON.stringify(error.responseJSON.error.error));
                else {
                    this.creatAlert(true, "error", "Somethig went wrong.");
                }
            })

        event.preventDefault();
    };

    closeSimplert() {
        this.setState({ showAlert: false });
    };

    render() {
        const { classes } = this.props;

        return (
            <React.Fragment>
                <CssBaseline />
                <Simplert
                    showSimplert={this.state.showAlert}
                    type={this.state.alertType}
                    title={this.state.alertTitle}
                    disableOverlayClick={true}
                    onClose={this.closeSimplert} />
                <main className={classes.layout}>
                    <Paper className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <AddRounded />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Add Beneficiary
          </Typography>
                        <form className={classes.form}>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="password">Beneficiary e-mail</InputLabel>
                                <Input
                                    name="newBeneficiary"
                                    type="test"
                                    value={this.state.newBeneficiary}
                                    onChange={this.handleChange}
                                    id="newBeneficiary"
                                    autoComplete="new-beneficiary"
                                />
                            </FormControl>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                onClick={this.handleSubmit}
                                className={classes.submit}
                            >
                                Add
              </Button>
                        </form>
                    </Paper>
                </main>
            </React.Fragment>
        );
    }
}

AddBeneficiary.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(ChangePasswordStyles)(AddBeneficiary));