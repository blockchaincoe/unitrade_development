import React from 'react';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { UserDetailsStyles } from '../../styles'


class UserDetails extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <React.Fragment>
        <List disablePadding>
          <ListItem className={UserDetailsStyles.listItem}>
            <ListItemText primary="First Name" />
            <Typography variant="body2">{this.props.values.firstName}</Typography>
          </ListItem>
          <ListItem style={UserDetailsStyles.listItem}>
            <ListItemText primary="Last Name" />
            <Typography variant="body2">{this.props.values.lastName}</Typography>
          </ListItem>
          <ListItem style={UserDetailsStyles.listItem}>
            <ListItemText primary="Email Address" />
            <Typography variant="body2">{this.props.values.email}</Typography>
          </ListItem>
          <ListItem style={UserDetailsStyles.listItem}>
            <ListItemText primary="Contact No." />
            <Typography variant="body2">{this.props.values.contact}</Typography>
          </ListItem>
          <ListItem style={UserDetailsStyles.listItem}>
            <ListItemText primary="Residential Address" />
            <Typography variant="body2">{this.props.values.address}</Typography>
          </ListItem>
          <ListItem style={UserDetailsStyles.listItem}>
            <ListItemText primary="City"/>
            <Typography variant="body2">{this.props.values.city}</Typography>
          </ListItem>
          <ListItem style={UserDetailsStyles.listItem}>
            <ListItemText primary="State/Province" />
            <Typography variant="body2">{this.props.values.province}</Typography>
          </ListItem>
          <ListItem style={UserDetailsStyles.listItem}>
            <ListItemText primary="Country" />
            <Typography variant="body2">{this.props.values.country}</Typography>
          </ListItem>
        </List>
      </React.Fragment>
    );
  }
}


export default UserDetails;