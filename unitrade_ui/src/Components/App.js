import React from 'react';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom';
import { Header, Footer } from './Layouts';
import { Login, SignUp, ForgotPassword, UserDashboard, BankDashboard } from './Pages';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import history from '../history';
import { Paper } from '@material-ui/core';
import styles from './styles';

 const theme = createMuiTheme({
    palette: {
      secondary: {  
        main: '#ffffff',
        dark: '#212121',
        light: '#BDBDBD',
        contrastText: '#000000'
      },
      primary: {
        main: '#000000',
        dark: '#33691E',
        light: '#C5E1A5',
        contrastText: '#ffffff'
        },
      default: {
        main: '#dddddd',
        contrastText: '#000000'
      }
    },
  });

class App extends React.Component{
  constructor(props){
    super(props);
    this.state={
     auth: '',
    }
  }
  handleLogout = () => {
    sessionStorage.clear('isLoggedIn');
    sessionStorage.clear('userRole');
    this.setState({auth: false});
  };

  handleLogin = () => {
    sessionStorage.setItem('isLoggedIn' , true);
    this.setState({auth: true});
  };
    render(){
        return(     
          <MuiThemeProvider theme={theme}>
            <Router history={history}>
              <Paper style={styles.appContainer}>
              {sessionStorage.getItem('isLoggedIn') ? null : <Header auth ={this.state.auth} handleLogout={this.handleLogout}/>}
                    <Switch>             
                      <Route exact path="/" component={()=><Login auth ={this.state.auth} handleLogin={this.handleLogin}/>}/>
                      <Route path="/forgotpassword" component={ForgotPassword}/> 
                      <Route path="/signup" component={SignUp}/>
                      {sessionStorage.getItem('isLoggedIn') && <Route path="/userdashboard" component= {() => <UserDashboard handleLogout={this.handleLogout}/> } />}
                      {sessionStorage.getItem('isLoggedIn') && <Route path="/bankdashboard" component= { () => <BankDashboard handleLogout={this.handleLogout}/>  } />}
                    </Switch>   
                  <Footer/>  
                </Paper>      
            </Router>
            </MuiThemeProvider>             
        );
    }
}

export default App;
