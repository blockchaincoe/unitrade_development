/** ********************** Import node modules ************************ */
import mongoose from "mongoose";

/** ********************** Import local modules ************************ */
import db from "../dbConnection";

// This defines user schema which will store user information
const UserSchema = new mongoose.Schema({
  first_name: { type: String, required: true },
  last_name: { type: String, required: true },
  contact: { type: Number, required: true },
  address: { type: String, required: true },
  city: { type: String, required: true },
  state: { type: String, required: true },
  country: { type: String, required: true },
  zip_code: { type: Number, required: true },
  organisation: { type: String },
  email: {
    type: String,
    required: true,
    index: { unique: true },
    match: /.+@.+\..+/,
    lowercase: true
  },
  password: { type: String, required: true, select: false },
  role: {
    type: String,
    required: true,
    enum: ["individual", "organisation"],
    default: "individual"
  },
  email_verification_code: { type: String },
  email_verification_expiry: { type: Date },
  client_api_key: { type: String },
  created_at: { type: Date },
  updated_at: { type: Date },
  status: {
    type: Boolean,
    default: true
  },
  package: { type: String },
  package_expiry: { type: Date },
  api_hits: { type: String },
  api_hits_history: [{ type: Number }]
});

export default db.model("User", UserSchema);
