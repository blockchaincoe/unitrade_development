/** ********************** Import node modules ************************ */
import mongoose from "mongoose";

/** ********************** Import local modules ************************ */
import db from "../dbConnection";

// This defines customer schema which will store customer information
const BankSchema = new mongoose.Schema({
  bankId: { type: String },
  bankName: { type: String },
  IFSCcode: { type: String },
  branch: { type: String },
  city: { type: String },
  state: { type: String },
  country: { type: String }
});

export default db.model("Bank", BankSchema);
