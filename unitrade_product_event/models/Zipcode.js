/** ********************** Import node modules ************************ */
import indianPincodeDatabase from "indian-pincode-database";
import mongoose from "mongoose";

/** ********************** Import local modules ************************ */
import db from "../dbConnection";

// populate database
indianPincodeDatabase.pushToDatabase("uniTrade-development", "zipcodes");

// This defines zipcode schema according to the module
const ZipcodeSchema = new mongoose.Schema({
  updatedAt: { type: Date },
  createdAt: { type: Date },
  officeName: { type: String },
  pincode: { type: Number },
  officeType: { type: String },
  deliveryStatus: { type: String },
  divisionName: { type: String },
  regionName: { type: String },
  circleName: { type: String },
  taluk: { type: String },
  districtName: { type: String },
  stateName: { type: String }
});

export default db.model("zipcode", ZipcodeSchema);
