/** ********************** Import node modules ************************ */
import mongoose from "mongoose";

/** ********************** Import local modules ************************ */
import db from "../dbConnection";

// This defines customer schema which will store customer information
const PackageSchema = new mongoose.Schema({
  name: { type: String, required: true },
  apiCount: { type: Number, required: true },
  price: { type: Number, required: true },
  duration: { type: Number, required: true },
  package_id: {
    type: String,
    required: true,
    index: { unique: true }
  }
});

export default db.model("Package", PackageSchema);
