/** ********************** Import node modules ************************ */
import Promise from "bluebird";

/** ********************** Import local modules ************************ */
import logger from "../utils/logger";
import Package from "../models/Package";

/** @function getAllPackages
 * @desc This is a service used for fetching packages
 */
const getAllPackages = () =>
  new Promise((resolve, reject) => {
    Package.find({})
      .then(result => {
        logger.info("DB packages response: ", result);
        resolve(result);
      })
      .catch(error => {
        logger.error("DB packages error: ", error);
        reject(Error(error));
      });
  });

/** @function checkPackage
 * @desc This is a service used for checking a package using it's Id
 * @param packageId include package Id
 */
const checkPackage = packageId =>
  new Promise((resolve, reject) => {
    Package.findById({ _id: packageId })
      .then(result => {
        logger.info("DB packages response: ", result);
        resolve(result);
      })
      .catch(error => {
        logger.error("DB packages error: ", error);
        reject(Error(error));
      });
  });

export default {
  getAllPackages,
  checkPackage
};
