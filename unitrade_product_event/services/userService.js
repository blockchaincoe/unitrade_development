/** ********************** Import node modules ************************ */
import crypto from "crypto";
import _ from "lodash";
import bcrypt from "bcrypt";
import config from "config";
import Promise from "bluebird";

/** ********************** Import local modules ************************ */
import appConst from "../utils/appConst";
import authentication from "../helpers/authentication";
import logger from "../utils/logger";
import timer from "../utils/timer";
import User from "../models/User";
import Zipcode from "../models/Zipcode";

/** ********************** Variable Listing ************************ */
const appConfig = config.get("TradeFinance");
const secret = "trade_finance_key_generator";
const algorithm = "sha1";

/** @function getZipcodeData
 * @desc This is a service used for fetching Zipcode data
 */
const getZipcodeData = pin =>
  new Promise((resolve, reject) => {
    Zipcode.find({ pincode: pin })
      .then(result => {
        // logger.info("DB packages response: ", result);
        const res = {
          city: result[0].districtName,
          state: result[0].stateName
        };
        resolve(res);
      })
      .catch(error => {
        logger.error("DB zipcode error: ", error);
        reject(Error(error));
      });
  });

/** @function signUp
 * @desc This is a service used for sign up by providing user details
 * @param {JSON Object} userDetails includes the user credentials and details (email, password, name, contact, organisation)
 */
const signUp = userDetails =>
  new Promise((resolve, reject) => {
    bcrypt
      .hash(userDetails.password, appConfig.saltRounds)
      .then(async hash => {
        const emailHash = Math.floor(Math.random() * 1000000000);
        logger.info(`password hash ${hash}`);
        _.set(userDetails, "password", hash);
        _.set(userDetails, "email_verification_code", emailHash);
        _.set(
          userDetails,
          "email_verification_expiry",
          timer.getTimeAfterAnHour()
        );
        const user = await new User(userDetails).save();
        resolve(user.email_verification_code);
      })
      .catch(error => {
        logger.error("DB Signup error: ", error);
        reject(Error(error));
      });
  });

/** @function verifyEmail
 * @desc This is a service used for verification of the user
 * @param {JSON Object} userHash includes the unique hash of the user sent via email
 */
const verifyEmail = (userHash, email) =>
  new Promise((resolve, reject) => {
    User.find({ email })
      .then(async userResult => {
        const expiryTime = Math.abs(
          userResult[0].email_verification_expiry - timer.getNow()
        );
        if (
          userResult[0].email_verification_code === userHash &&
          appConst.expiry_time >= expiryTime
        ) {
          await User.findOneAndUpdate({ email }, { $set: { status: true } });
          logger.info("DB verify response: Verified");
          resolve("User verified");
        } else {
          reject(new Error("verification link expired"));
        }
      })
      .catch(error => {
        logger.error("DB Verify Email error: ", error);
        reject(Error(error));
      });
  });

/* eslint-disable max-statements */
/** @function login
 * @desc This is a service used for login by providing user email and password
 * @param email includes user's email
 * @param password include user's password
 */
const login = (email, password) =>
  new Promise((resolve, reject) => {
    const projections = {
      email: true,
      last_name: true,
      first_name: true,
      password: true,
      status: true,
      id: true
    };
    User.find({ email }, projections)
      .then(async userResult => {
        if (userResult[0]) {
          const isCorrectPassword = await bcrypt.compare(
            password,
            userResult[0].password
          );
          if (userResult[0].status) {
            if (isCorrectPassword) {
              const token = authentication.createToken(userResult[0]);
              const loginResult = {
                first_name: userResult[0].first_name,
                last_name: userResult[0].last_name,
                user_id: userResult[0].id,
                email: userResult[0].email,
                token
              };
              logger.info("DB login response: ", loginResult);
              resolve(loginResult);
            } else {
              reject(new Error("Invalid password"));
            }
          } else {
            reject(new Error("Email not verified!"));
          }
        } else {
          reject(new Error("Email does not exist!"));
        }
      })
      .catch(error => {
        logger.error("DB login error: ", error);
        reject(new Error(error));
      });
  });

/** @function generateAPIKey
 * @desc This is a service used for generating client's API key
 */
const generateAPIKey = APIKeyEmail =>
  new Promise((resolve, reject) => {
    User.find({ email: APIKeyEmail })
      .then(result => {
        const hmac = crypto.createHmac(algorithm, secret);
        hmac.update(APIKeyEmail);
        const APIKey = hmac.digest("hex");
        result.client_api_key = APIKey;
        resolve(APIKey);
      })
      .catch(error => {
        logger.error("Cant generate API Key: ", error);
        reject(Error(error));
      });
  });

/** @function getUserDetails
 * @desc This is a service used for fetching user details by email
 * @param email includes user's email
 */
const getUserDetails = email =>
  new Promise((resolve, reject) => {
    const projections = {
      email: true,
      last_name: true,
      first_name: true,
      id: true,
      contact: true,
      address: true,
      city: true,
      state: true,
      country: true,
      zip_code: true,
      organisation: true,
      role: true,
      status: true
    };
    User.find({ email }, projections)
      .then(userResult => {
        logger.info("DB get user details response: ", userResult);
        resolve(userResult);
      })
      .catch(error => {
        logger.error("DB get user details error: ", error);
        reject(new Error(error));
      });
  });

/** @function getUserPackage
 * @desc This is a service used for fetching user package details by email
 * @param email includes user's email
 */
const getUserPackage = email =>
  new Promise((resolve, reject) => {
    const projections = {
      package: true,
      package_expiry: true,
      api_hits: true,
      api_hits_history: true
    };
    User.find({ email }, projections)
      .then(userResult => {
        logger.info("DB get user package details response: ", userResult[0]);
        resolve(userResult[0]);
      })
      .catch(error => {
        logger.error("DB get user package details error: ", error);
        reject(new Error(error));
      });
  });

/** @function updatePassword
 * @desc This is a service used for updating user password
 * @param email include user's email
 * @param oldPassword include user's oldPassword
 * @param newPassword include user's newPassword
 */
const updatePassword = (email, oldPassword, newPassword) =>
  new Promise((resolve, reject) => {
    const projections = {
      password: true,
      id: true
    };
    User.find({ email }, projections)
      .then(async result => {
        if (await bcrypt.compare(oldPassword, result[0].password)) {
          const passwordHash = await bcrypt.hash(
            newPassword,
            appConfig.saltRounds
          );
          const updateResult = User.findOneAndUpdate(
            { email },
            { $set: { password: passwordHash } }
          );
          logger.info("DB update password response: ", updateResult);
          resolve(updateResult);
        } else {
          reject(new Error("invalid old password"));
        }
      })
      .catch(error => {
        logger.error("DB Update password error: ", error);
        reject(new Error(error));
      });
  });

/** @function updatePackage
 * @desc This is a service used for updating user package
 * @param email include user's email
 * @param packageId include user's oldPassword
 * @param packageExpiry include user's packageExpiry
 */
const updatePackage = (email, packageId, packageExpiry) =>
  new Promise((resolve, reject) => {
    User.findOneAndUpdate(
      { email },
      { $set: { package: packageId, package_expiry: packageExpiry } }
    )
      .then(updateResult => {
        if (updateResult) {
          logger.info("DB update package response: ", updateResult);
          resolve(updateResult);
        } else {
          reject(new Error("invalid user"));
        }
      })
      .catch(error => {
        logger.error("DB Update package error: ", error);
        reject(new Error(error));
      });
  });

/** @function resetPassword
 * @desc This is a service used for user password reset
 * @param email include user's email
 * @param password include user's password
 */
const resetPassword = (email, password) =>
  new Promise((resolve, reject) => {
    bcrypt
      .hash(password, appConfig.saltRounds)
      .then(passwordHash =>
        User.findOneAndUpdate({ email }, { $set: { password: passwordHash } })
      )
      .then(resetResult => {
        logger.info("DB reset password response: ", resetResult);
        resolve(resetResult);
      })
      .catch(error => {
        logger.error("DB reset password error: ", error);
        reject(new Error(error));
      });
  });

export default {
  getZipcodeData,
  signUp,
  verifyEmail,
  login,
  generateAPIKey,
  getUserDetails,
  updatePassword,
  updatePackage,
  resetPassword,
  getUserPackage
};
