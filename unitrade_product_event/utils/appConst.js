module.exports = {
  verification_subject: "Please confirm your Email account",
  verification_body:
    "Please confirm your account by clicking the following link: ",
  verification_link: "http://192.168.60.140:3000/api/verifyEmail?link=",
  expiry_time: 30000000,
  reset_password_link: "http://192.168.60.140:3000/resetPassword/email=",
  reset_password_subject: "Reset Password request",
  reset_password_body:
    "We got a request to reset your password. Please click on the link: "
};
