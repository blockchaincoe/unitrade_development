exports.getNow = () => new Date();

exports.getTimeAfterAnHour = () => {
  const now = new Date();
  now.setHours(now.getHours() + 1);
  return now;
};
