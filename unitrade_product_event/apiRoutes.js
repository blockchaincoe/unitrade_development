/** ********************** Import node modules ************************ */
import { Router } from "express";

/** ********************** Import local modules ************************ */
import authentication from "./helpers/authentication";
import logger from "./utils/logger";
import packageController from "./controllers/packageController";
import userController from "./controllers/userController";

const apiRoutes = Router();

/** **************************** User APIs ********************************* */
/** Get Zipcode data API */
apiRoutes.get("/getZipcodeData", userController.getZipcodeData);

/** SignUp API */
apiRoutes.post("/signUp", userController.signUp);

/** Verify API */
apiRoutes.get("/verifyEmail", userController.verifyEmail);

/** Login API */
apiRoutes.post("/login", userController.login);

/** Generate API key */
apiRoutes.post("/generateAPIKey", userController.generateAPIKey);

/** Forgot Password API */
apiRoutes.post("/forgotPassword", userController.forgotPassword);

/** Reset Password API */
apiRoutes.post("/resetPassword", userController.resetPassword);

/** Packages API */
apiRoutes.get("/packages", packageController.getAllPackages);

/** ******************* Authentication Middleware ********************* */
apiRoutes.use((req, res, next) => {
  const token =
    req.body.token || req.params.token || req.headers["x-access-token"];
  if (token) {
    authentication
      .authenticateUser(token)
      .then(decode => {
        req.user = decode;
        next();
      })
      .catch(error => {
        logger.error("Error while checking token: Invalid token", error);
        res.status(403).send({ status: false, message: "Invalid token" });
      });
  } else {
    logger.error("Error while checking token: No Token Provided");
    res.status(403).send({ status: false, message: "No Token Provided" });
  }
});

/** Get User Details API */
apiRoutes.get("/getUserDetails", userController.getUserDetails);

/** Update Password API */
apiRoutes.post("/updatePassword", userController.updatePassword);

/** Update Packages API */
apiRoutes.put("/updatePackage", userController.updatePackage);

/** Get User Package API */
apiRoutes.get("/getUserPackage", userController.getUserPackage);

export default apiRoutes;
