/** ********************** Import local modules ************************ */
import logger from "../utils/logger";
import packageService from "../services/packageService";

/** @function getAllPackages
 * @desc This function is for getting all getAllPackages
 * @param {JSON Object} res includes success and data
 */
const getAllPackages = (req, res) => {
  logger.info("get all packages");

  packageService
    .getAllPackages()
    .then(packageResponse => {
      logger.info(`Package fetched: ${packageResponse}`);
      res.status(200).json({
        success: true,
        data: packageResponse
      });
    })
    .catch(error => {
      logger.error(`package fetching failed: ${error}`);
      res.status(500).json({
        success: false,
        error: error.message
      });
    });
};

export default {
  getAllPackages
};
