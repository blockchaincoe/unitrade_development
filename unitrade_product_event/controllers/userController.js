/** ********************** Import local modules ************************ */
import appConst from "../utils/appConst";
import logger from "../utils/logger";
import nodemailer from "../helpers/nodemailer";
import packageService from "../services/packageService";
import userService from "../services/userService";

/** ********************** Variable listing ************************ */
/* This regular expression checks the string contains atleast one lowercase letter,
  one uppercase letter, one special character and one digit */
const validPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@#$!%*?&])[A-Za-z\d$@#$!%*?&]{8,}/;

/* eslint-disable max-statements */
/* eslint max-lines: "off" */
/** @function getZipcodeData
 * @desc This function is for retrieveing city and state from pincode
 * @param {JSON Object} req includes the user's pincode
 * @param {JSON Object} res includes success and message
 */
const getZipcodeData = (req, res) => {
  const pin = Number(req.query.pin);

  logger.info("get Zip data of pincode : ", pin);
  userService
    .getZipcodeData(pin)
    .then(zipcodeResponse => {
      logger.info(`Zipcode fetched: ${zipcodeResponse}`);
      res.status(200).json({
        success: true,
        data: zipcodeResponse
      });
    })
    .catch(error => {
      logger.error(`zipcode fetching failed: ${error}`);
      res.status(500).json({
        success: false,
        error: error.message
      });
    });
};

/* eslint-disable max-statements */
/* eslint max-lines: "off" */
/** @function signUp
* @desc This function is for user registration 
* @param {JSON Object} req includes the user details (email, password, firstname, zipcode
    lastname, contact, address, organisation, city, country, state)
* @param {JSON Object} res includes success and message
*/
const signUp = (req, res) => {
  const userDetails = req.body;
  logger.info("signUp request: ", userDetails);
  if (userDetails.organisation !== undefined) {
    userDetails.role = "organisation";
  }
  if (
    userDetails !== undefined &&
    userDetails.email !== undefined &&
    userDetails.contact !== undefined &&
    userDetails.zip_code !== undefined &&
    userDetails.first_name !== undefined &&
    userDetails.last_name !== undefined &&
    userDetails.address !== undefined &&
    userDetails.city !== undefined &&
    userDetails.state !== undefined &&
    userDetails.country !== undefined &&
    userDetails.password !== undefined
  ) {
    if (userDetails.password.match(validPassword)) {
      userService
        .signUp(userDetails)
        .then(async signUpResponse => {
          logger.info(`Email verification code: ${signUpResponse}`);
          const URL = `${appConst.verification_link + signUpResponse}&email=${
            userDetails.email
          }`;
          const details = {
            to_email: userDetails.email,
            subject: appConst.verification_subject,
            body: `${appConst.verification_body}<a href=${URL}> Click here </a>`
          };
          nodemailer.sendMail(details);
          res.status(200).json({
            success: true,
            message: "User registered successfully. Please verify your email!"
          });
        })
        .catch(error => {
          logger.error("SignUp failed: ", error);
          res.status(500).json({
            success: false,
            error: error.message
          });
        });
    } else {
      logger.error("SignUp failed: Invalid Password!");
      res.status(400).json({
        success: false,
        error: "Invalid Password!"
      });
    }
  } else {
    logger.error("SignUp failed: Invalid or Empty fields!");
    res.status(400).json({
      success: false,
      error: "Invalid or Empty fields!"
    });
  }
};

/** @function verifyEmail
 * @desc This function is for user verification
 * @param {JSON Object} req includes the user verification hash
 * @param {JSON Object} res includes success and message
 */
const verifyEmail = (req, res) => {
  const { email, link } = req.query;
  logger.info("verification request: ", req.query);

  if (link !== undefined || email !== undefined) {
    userService
      .verifyEmail(link, email)
      .then(verificationResponse => {
        logger.info(`Verification successful: ${verificationResponse}`);
        res.status(200).json({
          success: true,
          message: "User verification successful!"
        });
      })
      .catch(error => {
        logger.error(`Verification failed: ${error}`);
        res.status(500).json({
          success: false,
          error: error.message
        });
      });
  } else {
    logger.error("Verification failed!");
    res.status(400).json({
      success: false,
      error: "Invalid link!"
    });
  }
};

/** @function login
 * @desc This function is for user login
 * @param {JSON Object} req includes the user email and password
 * @param {JSON Object} res includes success and data
 */
const login = (req, res) => {
  const { email, password } = req.body;
  logger.info(`login request: ${req.body}`);

  if (email !== undefined || password !== undefined) {
    userService
      .login(email, password)
      .then(loginResponse => {
        logger.info(`Login successful: ${loginResponse}`);
        res.status(200).json({
          success: true,
          data: loginResponse
        });
      })
      .catch(error => {
        logger.error(`Login failed: ${error}`);
        res.status(500).json({
          success: false,
          error: error.message
        });
      });
  } else {
    logger.error("Login failed!");
    res.status(400).json({
      success: false,
      error: "Invalid credentials!"
    });
  }
};

/* eslint-disable max-statements */
/* eslint max-lines: "off" */
/** @function generateAPIKey
 * @desc This function is for generating api key for client using his email
 * @param {JSON Object} req includes the user's email id
 * @param {JSON Object} res includes success and message
 */
const generateAPIKey = (req, res) => {
  const email = req.body.email;
  userService
    .generateAPIKey(email)
    .then(generateAPIKeyResponse => {
      logger.info(`API Key generated: ${generateAPIKeyResponse}`);
      res.status(200).json({
        success: true,
        data: generateAPIKeyResponse
      });
    })
    .catch(error => {
      logger.error(`API Key could not be generated: ${error}`);
      res.status(500).json({
        success: false,
        error: error.message
      });
    });
};

/** @function getUserDetails
 * @desc This function is for fetching user details
 * @param {JSON Object} req includes the user email in the 

 * @param {JSON Object} res includes success and data
 */
const getUserDetails = (req, res) => {
  const { userEmail } = req.user;
  logger.info(`Get User Details request: ${userEmail}`);

  if (userEmail !== undefined) {
    userService
      .getUserDetails(userEmail)
      .then(userResponse => {
        logger.info(`Get User Details successful: ${userResponse}`);
        res.status(200).json({
          success: true,
          data: userResponse
        });
      })
      .catch(error => {
        logger.error(`Get User Details failed: ${error}`);
        res.status(500).json({
          success: false,
          error: error.message
        });
      });
  } else {
    logger.error("Get User Details failed!");
    res.status(400).json({
      success: false,
      error: "Unable to fetch user's email from token!"
    });
  }
};

/** @function updatePassword
 * @desc This function is for updating user password
 * @param {JSON Object} req includes the user email, old password and new password
 * @param {JSON Object} res includes success and message
 */
const updatePassword = (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const { userEmail } = req.user;
  logger.info(`Update Password request: ${req.body}`);

  if (oldPassword !== undefined || newPassword !== undefined) {
    userService
      .updatePassword(userEmail, oldPassword, newPassword)
      .then(updatePasswordResponse => {
        logger.info(`Update Password successful: ${updatePasswordResponse}`);
        res.status(200).json({
          success: true,
          message: "Password changed successfully!"
        });
      })
      .catch(error => {
        logger.error(`Update Password failed: ${error}`);
        res.status(500).json({
          success: false,
          error: error.message
        });
      });
  } else {
    logger.error("Update Password failed!");
    res.status(400).json({
      success: false,
      error: "Invalid credentials!"
    });
  }
};

/** @function updatePackage
 * @desc This function is for updating user package
 * @param {JSON Object} req includes the user email and package id
 * @param {JSON Object} res includes success and message
 */
const updatePackage = (req, res) => {
  const { packageId } = req.body;
  const { userEmail } = req.user;
  logger.info(`Update Package request: ${req.body}`);

  if (packageId !== undefined) {
    packageService
      .checkPackage(packageId)
      .then(result => {
        const presentTime = new Date();
        const expiry = new Date(
          presentTime.setMonth(presentTime.getMonth() + result.duration)
        );
        userService
          .updatePackage(userEmail, packageId, expiry)
          .then(updatePackageResponse => {
            logger.info(`Update Password successful: ${updatePackageResponse}`);
            res.status(200).json({
              success: true,
              message: "Package updated"
            });
          });
      })
      .catch(error => {
        logger.error(`Update Package failed: ${error}`);
        res.status(500).json({
          success: false,
          error: error.message
        });
      });
  } else {
    logger.error("Update Package failed!");
    res.status(400).json({
      success: false,
      error: "Invalid package!"
    });
  }
};

/** @function forgotPassword
 * @desc This function is for submitting forgot password request
 * @param {JSON Object} req includes the user email
 * @param {JSON Object} res includes success and message
 */
const forgotPassword = (req, res) => {
  const { email } = req.body;
  logger.info("Forgot password request: ", email);

  if (email !== undefined) {
    userService
      .getUserDetails(email)
      .then(userResponse => {
        logger.info(`Forgot password response: ${userResponse}`);
        const URL = `${appConst.reset_password_link + email}`;
        const details = {
          to_email: email,
          subject: appConst.reset_password_subject,
          body: `${
            appConst.reset_password_body
          }<a href=${URL}> Reset Password </a>`
        };
        nodemailer.sendMail(details);
        res.status(200).json({
          success: true,
          message: "Reset Password link has been sent to your email!"
        });
      })
      .catch(error => {
        logger.error(`Verification failed: ${error}`);
        res.status(500).json({
          success: false,
          error: error.message
        });
      });
  } else {
    logger.error("Invalid Email!");
    res.status(400).json({
      success: false,
      error: "Invalid Email!"
    });
  }
};

/** @function resetPassword
 * @desc This function is for user password reset after forgot password
 * @param {JSON Object} req includes the user email and password
 * @param {JSON Object} res includes success and message
 */
const resetPassword = (req, res) => {
  const { email, password } = req.body;
  logger.info(`Reset Password request: ${req.body}`);

  if (email !== undefined || password !== undefined) {
    if (password.match(validPassword)) {
      userService
        .resetPassword(email, password)
        .then(resetPasswordResponse => {
          logger.info(`Reset Password successful: ${resetPasswordResponse}`);
          res.status(200).json({
            success: true,
            message: "Password reset successfully!"
          });
        })
        .catch(error => {
          logger.error(`Reset Password failed: ${error}`);
          res.status(500).json({
            success: false,
            error: error.message
          });
        });
    } else {
      logger.error("Reset password failed: Invalid Password!");
      res.status(400).json({
        success: false,
        error: "Invalid Password!"
      });
    }
  } else {
    logger.error("Reset Password failed!");
    res.status(400).json({
      success: false,
      error: "Invalid credentials!"
    });
  }
};

/** @function getUserPackage
 * @desc This function is for fetching user package and it's details
 * @param {JSON Object} req includes the user email
 * @param {JSON Object} res includes success and data
 */
const getUserPackage = (req, res) => {
  const { userEmail } = req.user;
  logger.info(`Get user Package request: ${userEmail}`);
  const resultDetails = {};
  userService
    .getUserPackage(userEmail)
    .then(result => {
      if (result.package) {
        resultDetails.userDetails = result;
        packageService.checkPackage(result.package).then(packageDetails => {
          resultDetails.packageDetails = packageDetails;
          logger.info(`Package details successful: ${resultDetails}`);
          res.status(200).json({
            success: true,
            data: resultDetails
          });
        });
      } else {
        logger.error("Fetching User package details failed");
        res.status(400).json({
          success: false,
          error: "User has no package"
        });
      }
    })
    .catch(error => {
      logger.error(`Fetching Package details failed: ${error}`);
      res.status(500).json({
        success: false,
        error: error.message
      });
    });
};

export default {
  getZipcodeData,
  signUp,
  verifyEmail,
  login,
  generateAPIKey,
  getUserDetails,
  updatePassword,
  updatePackage,
  forgotPassword,
  resetPassword,
  getUserPackage
};
