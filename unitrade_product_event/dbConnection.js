/** ********************** Import node modules ************************ */
import config from "config";
import mongoose from "mongoose";

/** ********************** Import local modules ************************ */
import logger from "./utils/logger";

/** ********************** Variable listing ************************** */
const dbConfig = config.get("TradeFinance.database");

/** Database Connection */
export default mongoose.createConnection(
  dbConfig,
  { useNewUrlParser: true },
  err => {
    if (err) {
      logger.error(err);
    } else {
      logger.info("Database Connection established");
    }
  }
);
