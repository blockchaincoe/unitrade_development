/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';
/**
 * Write your transction processor functions here
 */


/**
 * Notify all parties about new LC
 * @param {trade.finance.demo.newLC} newLC
 * @transaction
 */
async function newLC(tx) {
    const LCRegistry = await getAssetRegistry('trade.finance.demo.LC');
    const bankRegistry = await getParticipantRegistry('trade.finance.demo.Bank');
    const clientRegistry = await getParticipantRegistry('trade.finance.demo.Client');
    tx.LCnotify.applicant.activeLC.push(tx.LCnotify.LC_id);
    tx.LCnotify.issuingBank.pendingLC.push(tx.LCnotify.LC_id);
    tx.LCnotify.advisingBank.activeLC.push(tx.LCnotify.LC_id);
    tx.LCnotify.beneficiary.activeLC.push(tx.LCnotify.LC_id);
    await LCRegistry.update(tx.LCnotify);
    await bankRegistry.update(tx.LCnotify.issuingBank);
    await bankRegistry.update(tx.LCnotify.advisingBank);
    await clientRegistry.update(tx.LCnotify.applicant);
    await clientRegistry.update(tx.LCnotify.beneficiary);
}


/**
 * LC forwarded to Issuing Bank
 * @param {trade.finance.demo.forwardLC} forwardLC
 * @transaction
 */
async function forwardLC(tx) {
    const LCRegistry = await getAssetRegistry('trade.finance.demo.LC');
    const bankRegistry = await getParticipantRegistry('trade.finance.demo.Bank');
    const clientRegistry = await getParticipantRegistry('trade.finance.demo.Client');
    tx.LCforwarded.pendingAt = tx.pendingAt;
    tx.LCforwarded.status = tx.status;
    await LCRegistry.update(tx.LCforwarded);
    if(tx.status === "Issuing Bank Validated"){
        pop(tx.LCforwarded.issuingBank.pendingLC,tx.LCforwarded.LC_id);
        tx.LCforwarded.issuingBank.activeLC.push(tx.LCforwarded.LC_id);
        pop(tx.LCforwarded.advisingBank.activeLC,tx.LCforwarded.LC_id);
        tx.LCforwarded.advisingBank.pendingLC.push(tx.LCforwarded.LC_id);
    } 
    if(tx.status === "Advising Bank Validated"){
        pop(tx.LCforwarded.advisingBank.pendingLC,tx.LCforwarded.LC_id);
        tx.LCforwarded.advisingBank.activeLC.push(tx.LCforwarded.LC_id);
        pop(tx.LCforwarded.beneficiary.activeLC,tx.LCforwarded.LC_id);
        tx.LCforwarded.beneficiary.pendingLC.push(tx.LCforwarded.LC_id);
    }
    await bankRegistry.update(tx.LCforwarded.issuingBank);
    await bankRegistry.update(tx.LCforwarded.advisingBank);
    await clientRegistry.update(tx.LCforwarded.beneficiary);
}


/**
 * EDIT LC
 * @param {trade.finance.demo.editLC} editLC
 * @transaction
 */
async function editLC(tx) {
    const LCRegistry = await getAssetRegistry('trade.finance.demo.LC');

    if(tx.dateOfExpiry)
        tx.LCedited.dateOfExpiry = tx.dateOfExpiry;
    if(tx.currencyCode)
        tx.LCedited.currencyCode = tx.currencyCode;
    if(tx.amount != undefined)
        tx.LCedited.amount = tx.amount;
    if(tx.percentageCreditAmountTolerance != undefined)
        tx.LCedited.percentageCreditAmountTolerance = tx.percentageCreditAmountTolerance;
    if(tx.maximumCreditAmount != undefined)
        tx.LCedited.maximumCreditAmount = tx.maximumCreditAmount;
    if(tx.additionalAmountsCovered != undefined)
        tx.LCedited.additionalAmountsCovered = tx.additionalAmountsCovered;
    if(tx.requestType)
        tx.LCedited.requestType = tx.requestType;
    if(tx.subrequestType)
        tx.LCedited.subrequestType = tx.subrequestType;
    if(tx.creditAvailableWith)
        tx.LCedited.creditAvailableWith = tx.creditAvailableWith;
    if(tx.creditAvailableBy)
        tx.LCedited.creditAvailableBy = tx.creditAvailableBy;
    if(tx.usanceOfDrafts)
        tx.LCedited.usanceOfDrafts = tx.usanceOfDrafts;
    if(tx.drawee)
        tx.LCedited.drawee = tx.drawee;
    if(tx.defferedPaymentDetails)
        tx.LCedited.defferedPaymentDetails = tx.defferedPaymentDetails;
    if(tx.partialShipment)
        tx.LCedited.partialShipment = tx.partialShipment;
    if(tx.transshipment)
        tx.LCedited.transshipment = tx.transshipment;
    if(tx.shipmentFrom)
        tx.LCedited.shipmentFrom = tx.shipmentFrom;
    if(tx.portOfLoading)
        tx.LCedited.portOfLoading = tx.portOfLoading;
    if(tx.portOfDischarge)
        tx.LCedited.portOfDischarge = tx.portOfDischarge;
    if(tx.placeOfDelivery)
        tx.LCedited.placeOfDelivery = tx.placeOfDelivery;
    if(tx.latestDateOfShipment)
        tx.LCedited.latestDateOfShipment = tx.latestDateOfShipment;
    if(tx.typeOfGoods)
        tx.LCedited.typeOfGoods =   tx.typeOfGoods;
    if(tx.importLicense)
        tx.LCedited.importLicense = tx.importLicense;
    if(tx.exportImportCodeNumber)
        tx.LCedited.exportImportCodeNumber = tx.exportImportCodeNumber;
    if(tx.POnumber)
        tx.LCedited.POnumber = tx.POnumber;
    if(tx.countryOfOrigin)
        tx.LCedited.countryOfOrigin = tx.countryOfOrigin;
    if(tx.documentsRequired)
        tx.LCedited.documentsRequired = tx.documentsRequired;
    if(tx.addtionalConditions)
        tx.LCedited.addtionalConditions = tx.addtionalConditions;
    if(tx.chargesToBeneficiary != undefined)
        tx.LCedited.chargesToBeneficiary = tx.chargesToBeneficiary;
    if(tx.periodOfPresentationOfDocuments)
        tx.LCedited.periodOfPresentationOfDocuments = tx.periodOfPresentationOfDocuments;
    if(tx.LCinvoice)
        tx.LCedited.LCinvoice = tx.LCinvoice;
    if(tx.LCinsurance)
        tx.LCedited.LCinsurance = tx.LCinsurance;
    if(tx.LCotherDocs)
        tx.LCedited.LCotherDocs = tx.LCotherDocs;
    await LCRegistry.update(tx.LCedited);
}


/**
 * LC approved
 * @param {trade.finance.demo.approveLC} approveLC
 * @transaction
 */
async function approveLC(tx) {
    const LCRegistry = await getAssetRegistry('trade.finance.demo.LC');
    tx.LCapproved.status = tx.status;
    await LCRegistry.update(tx.LCapproved);
}


/**
 * Bills uploaded by beneficiary
 * @param {trade.finance.demo.uploadBillsUnderLC} uploadBillsUnderLC
 * @transaction
 */
async function uploadBillsUnderLC(tx) {
    const LCRegistry = await getAssetRegistry('trade.finance.demo.LC');
    const bankRegistry = await getParticipantRegistry('trade.finance.demo.Bank');
    const clientRegistry = await getParticipantRegistry('trade.finance.demo.Client');
    tx.LCbillsUploaded.billsDocs = tx.billsDocs;
    tx.LCbillsUploaded.pendingAt = tx.pendingAt;
    tx.LCbillsUploaded.status = tx.status;
    pop(tx.LCbillsUploaded.beneficiary.pendingLC ,tx.LCbillsUploaded.LC_id );
    tx.LCbillsUploaded.beneficiary.activeLC.push(tx.LCbillsUploaded.LC_id);
    pop(tx.LCbillsUploaded.advisingBank.activeLC,tx.LCbillsUploaded.LC_id);
    tx.LCbillsUploaded.advisingBank.pendingLC.push(tx.LCbillsUploaded.LC_id);
    await bankRegistry.update(tx.LCbillsUploaded.advisingBank);
    await clientRegistry.update(tx.LCbillsUploaded.beneficiary);
    await LCRegistry.update(tx.LCbillsUploaded);

}


/**
 * Bills verified by advising bank
 * @param {trade.finance.demo.verifyBillsUnderLC} verifyBillsUnderLC
 * @transaction
 */
async function verifyBillsUnderLC(tx) {
    const LCRegistry = await getAssetRegistry('trade.finance.demo.LC');
    const bankRegistry = await getParticipantRegistry('trade.finance.demo.Bank');
    tx.LCbillsVerified.pendingAt = tx.pendingAt;
    tx.LCbillsVerified.status = tx.status;
    pop(tx.LCbillsVerified.advisingBank.pendingLC , tx.LCbillsVerified.LC_id);
    tx.LCbillsVerified.advisingBank.activeLC.push(tx.LCbillsVerified.LC_id);
    pop(tx.LCbillsVerified.issuingBank.activeLC , tx.LCbillsVerified.LC_id);
    tx.LCbillsVerified.issuingBank.pendingLC.push(tx.LCbillsVerified.LC_id);
    await LCRegistry.update(tx.LCbillsVerified);
    await bankRegistry.update(tx.LCbillsVerified.issuingBank);
    await bankRegistry.update(tx.LCbillsVerified.advisingBank);
}


/**
 * funds released by issuing bank
 * @param {trade.finance.demo.releaseFunds} releaseFunds
 * @transaction
 */
async function releaseFunds(tx) {
    const LCRegistry = await getAssetRegistry('trade.finance.demo.LC');
    tx.LCfundsReleased.status = tx.status;
    await LCRegistry.update(tx.LCfundsReleased);
}


/**
 * bills under LC released by issuing bank
 * @param {trade.finance.demo.releaseBills} releaseBills
 * @transaction
 */
async function releaseBills(tx) {
    const LCRegistry = await getAssetRegistry('trade.finance.demo.LC');
    const bankRegistry = await getParticipantRegistry('trade.finance.demo.Bank');
    const clientRegistry = await getParticipantRegistry('trade.finance.demo.Client');

    tx.LCbillsReleased.pendingAt = 'COMPLETED';
    tx.LCbillsReleased.status = tx.status;
    pop(tx.LCbillsReleased.issuingBank.pendingLC , tx.LCbillsReleased.LC_id);
    pop(tx.LCbillsReleased.advisingBank.activeLC , tx.LCbillsReleased.LC_id);
    pop(tx.LCbillsReleased.applicant.activeLC , tx.LCbillsReleased.LC_id);
    pop(tx.LCbillsReleased.beneficiary.activeLC , tx.LCbillsReleased.LC_id);
    tx.LCbillsReleased.issuingBank.closedLC.push(tx.LCbillsReleased.LC_id);
    tx.LCbillsReleased.advisingBank.closedLC.push(tx.LCbillsReleased.LC_id);
    tx.LCbillsReleased.applicant.closedLC.push(tx.LCbillsReleased.LC_id);
    tx.LCbillsReleased.beneficiary.closedLC.push(tx.LCbillsReleased.LC_id);
    await LCRegistry.update(tx.LCbillsReleased);
    await bankRegistry.update(tx.LCbillsReleased.issuingBank);
    await bankRegistry.update(tx.LCbillsReleased.advisingBank);
    await clientRegistry.update(tx.LCbillsReleased.applicant);
    await clientRegistry.update(tx.LCbillsReleased.beneficiary);

}



function pop(arr, data) {

    let index = arr.findIndex( (arr_val) => {
      return arr_val === data;
    }); 
    arr.splice(index, 1);

}
