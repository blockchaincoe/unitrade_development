PK
     �[�Q���&  &     package.json{"engines":{"composer":"^0.19.4"},"name":"trade_finance_demo","version":"0.0.4","description":"demo application for trade finance","scripts":{"prepublish":"mkdirp ./dist && composer archive create --sourceType dir --sourceName . -a ./dist/trade_finance_demo.bna","pretest":"npm run lint","lint":"eslint .","test":"nyc mocha -t 0 test/*.js && cucumber-js"},"keywords":["composer","composer-network"],"author":"Newgen","email":"Newgen@newgen.co.in","license":"Apache-2.0","devDependencies":{"composer-admin":"^0.19.4","composer-cli":"^0.19.4","composer-client":"^0.19.4","composer-common":"^0.19.4","composer-connector-embedded":"^0.19.4","composer-cucumber-steps":"^0.19.4","chai":"latest","chai-as-promised":"latest","cucumber":"^2.2.0","eslint":"latest","nyc":"latest","mkdirp":"latest","mocha":"latest"}}PK
     �[�Q�7'9   9   	   README.md# trade_finance_demo

demo application for trade finance
PK
     �[�Q               models/PK
     �[�Q�<��V  V     models/trade.finance.demo.cto/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Write your model definitions here
 */

namespace trade.finance.demo

participant Bank identified by bankId {
  o String bankId
  o String bankName
  o String IFSCcode
  o String branch
  o String[] pendingLC optional
  o String[] activeLC optional 
  o String[] closedLC optional 
}

participant Client identified by clientId {
  o String clientId
  o String first_name
  o String last_name
  o String client_api_key
  o String address 
  o String[] pendingLC optional 
  o String[] activeLC optional 
  o String[] closedLC optional 
}

asset LC identified by LC_id { 
   // base details 
   o String LC_id 

   o String pendingAt
   o String status

   --> Client applicant
   --> Client beneficiary
   o String applicantFirstName
   o String applicantLastName
   o String applicantAddress
   o String beneficiaryFirstName
   o String beneficiaryLastName
   o String beneficiaryAddress

   // applicant details 
   o String typeOfLC
   --> Bank issuingBank 
   --> Bank advisingBank
   o String issuingBankName
   o String advisingBankName
   o String issuingBankIFSCcode
   o String advisingBankIFSCcode
   o String issuingBankBranch
   o String advisingBankBranch

   o String dateOfIssue
   o String dateOfExpiry
   o String currencyCode
   o Integer amount
   o Integer percentageCreditAmountTolerance
   o Integer maximumCreditAmount
   o Integer additionalAmountsCovered optional
   o String requestType 
   o String subrequestType
   
   // payment details
   o String creditAvailableWith
   o String creditAvailableBy
   o String usanceOfDrafts
   o String drawee
   o String defferedPaymentDetails optional 

   // shipment details
   o String partialShipment optional
   o String transshipment optional 
   o String shipmentFrom optional 
   o String portOfLoading optional 
   o String portOfDischarge optional 
   o String placeOfDelivery optional 
   o String latestDateOfShipment optional 
   

   // Goods Documents Conditions details
   o String typeOfGoods
   o String importLicense
   o String exportImportCodeNumber
   o String POnumber
   o String countryOfOrigin
   o String documentsRequired optional   
   o String addtionalConditions optional 

   // Other details
   o Integer chargesToBeneficiary
   o String periodOfPresentationOfDocuments //within .... days of issuance
   o String LCinvoice
   o String LCinsurance optional
   o String LCotherDocs optional
   o String billsDocs optional
}

transaction newLC {
  --> LC LCnotify
}

transaction forwardLC{
  --> LC LCforwarded
  o String pendingAt
  o String status
}

transaction editLC {
  --> LC LCedited
   o String dateOfExpiry optional
   o String currencyCode optional 
   o Integer amount optional
   o Integer percentageCreditAmountTolerance optional
   o Integer maximumCreditAmount optional
   o Integer additionalAmountsCovered optional
   o String request_type optional
   o String subrequest_type optional
   o String availableWith optional
   o String availableBy optional
   o String usanceOfDrafts optional
   o String drawee optional
   o String defferedPaymentDetails optional 
   o String partialShipment optional
   o String transshipment optional 
   o String shipmentFrom optional 
   o String portOfLoading optional 
   o String portOfDischarge optional 
   o String placeOfDelivery optional 
   o String latestDateOfShipment optional 
   o String typeOfGoods optional
   o String importLicense optional
   o String exportImportCodeNumber optional 
   o String POnumber optional 
   o String countryOfOrigin optional
   o String documentsRequired optional   
   o String addtionalConditions optional
   o Integer chargesToBeneficiary optional 
   o String periodOfPresentationOfDocuments optional  
   o String LCinvoice optional
   o String LCinsurance optional
   o String LCotherDocs optional
}

transaction approveLC {
  --> LC LCapproved
  o String status
}

transaction uploadBillsUnderLC{
  --> LC LCbillsUploaded
  o String billsDocs
  o String pendingAt
  o String status
}

transaction verifyBillsUnderLC{
  --> LC LCbillsVerified
  o String pendingAt
  o String status
}

transaction releaseFunds {
  --> LC LCfundsReleased
  o String status
}

transaction releaseBills {
  --> LC LCbillsReleased
  o String status
}

// enum LCType { 
//   o REVOCABLE 
//   o IRREVOCABLE 
//   o BACK TO BACK 
// }

// enum creditAvailableWith {
//   o ANY BANK
//   o ADVISING BANK
// }

// enum creditAvailableBy {
//   o ACCEPTANCE 
//   o DEFPAYMENT
//   o NEGOTIATION
//   o SIGHTPAYMENT
// }

// enum draweeType {
//   o ISSUING BANK
//   o NEGOTIATING BANK
// }

// enum shipmentType {
//   o PROHIBITED
//   o PERMITTED
// }

// enum LCStatus { 
//   o REQUESTED // LC requested by applicant
//   o ISSUING BANK VALIDATED 
//   o ADVISING BANK VALIDATED 
//   o BENEFICIARY APPROVED  // LC approved by benificary
//   o BILLS UPLOADED BY BENEFICIARY // bills under LC sent to advising bank 
//   o BILLS VERIFIED BY ADVISING BANK // bills verified by advising bank and sent to issuing bank
//   o FUNDS RELEASED // funds released by issuing bank  
//   o BILLS RELEASED // bills under LC sent to applicant
// } 

// enum RequestType { 
//   o EXPORT 
//   o IMPORT 
// }

// enum SubrequestType { 
//   o ISSUANCE 
//   o AMENDMENT 
//   o CANCELLATION 
//   o DRAFT 
// }

// enum usanceOfDraftsType { 
//   o SIGHT 
//   o USANSE 
// }

// enum goodsType {
//   o CAPITAL
//   o NON CAPITAL
// }
// PK
     �[�Q               lib/PK
     �[�Q�X�/L'  L'     lib/logic.js/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';
/**
 * Write your transction processor functions here
 */


/**
 * Notify all parties about new LC
 * @param {trade.finance.demo.newLC} newLC
 * @transaction
 */
async function newLC(tx) {
    const LCRegistry = await getAssetRegistry('trade.finance.demo.LC');
    const bankRegistry = await getParticipantRegistry('trade.finance.demo.Bank');
    const clientRegistry = await getParticipantRegistry('trade.finance.demo.Client');
    tx.LCnotify.applicant.activeLC.push(tx.LCnotify.LC_id);
    tx.LCnotify.issuingBank.pendingLC.push(tx.LCnotify.LC_id);
    tx.LCnotify.advisingBank.activeLC.push(tx.LCnotify.LC_id);
    tx.LCnotify.beneficiary.activeLC.push(tx.LCnotify.LC_id);
    await LCRegistry.update(tx.LCnotify);
    await bankRegistry.update(tx.LCnotify.issuingBank);
    await bankRegistry.update(tx.LCnotify.advisingBank);
    await clientRegistry.update(tx.LCnotify.applicant);
    await clientRegistry.update(tx.LCnotify.beneficiary);
}


/**
 * LC forwarded to Issuing Bank
 * @param {trade.finance.demo.forwardLC} forwardLC
 * @transaction
 */
async function forwardLC(tx) {
    const LCRegistry = await getAssetRegistry('trade.finance.demo.LC');
    const bankRegistry = await getParticipantRegistry('trade.finance.demo.Bank');
    const clientRegistry = await getParticipantRegistry('trade.finance.demo.Client');
    tx.LCforwarded.pendingAt = tx.pendingAt;
    tx.LCforwarded.status = tx.status;
    await LCRegistry.update(tx.LCforwarded);
    if(tx.status === "Issuing Bank Validated"){
        pop(tx.LCforwarded.issuingBank.pendingLC,tx.LCforwarded.LC_id);
        tx.LCforwarded.issuingBank.activeLC.push(tx.LCforwarded.LC_id);
        pop(tx.LCforwarded.advisingBank.activeLC,tx.LCforwarded.LC_id);
        tx.LCforwarded.advisingBank.pendingLC.push(tx.LCforwarded.LC_id);
    } 
    if(tx.status === "Advising Bank Validated"){
        pop(tx.LCforwarded.advisingBank.pendingLC,tx.LCforwarded.LC_id);
        tx.LCforwarded.advisingBank.activeLC.push(tx.LCforwarded.LC_id);
        pop(tx.LCforwarded.beneficiary.activeLC,tx.LCforwarded.LC_id);
        tx.LCforwarded.beneficiary.pendingLC.push(tx.LCforwarded.LC_id);
    }
    await bankRegistry.update(tx.LCforwarded.issuingBank);
    await bankRegistry.update(tx.LCforwarded.advisingBank);
    await clientRegistry.update(tx.LCforwarded.beneficiary);
}


/**
 * EDIT LC
 * @param {trade.finance.demo.editLC} editLC
 * @transaction
 */
async function editLC(tx) {
    const LCRegistry = await getAssetRegistry('trade.finance.demo.LC');

    if(tx.dateOfExpiry)
        tx.LCedited.dateOfExpiry = tx.dateOfExpiry;
    if(tx.currencyCode)
        tx.LCedited.currencyCode = tx.currencyCode;
    if(tx.amount != undefined)
        tx.LCedited.amount = tx.amount;
    if(tx.percentageCreditAmountTolerance != undefined)
        tx.LCedited.percentageCreditAmountTolerance = tx.percentageCreditAmountTolerance;
    if(tx.maximumCreditAmount != undefined)
        tx.LCedited.maximumCreditAmount = tx.maximumCreditAmount;
    if(tx.additionalAmountsCovered != undefined)
        tx.LCedited.additionalAmountsCovered = tx.additionalAmountsCovered;
    if(tx.requestType)
        tx.LCedited.requestType = tx.requestType;
    if(tx.subrequestType)
        tx.LCedited.subrequestType = tx.subrequestType;
    if(tx.creditAvailableWith)
        tx.LCedited.creditAvailableWith = tx.creditAvailableWith;
    if(tx.creditAvailableBy)
        tx.LCedited.creditAvailableBy = tx.creditAvailableBy;
    if(tx.usanceOfDrafts)
        tx.LCedited.usanceOfDrafts = tx.usanceOfDrafts;
    if(tx.drawee)
        tx.LCedited.drawee = tx.drawee;
    if(tx.defferedPaymentDetails)
        tx.LCedited.defferedPaymentDetails = tx.defferedPaymentDetails;
    if(tx.partialShipment)
        tx.LCedited.partialShipment = tx.partialShipment;
    if(tx.transshipment)
        tx.LCedited.transshipment = tx.transshipment;
    if(tx.shipmentFrom)
        tx.LCedited.shipmentFrom = tx.shipmentFrom;
    if(tx.portOfLoading)
        tx.LCedited.portOfLoading = tx.portOfLoading;
    if(tx.portOfDischarge)
        tx.LCedited.portOfDischarge = tx.portOfDischarge;
    if(tx.placeOfDelivery)
        tx.LCedited.placeOfDelivery = tx.placeOfDelivery;
    if(tx.latestDateOfShipment)
        tx.LCedited.latestDateOfShipment = tx.latestDateOfShipment;
    if(tx.typeOfGoods)
        tx.LCedited.typeOfGoods =   tx.typeOfGoods;
    if(tx.importLicense)
        tx.LCedited.importLicense = tx.importLicense;
    if(tx.exportImportCodeNumber)
        tx.LCedited.exportImportCodeNumber = tx.exportImportCodeNumber;
    if(tx.POnumber)
        tx.LCedited.POnumber = tx.POnumber;
    if(tx.countryOfOrigin)
        tx.LCedited.countryOfOrigin = tx.countryOfOrigin;
    if(tx.documentsRequired)
        tx.LCedited.documentsRequired = tx.documentsRequired;
    if(tx.addtionalConditions)
        tx.LCedited.addtionalConditions = tx.addtionalConditions;
    if(tx.chargesToBeneficiary != undefined)
        tx.LCedited.chargesToBeneficiary = tx.chargesToBeneficiary;
    if(tx.periodOfPresentationOfDocuments)
        tx.LCedited.periodOfPresentationOfDocuments = tx.periodOfPresentationOfDocuments;
    if(tx.LCinvoice)
        tx.LCedited.LCinvoice = tx.LCinvoice;
    if(tx.LCinsurance)
        tx.LCedited.LCinsurance = tx.LCinsurance;
    if(tx.LCotherDocs)
        tx.LCedited.LCotherDocs = tx.LCotherDocs;
    await LCRegistry.update(tx.LCedited);
}


/**
 * LC approved
 * @param {trade.finance.demo.approveLC} approveLC
 * @transaction
 */
async function approveLC(tx) {
    const LCRegistry = await getAssetRegistry('trade.finance.demo.LC');
    tx.LCapproved.status = tx.status;
    await LCRegistry.update(tx.LCapproved);
}


/**
 * Bills uploaded by beneficiary
 * @param {trade.finance.demo.uploadBillsUnderLC} uploadBillsUnderLC
 * @transaction
 */
async function uploadBillsUnderLC(tx) {
    const LCRegistry = await getAssetRegistry('trade.finance.demo.LC');
    const bankRegistry = await getParticipantRegistry('trade.finance.demo.Bank');
    const clientRegistry = await getParticipantRegistry('trade.finance.demo.Client');
    tx.LCbillsUploaded.billsDocs = tx.billsDocs;
    tx.LCbillsUploaded.pendingAt = tx.pendingAt;
    tx.LCbillsUploaded.status = tx.status;
    pop(tx.LCbillsUploaded.beneficiary.pendingLC ,tx.LCbillsUploaded.LC_id );
    tx.LCbillsUploaded.beneficiary.activeLC.push(tx.LCbillsUploaded.LC_id);
    pop(tx.LCbillsUploaded.advisingBank.activeLC,tx.LCbillsUploaded.LC_id);
    tx.LCbillsUploaded.advisingBank.pendingLC.push(tx.LCbillsUploaded.LC_id);
    await bankRegistry.update(tx.LCbillsUploaded.advisingBank);
    await clientRegistry.update(tx.LCbillsUploaded.beneficiary);
    await LCRegistry.update(tx.LCbillsUploaded);

}


/**
 * Bills verified by advising bank
 * @param {trade.finance.demo.verifyBillsUnderLC} verifyBillsUnderLC
 * @transaction
 */
async function verifyBillsUnderLC(tx) {
    const LCRegistry = await getAssetRegistry('trade.finance.demo.LC');
    const bankRegistry = await getParticipantRegistry('trade.finance.demo.Bank');
    tx.LCbillsVerified.pendingAt = tx.pendingAt;
    tx.LCbillsVerified.status = tx.status;
    pop(tx.LCbillsVerified.advisingBank.pendingLC , tx.LCbillsVerified.LC_id);
    tx.LCbillsVerified.advisingBank.activeLC.push(tx.LCbillsVerified.LC_id);
    pop(tx.LCbillsVerified.issuingBank.activeLC , tx.LCbillsVerified.LC_id);
    tx.LCbillsVerified.issuingBank.pendingLC.push(tx.LCbillsVerified.LC_id);
    await LCRegistry.update(tx.LCbillsVerified);
    await bankRegistry.update(tx.LCbillsVerified.issuingBank);
    await bankRegistry.update(tx.LCbillsVerified.advisingBank);
}


/**
 * funds released by issuing bank
 * @param {trade.finance.demo.releaseFunds} releaseFunds
 * @transaction
 */
async function releaseFunds(tx) {
    const LCRegistry = await getAssetRegistry('trade.finance.demo.LC');
    tx.LCfundsReleased.status = tx.status;
    await LCRegistry.update(tx.LCfundsReleased);
}


/**
 * bills under LC released by issuing bank
 * @param {trade.finance.demo.releaseBills} releaseBills
 * @transaction
 */
async function releaseBills(tx) {
    const LCRegistry = await getAssetRegistry('trade.finance.demo.LC');
    const bankRegistry = await getParticipantRegistry('trade.finance.demo.Bank');
    const clientRegistry = await getParticipantRegistry('trade.finance.demo.Client');

    tx.LCbillsReleased.pendingAt = 'COMPLETED';
    tx.LCbillsReleased.status = tx.status;
    pop(tx.LCbillsReleased.issuingBank.pendingLC , tx.LCbillsReleased.LC_id);
    pop(tx.LCbillsReleased.advisingBank.activeLC , tx.LCbillsReleased.LC_id);
    pop(tx.LCbillsReleased.applicant.activeLC , tx.LCbillsReleased.LC_id);
    pop(tx.LCbillsReleased.beneficiary.activeLC , tx.LCbillsReleased.LC_id);
    tx.LCbillsReleased.issuingBank.closedLC.push(tx.LCbillsReleased.LC_id);
    tx.LCbillsReleased.advisingBank.closedLC.push(tx.LCbillsReleased.LC_id);
    tx.LCbillsReleased.applicant.closedLC.push(tx.LCbillsReleased.LC_id);
    tx.LCbillsReleased.beneficiary.closedLC.push(tx.LCbillsReleased.LC_id);
    await LCRegistry.update(tx.LCbillsReleased);
    await bankRegistry.update(tx.LCbillsReleased.issuingBank);
    await bankRegistry.update(tx.LCbillsReleased.advisingBank);
    await clientRegistry.update(tx.LCbillsReleased.applicant);
    await clientRegistry.update(tx.LCbillsReleased.beneficiary);

}



function pop(arr, data) {

    let index = arr.findIndex( (arr_val) => {
      return arr_val === data;
    }); 
    arr.splice(index, 1);

}
PK 
     �[�Q���&  &                   package.jsonPK 
     �[�Q�7'9   9   	             P  README.mdPK 
     �[�Q                        �  models/PK 
     �[�Q�<��V  V               �  models/trade.finance.demo.ctoPK 
     �[�Q                        f  lib/PK 
     �[�Q�X�/L'  L'               �  lib/logic.jsPK      ]  �B    